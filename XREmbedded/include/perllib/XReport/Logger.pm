#######################################################################
# @(#) $Id: Logger.pm,v 1.2 2002/10/23 22:50:18 Administrator Exp $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
package XReport::Logger;

use strict;
use Carp;
use IO::Socket::INET;
use constant DBGDEST => '226.1.1.9:3000';
use Time::HiRes ();
use POSIX ();

use Exporter;
our ($VERSION, @ISA, @EXPORT, @EXPORT_OK);
@ISA         = qw(Exporter);
@EXPORT_OK   = qw( );

sub log {
  my $self = shift; my ($Ident, $FileHandles) = @{$self}{'Ident', 'FileHandles'}; 
  $main::xreport_dbgsock = new IO::Socket::INET(Proto=>'udp',PeerAddr=>DBGDEST, Reuse => 1) unless $main::xreport_dbgsock;

  #my $localTime = scalar localtime();
  my $t0 = Time::HiRes::gettimeofday();
  my $localTime = POSIX::strftime("%Y-%m-%d %H:%M:%S", localtime(int($t0))).sprintf(".%03d", int(($t0-int($t0))*1000));
  for (@_) {
    $main::xreport_dbgsock->send("$localTime - $ENV{COMPUTERNAME} $main::Application->{ApplName}.$$.LOG : \>$_", 0, DBGDEST) if $main::xreport_dbgsock;
  }

  for my $LOG (values(%$FileHandles)) {
    for (@_) {
      print $LOG "$localTime - $Ident >", $_, "\< $Ident\n";
	}
  }
}

sub logwarn {
  my $self = shift; my ($Ident, $FileHandles) = @{$self}{'Ident', 'FileHandles'}; 
  $main::xreport_dbgsock = new IO::Socket::INET(Proto=>'udp',PeerAddr=>DBGDEST, Reuse => 1) unless $main::xreport_dbgsock;

#  my $localTime = scalar localtime();
  my $t0 = Time::HiRes::gettimeofday();
  my $localTime = POSIX::strftime("%Y-%m-%d %H:%M:%S", localtime(int($t0))).sprintf(".%03d", int(($t0-int($t0))*1000));
  my $msg = join('', @_);
  $main::xreport_dbgsock->send("$localTime - $ENV{COMPUTERNAME} $main::Application->{ApplName}.$$.warn: \>$msg", 0, DBGDEST) if $main::xreport_dbgsock;
  warn "$localTime - $main::Application->{ApplName} \>$msg\n";
  
  for my $LOG (values(%$FileHandles)) {
      print $LOG "$localTime - $Ident warn: >", $msg, "\n";
  }
}

sub AddFile {
  my ($self, $log_id, $log_file) = @_; my $FileHandles = $self->{'FileHandles'};

  $self->RemoveFile($log_id) if ( $FileHandles->{$log_id} );
  
  my $LOG = $log_file;

  if ( !ref($log_file) ) {
    $LOG = Symbol::gensym();
	
    open($LOG, ">>$log_file")
	 or
	die "ApplicationError: Unable to open log file $log_file - $!";

    my $sel = select($LOG); $| = 1; select($sel);
  }
 
  $FileHandles->{$log_id} = $LOG;

  return $LOG;
}

sub RemoveFile {
  my ($self, $log_id) = @_; my $FileHandles = $self->{'FileHandles'};

  my $LOG = $FileHandles->{$log_id};

  if ( $LOG ) {
    close($LOG);
  }

  delete $FileHandles->{$log_id};
}

sub new { 
  my ($className, $Ident) = @_; my $self;
  $Ident = 'PROC' unless $Ident;
  $self = { 
    'Ident' => "$Ident.$$ " ,
    'FileHandles' => {} 
  };
  
  bless $self;
}

1;
