#######################################################################
# @(#) $Id: QUtil.pm,v 1.2 2002/10/23 22:50:18 Administrator Exp $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
package XReport::QUtil;

use strict;
use Carp;

use XReport;
use XReport::DBUtil;

use vars qw($VERSION @ISA @EXPORT_OK);
our ($VERSION, @ISA, @EXPORT, @EXPORT_OK);

require Exporter;

@ISA = qw(Exporter);

@EXPORT = qw();
@EXPORT_OK = qw();

# export: $tblName $tblPkey @tblFlds
# export_ok: &ATTACH &QCreate &QUpdate 

$VERSION = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r }; 

#my $tblName = "tbl_JobReports";
#my $tblPkey = "JobReportId";
#
#my @tblFlds = ( "JobReportName"
#	       , "LocalFileName", "SrvName", "XferRecipient" 
#	       , "JobName", "JobNumber", "XferStartTime", "TarFileNumberIN"
#	       , "RemoteHostAddr", "RemoteFileName", "JobExecutionTime", "XferEndTime"
#	       , "XferMode", "XferInBytes", "XferInLines", "XferInPages", "XferDaemon"
#	       , "XferId", "ElabStartTime", "ElabEndTime", "InputLines", "XferOutBytes"
#	       , "InputPages", "ParsedLines", "ParsedPages", "DiscardedLines"
#	       , "DiscardedPages", "Status", "UserTimeRef"
#	       , "UserTimeElab", "UserReportRef", "UserReportTitle", "UserRef"
#	       , "LocalPathId_IN", "LocalPathId_OUT", "HoldDays", "HasIndexes", "OrigJobReportName"
#	       , "XferDestArea"
#	      );

sub _getFieldsHash {
   my ($fields) = @_;
   my $row = {};
   for (0..$fields->Count()-1) {
     my $coln = $fields->Item($_)->Name();
     my $field = $fields->Item($_); 
     my $field_value = $field->Value();
   
     if (  ref($field_value) ) {
       my $fldval_array = [];
       push @$fldval_array, $fields->Item($_)->Value()->Date('yyyy-MM-dd') if $fields->Item($_)->Value()->can('Date');
       push @$fldval_array, $fields->Item($_)->Value()->Time('HH:mm:ss') if $fields->Item($_)->Value()->can('Time');
       push @$fldval_array, $fields->Item($_)->Value() unless scalar(@$fldval_array);
       $field_value = join(' ', @{$fldval_array});
     }
     $row->{$coln} = $field_value; 
   }
   return $row;
}

sub _getTblFlds {
   my ($que) = @_;
   my $tblName = $que->{__tgtTable} || 'tbl_JobReports';
   my $db = $que->{'db'};
   my $r = $db->_SQL("SELECT TOP 0 * FROM $tblName");
   my $rs = XReport::DBUtil::RECORDSET->new($r);
   my @fl = $rs->GetFieldsNames();
   $rs->Close();
   my $sysr = $db->_SQL(qq{
SELECT Col.Column_Name from INFORMATION_SCHEMA.TABLE_CONSTRAINTS Tab, INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE Col 
WHERE Col.Constraint_Name = Tab.Constraint_Name AND Col.Table_Name = Tab.Table_Name AND Constraint_Type = 'PRIMARY KEY'
    AND Col.Table_Name = '$tblName'
   });
   die "table $tblName has ho primary key " if $sysr->eof();
   my $coln = $sysr->Fields()->Item(0)->Value();
   $sysr->Close();  
   return ($coln, @fl);
}


sub SQL {
  my $que = shift;
  my @SQL = ( @_ );
#  print "$que (".ref($que).") ".ref($que->{'db'})."------- @SQL\n";
#  i::logit("selecting rows for SQL: $SQL[0]");# unless ($SQL[0] =~ /^SELECT/i);
  my $db = $que->{'db'};
  my $r = $db->_SQL(@SQL);
  $main::debug && i::logit("No result set returned from SQL @SQL") unless $r;
  unless ($r && $SQL[0] =~ /(?:\A\s*SELECT\s|\sOUTPUT\s)/is) {
     $r->Close() if $r;
     return undef;
  }
  return undef if $r->eof();
  
  my $result = [ $r->eof() ? undef : _getFieldsHash($r->Fields()) ];
  unless (wantarray) {
    $r->Close();
    return shift @{$result};
  }
  $r->MoveNext();
  while (!$r->eof()) {
      push @{$result}, _getFieldsHash($r->Fields());
      $r->MoveNext();
  }
  $r->Close();
  return ($result->[0] ? $result : undef);

}

sub ATTACH {
#  print join('=', @_), "\n";
  my $class = shift;
  my $prms = my $que = { @_ };
#  print join('=', %{ $prms }), "\n";

  $prms->{'db'} = new XReport::DBUtil( @_ ) unless defined($prms->{'db'});
  $que->{'db'} = $prms->{'db'};
  $que->{__tgtTable} = delete($prms->{ExternalTableName}) || 'tbl_JobReports';

  bless $que, $class;
  my $tbfldlist = $main::Application->{$que->{__tgtTable}.'_cols'};
  unless ( $tbfldlist ) {
     my @t = $que->_getTblFlds($que->{__tgtTable});
     $main::Application->{$que->{__tgtTable}.'_pkey'} = shift @t;
     $main::Application->{$que->{__tgtTable}.'_cols'} = join(' ', @t ); 
     $tbfldlist = $main::Application->{$que->{__tgtTable}.'_cols'};
  }
  die "unable to retrieve $que->{__tgtTable} Columns" unless $tbfldlist;
  $que->{__tblPkey} = $main::Application->{$que->{__tgtTable}.'_pkey'} || die "unable to retrieve $que->{ExternalTableName} primary key";
  $que->{__tblFlds} = [ split /\s+/, $tbfldlist ];

  return $que;
}

sub QCreate {

  # db       => dbc
  # colname  => colvalue
  # Id       => JobReportId

  my $que = shift;
  my $prm = { @_ };

  croak "No Server name specified" unless $prm->{'SrvName'};

  $que = ATTACH($que, @_ ) unless ($que ne 'XReport::QUtil');
  my ($tblName, $tblPkey) = @{$que}{qw(__tgtTable __tblPkey)};

  my $db = $que->{'db'};

  $que->{'Status'} = $CD::stAccepted unless $que->{'Status'};

  my $dbr;

  (my $metaData_key) = grep /^STOREMETADATA$/i, keys %{$XReport::cfg};
  my $thiscfg = $XReport::cfg->{'daemon'}->{$que->{'SrvName'}};


  my @JRNfldsN = qw(TypeOfWork Priority WorkClass); # numeric JobReportNames flds
  my @JRNfldsS = qw(LocalPathId_IN LocalPathId_OUT); # strings JobReportNames Fields
  
  my $JRNvals = { map { $_ => delete($que->{$_}) } (@JRNfldsN, @JRNfldsS) };
  my $JRNsel = join(',', 
                         (map { $JRNvals->{$_} ? "$JRNvals->{$_} AS $_" 
                                   : ("coalesce(jrn.$_,".(!$thiscfg ? 0 : ($thiscfg->{$_} || 0)).") AS $_")
                              } @JRNfldsN )
                       , (map { $JRNvals->{$_} ? "'$JRNvals->{$_}' AS $_" 
                                   : ("coalesce(jrn.Target$_,'".(!$thiscfg ? '' : ($thiscfg->{$_} || ''))."') AS $_")
                        } @JRNfldsS )
                   );
  my @cols2insert = grep { exists($que->{$_}) } @{$que->{__tblFlds}};
  my $SrvParameters = delete($que->{SrvParameters}) || '';
  my $fldvals = join(',', map { "'". ($_ || '') ."'" } @{$que}{@cols2insert});
  my $fldnames = join(',', map { "[$_]"} @cols2insert);
  my $srclist = join(',', map { "src.[$_]"} @cols2insert);
  my $add2wq = '';
  $add2wq = qq{
   OUTPUT '$tblName', inserted.\$IDENTITY, src.TypeOfWork, src.WorkClass, src.Priority, inserted.Status
        , src.XferDaemon as SrvName, src.SrvParameters
   INTO tbl_WorkQueue (ExternalTableName, ExternalKey, TypeOfWork, WorkClass, Priority, Status, SrvName, SrvParameters)
  } unless $metaData_key;
  my $SQL = qq{
with newjr as (
SELECT newvals.*, $JRNsel
                ,'$SrvParameters' as SrvParameters
from ( VALUES (0, $fldvals) ) newvals ( fakeid, $fldnames )
left outer join tbl_JobReportNames jrn on jrn.JobReportName = newvals.JobReportname 
)
MERGE $tblName as tgt
USING newjr as src
on tgt.\$IDENTITY = src.fakeid
WHEN NOT MATCHED BY TARGET THEN
insert ( $fldnames, [LocalPathId_IN], [LocalPathId_OUT] ) VALUES( $srclist, src.[LocalPathId_IN], src.[LocalPathId_OUT] ) 
$add2wq
OUTPUT '$tblName' as ExternalTable, inserted.\$IDENTITY as ExternalKey
         , src.TypeOfWork, src.WorkClass, src.Priority, src.SrvParameters
         , INSERTED.*
;
  };
  
  $dbr = $que->SQL($SQL);
  
  $que->{'Id'} = $dbr->{'Id'};
  foreach ( keys %{$dbr} ) { $que->{$_} = $dbr->{$_}; }
  $que->{'Id'} = $dbr->{ExternalKey};
  return wantarray ? ($que->{Id}, $que->{LocalPathId_IN}, $que->{LocalPathId_OUT}) : $que->{'Id'};
}

sub QUpdate {

  # db     => dbc
  # colname  => colvalue
  # Id => JobReportId

  my $que = shift;
#  print join(',', @_), "\n";
  $que = ATTACH($que, @_ ) unless ($que ne 'XReport::QUtil');
  my ($tblName, $tblPkey) = @{$que}{qw(__tgtTable __tblPkey)};

  my $db = $que->{'db'};

  my $dbr;

  my ($setstmt, $sep) = ('', '', '');
  foreach my $coln (@{$que->{__tblFlds}}) {
    next unless $que->{$coln};
    if ($coln eq $tblPkey) {
      $que->{'Id'} = $que->{$coln};
      next;
    }
    
    my $qt = (($que->{$coln} =~ /^(?:\d+|(?:\w+\(\)))$/) ? "" : "'");
    (my $val = $que->{$coln}) =~ s/'/''/g;
    $setstmt .= $sep . $coln . " = " . $qt . $val . $qt; 
    $sep = ", ";
  }

  $que->SQL("BEGIN TRANSACTION");

  $dbr = $que->SQL( 
		  "UPDATE $tblName SET ". $setstmt .
		  " OUTPUT INSERTED.* WHERE \$IDENTITY = " . $que->{'Id'}
		 );
  die "Update for $que->{Id} into $tblName failed" unless $dbr;
  my @updtcols = grep { $dbr->{$_} } keys %{$dbr}; 
  i::logit(join("\n", ( "UPDATE into $tblName SUCCEDED:"
                     , join("\t", @updtcols)
                     , join("\t", @{$dbr}{@updtcols}) ) ) 
                     );
  
  (my $metaData_key) = grep /^STOREMETADATA$/i, keys %{$XReport::cfg};
  if ( !$metaData_key ) {
     $dbr = $que->SQL( 
   		   "UPDATE tbl_WorkQueue SET Status = " . $que->{'Status'} .
   		   (exists($que->{TypeOfWork}) && $que->{TypeOfWork} ? ", TypeOfWork = " . $que->{TypeOfWork} : '' ) . 
              (exists($que->{WorkClass}) && $que->{WorkClass} ? ", WorkClass = " . $que->{WorkClass} : '' ) . 
   		   " OUTPUT INSERTED.* WHERE ExternalKey = " . $que->{'Id'} .
   		   " AND ExternalTableName = '" . $tblName . "'"
   		 );
     die "Update for $que->{Id} into tbl_WorkQueue failed" unless $dbr;
     my @updwcols = keys %{$dbr}; 
     i::logit(join("\n", ( "UPDATE into $tblName SUCCEDED:"
                     , join("\t", @updwcols)
                     , join("\t", @{$dbr}{@updwcols}) ) ) 
                     );
  }
  $que->SQL("COMMIT TRANSACTION");

  return $que;

}

1;
