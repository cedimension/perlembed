package XReport::Receiver::CDAMStream;

use strict 'vars';

use POSIX;

use Convert::EBCDIC;
use Data::Dumper;

use parent XReport::Receiver;

use XReport;
use XReport::JobREPORT;
use XReport::Util;
use XReport::QUtil;
use XReport::JUtil;
use XReport::Storage::IN;
use XReport::ENUMS qw(:XF_ENUMS);

my $translator = new Convert::EBCDIC;

sub JD_to_YMD {
	# expects ARG = [ century (0: 1900, 1: 2000 ...) (decimal), year of century (decimal), day num (NNNx) ]
	# a Julian date in packed decimal can be passed directly unpacking it with mask 'cH2H4'
#    warn "JD_to_YMD: century: $_[0] year: $_[1] day: $_[2]\n";
    return strftime('%Y-%m-%d', 
           localtime ( POSIX::mktime(0,0,0,0,0,(100*$_[0]) + $_[1])  # 24:00:00 Dec 31 of prev year
                       + substr($_[2], 0, 3) * 86400                 # seconds for daynum 
                     ) );
}

sub centh2time {
    my $val = unpack('N', $_[0]);
#   warn "JobNumber VAL: $val\n";
    my ($sec, $centh) = ($val/100, $val%100);
    my ($min, $ss)    = ($sec/60,  $sec%60 );
    my ($hh, $mm)     = ($min/60,  $min%60 );
    return sprintf('T%02d%02d%02d%02d', $hh, $mm, $ss, $centh);
}


BEGIN {
    $::translator = new Convert::EBCDIC();
    $::fldmap =  [
            { 'D' => 0, 'N' => 'DMY01', 'P' => 'TOHEXAD', 'L' => 2 },
           { 'D' => 2, 'N' => 'CTDVER', 'P' => 'TOASCII', 'L' => 4 },
           { 'D' => 6, 'N' => 'PRFIX', 'P' => 'TOASCII', 'L' => 8 },
           { 'D' => 14, 'N' => 'OJBID', 'P' => 'TOASCII', 'L' => 6 },
           { 'D' => 20, 'N' => 'JOBNM', 'P' => 'TOASCII', 'L' => 8 },
           { 'D' => 28, 'N' => 'PROCSTEP', 'P' => 'TOHEXAD', 'L' => 8 },
           { 'D' => 36, 'N' => 'STEPN', 'P' => 'TOASCII', 'L' => 8 },
           { 'D' => 44, 'N' => 'STEPNUM', 'P' => 'TOASCII', 'L' => 3 },
           { 'D' => 47, 'N' => 'CTDDATE', 'P' => 'EJD2DATE', 'L' => 6 },
           { 'D' => 55, 'N' => 'CTDTIME', 'P' => 'TOASCII', 'L' => 7 },
           { 'D' => 62, 'N' => 'SYSID', 'P' => 'TOASCII', 'L' => 4 },
           { 'D' => 66, 'N' => 'OEXTI', 'P' => 'TOHEXAD', 'L' => 2 },
           { 'D' => 68, 'N' => 'DSNAME', 'P' => 'TOASCII', 'L' => 44 },
           { 'D' => 112, 'N' => 'TRANSTO', 'P' => 'TOASCII', 'L' => 3 },
           { 'D' => 115, 'N' => 'TRANSUBT', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 116, 'N' => 'RESVD01', 'P' => 'TOHEXAD', 'L' => 4 },
           { 'D' => 120, 'N' => 'DDNAM', 'P' => 'TOASCII', 'L' => 8 },
           { 'D' => 128, 'N' => 'RECFM', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 129, 'N' => 'CCTYP', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 130, 'N' => 'LRECL', 'P' => 'TOSHORT', 'L' => 2 },
           { 'D' => 132, 'N' => 'BLKSZ', 'P' => 'TOSHORT', 'L' => 2 },
           { 'D' => 134, 'N' => 'CLASS', 'P' => 'TOASCII', 'L' => 1 },
           { 'D' => 135, 'N' => 'IDTRC', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 136, 'N' => 'HOLD', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 137, 'N' => 'DEST', 'P' => 'TOASCII', 'L' => 8 },
           { 'D' => 145, 'N' => 'WRITERN', 'P' => 'TOASCII', 'L' => 8 },
           { 'D' => 153, 'N' => 'FORM', 'P' => 'TOASCII', 'L' => 4 },
           { 'D' => 157, 'N' => 'FCB', 'P' => 'TOASCII', 'L' => 4 },
           { 'D' => 161, 'N' => 'UCS', 'P' => 'TOASCII', 'L' => 4 },
           { 'D' => 165, 'N' => 'UCSOP', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 166, 'N' => 'CHAR1', 'P' => 'TOASCII', 'L' => 8 },
           { 'D' => 174, 'N' => 'CHAR3', 'P' => 'TOASCII', 'L' => 8 },
           { 'D' => 182, 'N' => 'FLASH', 'P' => 'TOASCII', 'L' => 4 },
           { 'D' => 186, 'N' => 'FLASC', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 187, 'N' => 'BURST', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 188, 'N' => 'OPTCD', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 189, 'N' => 'PACKR', 'P' => 'TOASCII', 'L' => 8 },
           { 'D' => 197, 'N' => 'ALOPT', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 198, 'N' => 'PRFLG', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 199, 'N' => 'MODIF', 'P' => 'TOASCII', 'L' => 4 },
           { 'D' => 203, 'N' => 'PGCTR', 'P' => 'TOHEXAD', 'L' => 4 },
           { 'D' => 207, 'N' => 'JTYPE', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 208, 'N' => 'EDATE', 'P' => 'PJD2DATE', 'L' => 4 },
           { 'D' => 212, 'N' => 'ACCT3', 'P' => 'TOASCII', 'L' => 1 },
           { 'D' => 213, 'N' => 'DMY08', 'P' => 'TOHEXAD', 'L' => 4 },
           { 'D' => 217, 'N' => 'PAGEDEF', 'P' => 'TOASCII', 'L' => 6 },
           { 'D' => 223, 'N' => 'FORMDEF', 'P' => 'TOASCII', 'L' => 6 },
           { 'D' => 229, 'N' => 'OUTPT', 'P' => 'TOASCII', 'L' => 8 },
           { 'D' => 237, 'N' => 'APATP', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 238, 'N' => 'NEXTTTRZ', 'P' => 'TOHEXAD', 'L' => 2 },
           { 'D' => 240, 'N' => 'NEXTTT', 'P' => 'TOSHORT', 'L' => 2 },
           { 'D' => 242, 'N' => 'NEXTR', 'P' => 'TOTINY', 'L' => 1 },
           { 'D' => 243, 'N' => 'NEXTZ', 'P' => 'TOTINY', 'L' => 1 },
           { 'D' => 244, 'N' => 'NEXTRBD', 'P' => 'TOSHORT', 'L' => 2 },
           { 'D' => 246, 'N' => 'PREVTTRZ', 'P' => 'TOHEXAD', 'L' => 2 },
           { 'D' => 248, 'N' => 'PREVTT', 'P' => 'TOSHORT', 'L' => 2 },
           { 'D' => 250, 'N' => 'PREVR', 'P' => 'TOTINY', 'L' => 1 },
           { 'D' => 251, 'N' => 'PREVZ', 'P' => 'TOTINY', 'L' => 1 },
           { 'D' => 252, 'N' => 'PREVRBD', 'P' => 'TOSHORT', 'L' => 2 },
           { 'D' => 254, 'N' => 'ORDERID', 'P' => 'TOHEXAD', 'L' => 5 },
           { 'D' => 259, 'N' => 'LINCT', 'P' => 'TOSHORT', 'L' => 2 },
           { 'D' => 261, 'N' => 'FRBA', 'P' => 'TOHEXAD', 'L' => 8 },
           { 'D' => 269, 'N' => 'LRBA', 'P' => 'TOHEXAD', 'L' => 8 },
           { 'D' => 277, 'N' => 'ACCT', 'P' => 'TOASCII', 'L' => 8 },
           { 'D' => 287, 'N' => 'ETIME', 'P' => 'CCCC2TIME', 'L' => 4 },
           { 'D' => 291, 'N' => 'COPIES', 'P' => 'TOHEXAD', 'L' => 1 },
           { 'D' => 292, 'N' => 'ACCT2', 'P' => 'TOHEXAD', 'L' => 2 },
          ];

    @::ordmap = sort { $a->{D} <=> $b->{D} } @{$::fldmap};
    @::fldnames = map { $_->{N} } @::ordmap; 
    $::fldmask = join(' ', map { '@'.$_->{D}.'a'.$_->{L} } @::ordmap ); 
    $::fldprocs = { map { $_->{N} => ($_->{P} eq 'TOASCII' ? $::translator->can('toascii') : 
                   $_->{P} eq 'TOTINY' ? sub { shift; return unpack("C", shift) } :
                   $_->{P} eq 'TOSHORT' ? sub { shift; return unpack("n", shift) } :
#                   $_->{P} eq 'JD2DATE' ? sub { return JD_to_YMD(shift, substr(shift, 1)) } :
                   $_->{P} eq 'EJD2DATE' ? sub { return  JD_to_YMD( 1, unpack('x A2 A3', $_[0]->toascii($_[1]) ) )} :
                   $_->{P} eq 'PJD2DATE' ? sub { return JD_to_YMD( unpack('cH2H4', $_[1]) ) } :
                   $_->{P} eq 'CCCC2TIME' ? sub { return centh2time($_[1]) } :

                   sub { shift; return unpack("H*", shift) } ) } @::ordmap };
    $::asaaction = {
         "\xf1" => sub { return $main::ffeed.join('',@_); },
         "\xf0" => sub { return "\x0d\x0a\x0d\x0a".join('',@_); },
         "\x4e" => sub { return "\x0d".join('',@_); },
         "\x60" => sub { return "\x0d\x0a\x0d\x0a\x0d\x0a".join('',@_); },
         "\x40" => sub { return "\x0d\x0a".join('',@_); },
         "\x89" => sub { return join('',@_).$main::ffeed; },
         "\x01" => sub { return join('',@_)."\x0d"; },
         "\x09" => sub { return join('',@_)."\x0d\x0a"; },
         "\x11" => sub { return join('',@_)."\x0d\x0a\x0d\x0a"; },
         "\x19" => sub { return join('',@_)."\x0d\x0a\x0d\x0a\x0d\x0a"; },
         "\x89" => sub { return $main::ffeed; },
         "\x0b" => sub { return join('',@_)."\x0d\x0a"; },
         "\x13" => sub { return join('',@_)."\x0d\x0a\x0d\x0a"; },
         "\x1b" => sub { return join('',@_)."\x0d\x0a\x0d\x0a\x0d\x0a"; },
         'blank' => sub { return "\x0d\x0a".join('',@_); },
            };


}

sub rh2ascii {
    my $self = shift;
    my $rec = shift;
    $self->{cinfo}->{maxlrec_detected} = length($rec) if length($rec) > $self->{cinfo}->{maxlrec_detected};
    my ($asa, $rdata) = unpack("a a*", $rec);
    $rdata = '' unless $rdata;
    return &{$::asaaction->{exists($::asaaction->{$asa}) ? $asa : 'blank'}}(
                         &{$self->{translator}->{proc}}($self->{translator}->{hndl}, $rdata));
}

sub rh2none {
    my $self = shift;
    my $rec = shift;
    $self->{cinfo}->{maxlrec_detected} = length($rec) if length($rec) > $self->{cinfo}->{maxlrec_detected};
    return $rec;
}

sub recordparser {
    my $self = shift;
    my $data = shift;
    my $lrecl = ($self->{cinfo}->{lrecl} || 80);
    $self->{cinfo}->{maxlrec_detected} = $lrecl;
#    warn "LRECL: $lrecl BUFFER:\n", join("\n", unpack("(H100)*", $data)), "\n", '-' x 100, "\n";
    # split with parents adds the matching string to the entries in list
    # if the matching string is found at the beginnig of the block the first item in list will be empty
    my $recsarray = [ map { [length($_), $_ ] } map { my $rec = join('', map { my $fld = $_; 
           $fld =~ /^\xff/ ? do {
                               my ($c, $l) = unpack("xaC", $fld);
                               $c x $l ;
                                } : $fld  } split /(\xff..)/s, $_ );  
                       (exists($self->{recordexit}) ? &{$self->{recordexit}}($self, $rec) 
                                                    : $rec)
                     } split(/\xfe/s, $data ) 
     ];
    my $outbuff = join('', map { pack("n/a*", $_->[1]) } @{$recsarray});
#    $self->{rbytes} += length($outbuff);
    
    return ($outbuff, '', $recsarray);
}

sub parseCDAMHDR {
    my ($self, $datahdr) = (shift, shift);
    
    my $rawflds = {};
    @{$rawflds}{@::fldnames} = unpack($::fldmask, substr($datahdr, 2));
    my $hdr = { map { (my $val = &{$::fldprocs->{$_}}($::translator, $rawflds->{$_})) =~ s/\s+$//; $_ => $val } @::fldnames };
    $hdr->{hdrl} = unpack('n', $datahdr);

    if ($hdr->{'ETIME'} && $hdr->{'EDATE'}) {
      $hdr->{'CRDTIM'} = $hdr->{EDATE}.'T'.join(':', unpack('xa2a2a2', $hdr->{ETIME}));
    }      
    elsif ($hdr->{'CTDTIME'} && $hdr->{'CTDDATE'}) {
      $hdr->{'CRDTIM'} = $hdr->{CTDDATE}.'T'.join(':', unpack('xa2a2a2', $hdr->{CTDTIME}));
      $hdr->{'userTimeRef'} = $hdr->{CTDDATE}.'T'.join(':', unpack('xa2a2a2', $hdr->{CTDTIME}));
    }      
    $hdr->{'CRDTIM'} = strftime("%Y-%m-%dT%H:%M:%S.%U", localtime) unless defined($hdr->{'CRDTIM'});
    @{$hdr}{qw(JobName JobOrigin RemoteFileName JobExecutionTime lrecl)} 
                                      = @{$hdr}{qw(JOBNM SYSID DSNAME CRDTIM LRECL)};
    $hdr->{JobNumber} = do { local $_ = $hdr->{OJBID};
    	  ( /^S(\d+)$/i ? "STC$1"
    	  : /^T(\d+)$/i ? "TSU$1" 
          : /^J(\d+)$/i ? "JOB$1"
          : $_);
    }; 

    $hdr->{lrecl} = 80 unless $hdr->{lrecl};                                  
    $hdr->{rpttype} = '.CDAM.JESDATA';
    $hdr->{XferMode} = XReport::ENUMS::XF_PSF;
  
#    warn "CDAM HEADER: ", Dumper($hdr), "\n";
    my ($vkey) = grep {/verifyctlstream/i} (keys %{$XReport::cfg});
    my $verify = {};
    $verify = $XReport::cfg->{$vkey} if $vkey;
    while ( my ($var, $val) = each %$verify ) {
        warn "VERIFY: var: $var val: $val cfgval: $hdr->{$var}\n";
        return $self->setRequestErr("$var verify failed (VAL: $hdr->{$var})") unless exists($hdr->{$var}) && $hdr->{$var} =~ /$val/;
    }
    
    return $hdr;
}

sub new {
	my $class = shift;
	my %args = (@_);

	my $cinfo = ( exists($args{datahdr}) ? $class->parseCDAMHDR(delete $args{datahdr}) : {} );
	
	my $self = $class->SUPER::new(%args, XferStartTime => $cinfo->{CRDTIM}, 
	               translator => { hndl => $::translator, proc => $::translator->can('toascii') }, cinfo => $cinfo, );
	$self->SetReportName('XRRENAME');
	return $self;               
}

sub initJobReport {
  my $newjrh = shift;

  if ( $main::globals->{XRDBASE} ) {
    $newjrh->{JCH} = $main::globals->{XRDBASE};
  }
  else {
    $newjrh->{JCH} = new XReport::JUtil();
  }
  my $dbc = $newjrh->{JCH};
  my $cinfo = $newjrh->{'cinfo'};  

  if (my $oldid = $dbc->JCExist($newjrh->CtlStream())) {
    return $newjrh->notifyError("$cinfo->{JOBNM} ($cinfo->{OJBID}) appears to be duplicated ($oldid)- process terminated");
  }

  #TODO chkattrib: add handling of different queue types
  my $confdir = $XReport::cfg->{confdir};
  my $rptconf = $confdir.'/'.$newjrh->{cinfo}->{reportname}.'.xml';
  print "Checking rptconf $rptconf\n";
  # TODO chkattrib: merge rptconf with XReport/service config
  my $override = -f $rptconf ? $newjrh->parseRptConf($rptconf) : {};

  return $newjrh->notifyError("$cinfo->{JOBNM} ($cinfo->{OJBID}) rejected by rptconf - process terminated")
    if (!defined($override) );

  $newjrh->{'cinfo'} = { %{$newjrh->{'cinfo'}}, %$override } if $override;
  my $newJRID  = $newjrh->addNewRequest();
  @{$newjrh}{qw(reportname)} = $cinfo->{reportname};
  $newjrh->{odatetime} = join('', unpack("a4xa2xa2xa2xa2xa2", $newjrh->{XferStartTime}));
  @{$newjrh}{qw(filen outpdir odatetime)} =
                   $newjrh->setXferFileA('', @{$newjrh}{qw(reportname dbid odatetime LocalPathId_IN)});
  @{$newjrh}{qw(dfilen dbodir dbdtime)} = @{$newjrh}{qw(filen outpdir odatetime)};
                                     
#  warn "initJR new handle\n", Dumper($newjrh), "\n";

  my $datafile = join('/', @{$newjrh}{qw(outpdir filen)}).".DATA.TXT.gz";
  my $recordexit = ($cinfo->{convert2ascii} ? 'rh2ascii' : 'rh2none' );
  my $ofileh = XReport::Storage::IN->Create($datafile 
                            ,'cinfo' => $newjrh->{cinfo}
                            ,'translator' => { hndl => $::translator, proc => $::translator->can('toascii') }
                            ,'recordparser' => $newjrh->can('recordparser')
                            ,'recordexit' => $newjrh->can($recordexit)
                            );
  if ( !$ofileh ) {
        $newjrh->setRequestErr("DATA FILE OPEN ERROR for $datafile - $!");
        die "invalid handle" unless $ofileh;
  };
  die "record handler not found for $newjrh->{cinfo}->{XferMode} " unless $ofileh->{recordparser};
  
  $newjrh->DoLog("$newjrh->{SrvName} starting to store $cinfo->{JOBNM} ($cinfo->{OJBID}) data into "
           . $newjrh->{filen} .".DATA.TXT.gz (" . $newjrh->{LocalPathId_IN} . ")");
  @{$newjrh}{qw(outfh dfilen)} = ($ofileh, $datafile);
  return ($ofileh);
}	

1;
