package XReport::Receiver::FTPServ;

use strict 'vars';
use UNIVERSAL;

use Carp;

#use base qw(XReport::Receiver XReport::Receiver::FTPcmds);
use base qw(XReport::Receiver);

our $VERSION = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

#our ($dataport, $FTPuser, $FTPusertemp, $passive);
#our ($dataserv, $datasock, $blen, $pklen, $local);

#use constant XF_LPR => 1;
#use constant XF_PSF => 2;
#use constant XF_FTPB => 3;
#use constant XF_FTPC => 4;
#use constant XF_ASPSF => 5;
#use constant XF_AFPSTREAM => 6;

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $self = {port      => 21,
	      eof       => 0,
	      sel       => new IO::Select(),
	      sock      => new IO::Socket::INET(),
	      socktimeout => 240,
	      ackstr    => '', ackmsg    => '',
	      prtname   => '', queuetype => '', queuename => '',
	      peeraddr  => '', peerport  => '', loclport  => '',
	      outbytes  => 0,  rbytes    => 0,  wbytes => 0,
	      dbytes    => 0,  cbytes    => 0,
	      dfilen    => '', cfilen    => '', cdata => '',  dbuff => '',
	      cntr      => 0,  
	      cinfo     => {
			    Type      => 'A',
			    Format    => 'N',
			    XferMode  => XReport::ENUMS::XF_LPR,
			    Mode      => 'S',
			   }, 
#	      sdata => {},

	      logrtn    => sub {
		print "FTPServ $$:", @_, "\n" if ($main::debug == 1);
		return;
	      },

	     };

  $self = { %{$self},
	    @_ };

  bless $self, $class;
  
  $self->DoLog("New session started");
  return $self;
}

sub FTPrecv {
  my $self = shift;
  my $maxlen = shift || 1024;
  my $outarray = {};
  my ($sel, $sock, $dskt, $socktimeout) = @{$self}{qw(sel sock dskt socktimeout)};
  $self->logDebug("Waiting for " . $sel->count() . " sockets to become ready");
  $self->logDebug("localport: ". $self->{localport}) if exists($self->{localport});
  $self->logDebug("dataport: ". $self->{dataport}) if exists($self->{dataport});
  my @rdylist = $sel->can_read($socktimeout);
  if (!scalar(@rdylist)) {
    $self->DoLog("Connection timed out during cntl pkt $self->{'cntr'}");
    return undef;
  }
  for my $rdyskt (@rdylist) {
    my $sktport = $rdyskt->sockport();
    if ($rdyskt == $self->{sock}) { # is from command port
      $self->logDebug("Commnd data ready at socket at $sktport");
      my $cmdstat = $self->FTPCrecv(my $STMT, my $PARMS, $maxlen); 
      $outarray->{cmd} = [$STMT, $PARMS] if $cmdstat;
    }
    elsif (exists($self->{datasock}) and $rdyskt == $self->{datasock}) { # is from data port
	    $self->logDebug("data stream ready at socket at $sktport");
      my $blen = sysread($self->{datasock}, my $rcvbuff, $maxlen);
      if (! defined($blen) ) {
	     $self->DoLog( "Some error during sysread of data pkt - $! - $?\n");
      }
      elsif ( !$blen ) {
	$self->DoLog("Null Data pkt received - time to close ..");
	$self->{sel}->remove($self->{datasock}); 
	$self->FTPCsend("226 Closing data connection.");
	$self->{datasock}->close();
	$self->{datasock} = undef;;
	$outarray->{data} = '';
      }
      else {
	$self->{'rbytes'} += $blen;
	$self->{'dtabytes'} += $blen;
	$outarray->{data} = $rcvbuff;
	#$self->logDebug("Data pkt received: blen: $blen outarray: ", scalar(keys %$outarray));
      }
    }
    elsif (exists($self->{dataserv}) and $rdyskt == $self->{dataserv}) { # is from data listening socket
      $self->logDebug("connection ready at socket at $sktport");
      my $datasock = ($self->{passive} ? $self->{dataserv}->accept() : $self->{datasock});
      $self->{sel}->remove($self->{dataserv}); 
      $self->{dataserv}->close();
      if (! ($datasock) ) {
	    $self->DoLog( "Some error during connect to  data socket - $!");
	    return undef;
      }
      $self->{localport} = $datasock->sockport();
      $self->DoLog("Passive connection accepted - Data port is now $self->{localport}");
      $self->{datasock} = $datasock;
      $self->{sel}->add($self->{datasock}); 
    }
  }
    
  return { %$outarray } if scalar(keys %$outarray);
  
  return undef;
}

sub FTPCrecv {
  my $self = shift;
  my $STMT = \$_[0]; shift;
  my $PARMS = \$_[0]; shift;
  my $maxlen = shift || 1024;
  my ($sel, $sock, $ackmsg) = @{$self}{qw(sel sock ackmsg)};
  my $cmdbuff = '';
  ($$STMT, $$PARMS) = ('', '');

  $self->{'cntr'} += 1;
  my $blen = sysread($sock, $cmdbuff, $maxlen);
  croak "Some error encounterd during cntl pkt $self->{'cntr'} sysread\n"
	. "errstr:$?\n" unless (defined($blen) and $blen > 0);

  $self->{ctlbytes} += $blen;
  ($$STMT, $$PARMS) = ($cmdbuff =~ /^\s*(\w+)(?:\s+([^\s\x0d\x0a][^\x0d\x0a]*)|\s*)?[\x0d\x0a]*$/);
  $$STMT = uc($$STMT);

  $self->{'rbytes'} += $blen;
  $self->logDebug("FTP cmd ".$$STMT." received: ", length($cmdbuff));
  return length($cmdbuff);

}

sub FTPCsend {

  my ($self, $msg) = (shift, shift);
  my ($sel, $sock, $ackstr, $rbytes, $socktimeout) = @{$self}{qw(sel sock ackstr rbytes socktimeout)};
  if (!exists($self->{selw})) {
    $self->{selw} = new IO::Select($self->{sock});
  }
  if (! $self->{selw}->can_write($socktimeout)) {
    $self->DoLog( "Connection timed out");
    return undef;
  }
  $self->logDebug( "FTP child $$ to $self->{peeraddr}($self->{peerport}) => " . $msg );
  $msg .= "\n";
  $self->DoLog( "Returning Error to peer - $msg") if $msg =~ /^5\d\d/;
  return syswrite($sock, $msg, length($msg));
}

our $site_parms;

sub verifySParms {
  my $hndl = shift;
  my @notdef = ();
  foreach my $subp (keys %$site_parms) {
    push @notdef, $subp if (!exists($hndl->{'cinfo'}->{$subp}) and $site_parms->{$subp}->[1]);
  }
  return ( scalar(@notdef) ? join(',', @notdef) : '');

}

sub _cmd_CWD {
  my $hndl = shift;
  if ($_[0] && $_[0] =~ /^_dohelp/) {
    $hndl->FTPCsend("214 CD : set/display current report name ");
    return 1;
  }
  my ($reqname, undef) = split / /, shift, 2;
  my $currdir = $hndl->{'cinfo'}->{reqname};
  return $hndl->FTPCsend("200 Current Report Name is $currdir") unless $reqname;
  
  $hndl->{cinfo}->{reqname} = uc($reqname);
  return $hndl->FTPCsend("200 Current report name is now $hndl->{cinfo}->{reqname}");
}

sub _cmd_STAT {
  my $hndl = shift;
  if ($_[0] && $_[0] =~ /^_dohelp/) {
    $hndl->FTPCsend("214 STAT : describe the status of the FTP session");
    return 1;
  }
  $hndl->FTPCsend("211-Server FTP talking to host $hndl->{peeraddr}, port $hndl->{peerport}");
  $hndl->FTPCsend("211-User: $hndl->{'FTPuser'}");
  $hndl->FTPCsend("211-The control connection has transferred $hndl->{ctlbytes} bytes") if $hndl->{ctlbytes};
  $hndl->FTPCsend("211-The data connection has transferred $hndl->{dtabytes} bytes") if $hndl->{dtabytes};
  $hndl->FTPCsend("211-The next data stream " 
		  . (exists($hndl->{'cinfo'}->{PRMOD}) ? "has to have a " . $hndl->{'cinfo'}->{PRMOD} : "has undefined" )
		  . " format, of type " . $hndl->{'cinfo'}->{Type} 
		 );
  my $subp = {};
  @{$subp}{keys %$site_parms} = @{ $hndl->{'cinfo'} }{keys %$site_parms};
  foreach my $parm ( keys %$subp ) {
    $hndl->FTPCsend("211-$parm: $subp->{$parm}") if exists($hndl->{'cinfo'}->{$parm});
  }

  return $hndl->FTPCsend("211 *** end of status ***");
}

$site_parms->{PRMOD}           = ['Set origin dataset format', 1,
				 sub {
				   my ($hndl, $set) = (shift, shift);
				   (my $var, my $val, undef) = split /=/, $set, 3;
				   if ($val =~ /^\s*(PSF|AFPSTREAM|ASA|ASCII|FTPC|FTPB|BINSTREAM|MODCASTREAM|LPR)\s*$/i ) {
				   	 my $md = $1;
				     $hndl->logDebug("Setting $var to '$md'");
				     $hndl->{'cinfo'}->{$var} = uc($md);
				     $hndl->{'cinfo'}->{'XferMode'} = (
							    $md eq 'PSF' ? XReport::ENUMS::XF_PSF : 
							    $md eq 'AFPSTREAM' ? XReport::ENUMS::XF_AFPSTREAM : 
                                $md eq 'MODCASTREAM' ? XReport::ENUMS::XF_AFPSTREAM : 
                                $md eq 'BINSTREAM' ? XReport::ENUMS::XF_BINSTREAM : 
							    $md eq 'FTPC' ? XReport::ENUMS::XF_FTPC : 
							    $md eq 'FTPB' ? XReport::ENUMS::XF_FTPB : 
							    $md eq 'FTPS' ? XReport::ENUMS::XF_LPR : 
                                $md eq 'LPR' ? XReport::ENUMS::XF_LPR : 
							    $md eq 'ASCII' ? XReport::ENUMS::XF_ASCII : 
							    XReport::ENUMS::XF_LPR
							   );
				     return 1;
				   }
				   else  {
				     $hndl->FTPCsend("501-$var $val is not supported - $var ignored.");
				     return undef;
				   }
				 },
				 ];
$site_parms->{JOBNM}        = ['Set Origin JobName of data', 1, 
				 sub {
				   my ($hndl, $set) = (shift, shift);
				   (my $var, my $val, undef) = split /=/, $set, 3;
				   if ($val =~ /^\s*([\$\.\-\_\#\w]+)\s*$/ ) {
				     $hndl->logDebug("Setting $var to '$1'");
				     $hndl->{'cinfo'}->{$var} = $1;
				     $hndl->{'cinfo'}->{J} = $1;
				     $hndl->{'cinfo'}->{JobName} = $1;
				     return 1;
				   }
				   else  {
				     $hndl->FTPCsend("501-$var $val is not supported - $var ignored.");
				     return undef;
				   }
				 },
				];
$site_parms->{JOBID}      = ['Set Origin JobNumber of data', 1, 
				 sub {
				   my ($hndl, $set) = (shift, shift);
				   (my $var, my $val, undef) = split /=/, $set, 3;
				   if ($val =~ /^\s*(\w+)\s*$/ ) {
				     $hndl->logDebug("Setting $var to '$1'");
				     $hndl->{'cinfo'}->{$var} = $1;
				     $hndl->{'cinfo'}->{OJBID} = $1;
				     $hndl->{'cinfo'}->{JobNumber} = $1;
				     return 1;
				   }
				   else  {
				     $hndl->FTPCsend("501-$var $val is not supported - $var ignored.");
				     return undef;
				   }
				 },
				];
$site_parms->{OWNER}        = ['Force Origin Owner of Data instead of JOB', 1, 
				 sub {
				   my ($hndl, $set) = (shift, shift);
				   (my $var, my $val, undef) = split /=/, $set, 3;
				   if ($val =~ /^\s*(\w+)\s*$/ ) {
				     $hndl->logDebug("Setting $var to '$1'");
				     $hndl->{'cinfo'}->{$var} = $1;
				     $hndl->{'cinfo'}->{P} = $1;
				     $hndl->{'cinfo'}->{JobOwner} = $1;
				     return 1;
				   }
				   else  {
				     $hndl->FTPCsend("501-$var $val is not supported - $var ignored.");
				     return undef;
				   }
				 },
				];
$site_parms->{RemoteFileName} = ['set Origin File name', 1, 
				 sub {
				   my ($hndl, $set) = (shift, shift);
				   (my $var, my $val, undef) = split /=/, $set, 3;
                   if ($val =~ /^\s*\'?([\$\w\.\/:\(\)_\-]+)\'?\s*$/ ) {
				     $hndl->logDebug("Setting $var to '$1'");
				     $hndl->{'cinfo'}->{$var} = $1;
				     $hndl->{'cinfo'}->{N} = $1;
				     return 1;
				   }
				   else  {
				     $hndl->FTPCsend("501-$var $val is not supported - $var ignored.");
				     return undef;
				   }
				 },
				 ];
$site_parms->{JORIGIN}      = ['set origin system Id for data', 1, 
				 sub {
				   my ($hndl, $set) = (shift, shift);
				   (my $var, my $val, undef) = split /=/, $set, 3;
				   if ($val =~ /^\s*(\w+)\s*$/ ) {
				     $hndl->logDebug("Setting $var to '$1'");
				     $hndl->{'cinfo'}->{$var} = $1;
				     $hndl->{'cinfo'}->{H} = $1;
				     $hndl->{'cinfo'}->{JobOrigin} = $1;
				     return 1;
				   }
				   else  {
				     $hndl->FTPCsend("501-$var $val is not supported - $var ignored.");
				     return undef;
				   }
				 },
				];
$site_parms->{Recipient}      = ['Force Recipient Object to specific Name ', 0, 
				 sub {
				   my ($hndl, $set) = (shift, shift);
				   (my $var, my $val, undef) = split /=/, $set, 3;
				   if ($val =~ /^\s*(\w+)\s*$/ ) {
				     $hndl->logDebug("Setting $var to '$1'");
				     $hndl->{'cinfo'}->{$var} = $1;
				     return 1;
				   }
				   else  {
				     $hndl->FTPCsend("501-$var $val is not supported - $var ignored.");
				     return undef;
				   }
				 },
				];
$site_parms->{'MAILTO'}      = ['Set mail address to be notified at completion', 0, 
				 sub {
				   my ($hndl, $set) = (shift, shift);
                           $set =~ s/ *= */=/g;
                           
				   (my $var, my $val, undef) = split /=/, $set, 3;
				   if ($val =~ /^(")?(.*)\1?\s*$/ ) {
				     $hndl->logDebug("Setting $var to '$2'");
				     $hndl->{'cinfo'}->{$var} = $2;
				     return 1;
				   }
				   else  {
				     $hndl->FTPCsend("501-$var $val is not supported - $var ignored.");
				     return undef;
				   }
				 },
				];
$site_parms->{'PRINTO'}      = ['Set printer destination name to send output at completion', 0, 
				 sub {
				   my ($hndl, $set) = (shift, shift);
                           $set =~ s/ *= */=/g;
                           
				   (my $var, my $val, undef) = split /=/, $set, 3;
				   if ($val =~ /^(")?(.*)\1?\s*$/ ) {
				     $hndl->logDebug("Setting $var to '$2'");
				     $hndl->{'cinfo'}->{$var} = $2;
				     return 1;
				   }
				   else  {
				     $hndl->FTPCsend("501-$var $val is not supported - $var ignored.");
				     return undef;
				   }
				 },
				];
$site_parms->{'CRDTIM'}= ['Force Job Execution Date ', 0,
				 sub {
				   my ($hndl, $set) = (shift, shift);
				   (my $var, my $val, undef) = split /=/, $set, 3;
				   if ($val =~ /^\s*(\d{4}-\d{2}-\d{2}|\d{4}-\d{2}-\d{2}\s\d\d.\d\d)\s*$/ ) {
				     $hndl->logDebug("Setting ($set) $var to '$1'");
				     $hndl->{'cinfo'}->{$var} = $1;
				     return 1;
				   }
				   else  {
				     $hndl->FTPCsend("501-$var $val is not supported - $var ignored.");
				     return undef;
				   }
				 },
				];
$site_parms->{XferStartTime}    = ['Force Transfer Date ', 0, 
				 sub {
				   my ($hndl, $set) = (shift, shift);
				   (my $var, my $val, undef) = split /=/, $set, 3;
				   if ($val =~ /^\s*(\d{4}-\d{2}-\d{2}|\d{4}-\d{2}-\d{2}\s\d\d.\d\d)\s*$/ ) {
				     $hndl->logDebug("Setting ($set) $var to '$1'");
				     $hndl->{$var} = $1;
				     return 1;
				   }
				   else  {
				     $hndl->FTPCsend("501-$var $val is not supported - $var ignored.");
				     return undef;
				   }
				 },
				];
$site_parms->{FIXRecfm}       = ['Set Fixed length input Record Format  ', 0,
				 sub {
				   my ($hndl, $set) = (shift, shift);
				   (my $var, my $lrecl, my $val) = split /[=\s]+/, $set, 3;
				   # return $hndl->FTPCsend("200-$var $val is not supported - $var ignored.") if ($val !~ /^\s*(\d+)\s*$/ ); 
				   
				   $hndl->logDebug("Setting $var to 1 LRECL to $lrecl");
				   $hndl->{'cinfo'}->{$var} = 1; #$val;
				   $hndl->{'cinfo'}->{'LRECL'} = $lrecl;

				   my $fattrs = { split /[=\s]+/, $val };
				   @{ $hndl->{'cinfo'} }{keys %$fattrs} = @{$fattrs}{keys %$fattrs};
				   delete($hndl->{'cinfo'}->{VARecfm});
				   $hndl->FTPCsend("200-$var attributes " . join(',', keys %$fattrs) . " now set");

				   return 1;
				 },
				];
$site_parms->{BLKSIZE}        = ['Set Record stream blksize  ', 0,
                 sub {
                   my ($hndl, $set) = (shift, shift);
                   (my $var, my $val) = split /[=\s]+/, $set, 2;
                   # return $hndl->FTPCsend("200-$var $val is not supported - $var ignored.") if ($val !~ /^\s*(\d+)\s*$/ ); 
                   
                   $hndl->logDebug("Setting $var");
                   $hndl->{'cinfo'}->{$var} = $val;
                   return 1;
                 },
                ];
$site_parms->{LRECL}        = ['Set input Record logical len ', 0,
                 sub {
                   my ($hndl, $set) = (shift, shift);
                   (my $var, my $val) = split /[=\s]+/, $set, 2;
                   # return $hndl->FTPCsend("200-$var $val is not supported - $var ignored.") if ($val !~ /^\s*(\d+)\s*$/ ); 
                   
                   $hndl->logDebug("Setting $var");
                   $hndl->{'cinfo'}->{$var} = $val;
                   return 1;
                 },
                ];
$site_parms->{VARrecfm}        = ['Set Variable length input Record Format  ', 0,
				 sub {
				   my ($hndl, $set) = (shift, shift);
				   (my $cmd, my $var, my $lrecl, my $val) = split /[=\s]+/, $set, 4;
				   # return $hndl->FTPCsend("200-$var $val is not supported - $var ignored.") if $val;

				   $hndl->logDebug("Setting $var");
				   $hndl->{'cinfo'}->{$var} = 1;
				   
				   $hndl->{'cinfo'}->{'LRECL'} = $lrecl;
				   my $fattrs = { split /[=\s]+/, $val };
				   @{ $hndl->{'cinfo'} }{keys %$fattrs} = @{$fattrs}{keys %$fattrs};
				   delete($hndl->{'cinfo'}->{FIXRecfm});
				   $hndl->FTPCsend("200-$var attributes " . join(',', keys %$fattrs) . " now set");
				   return 1;
				 },
				 ];

sub _cmd_SITE {
  my $hndl = shift;
  if ($_[0] and $_[0] =~ /^_dohelp/) {
    $hndl->FTPCsend("214-The SITE sub parameters are:");
    foreach my $parm ( keys %$site_parms ) {
      $hndl->FTPCsend("211-$parm: $site_parms->{$parm}->[0]");
    }
    return $hndl->FTPCsend("214 Use server STAT command to display present values.");
  }
#  (my $cmd, undef) = split / /, shift, 2;
  my $cmd = shift;
  my ($var, $val) = ($cmd ? split(/[\s=]+/, $cmd, 2) : ('', ''));

  if (!$var) {
    if ( my $notdef = $hndl->verifySParms() ) {
      $hndl->FTPCsend("501-Following Site Sub Parameters are required to proceed:") || return undef;
      return $hndl->FTPCsend("501 " . $notdef . '.');
    }
    
    return $hndl->FTPCsend("202 SITE not necessary; you may proceed") unless $var;
  }

  unless ( (my $kvar) = grep(/$var/i, keys %$site_parms) ) {
    $hndl->FTPCsend("200-Unrecognized parameter '".$cmd."' on SITE command.")
  }
  else {
    $hndl->logDebug("now callig $kvar sub to handle $cmd");
    my $coderef = $site_parms->{$kvar}->[-1];
    $cmd =~ s/$var/$kvar/;
    return $hndl->FTPCsend("501 SITE command was ignored")
      unless &$coderef($hndl, $cmd);
  }

  return $hndl->FTPCsend("200 SITE command was accepted")
  
}

sub _cmd_HELP {
  my $hndl = shift;
  if ($_[0] and $_[0] =~ /^_dohelp/) {
    $hndl->FTPCsend("214-HELP {command}: describe a command. If no command is specified,");
    return $hndl->FTPCsend("214 HELP lists all available commands");
  }
  (my $cmd, undef) = ($_[0] ? split / /, shift, 2 : ('', ''));

  my $pkgname = sprintf('%s', __PACKAGE__);
  my $pkg = \%{$pkgname.'::'};
  if ( $cmd ) {
    local *sym = $$pkg{'_cmd_'.uc($cmd)};
    return &sym($hndl,'_dohelp') if defined(&sym);
  }

  #print "scanning now $pkgname\n";
  $hndl->FTPCsend("214-This server all available commands:");
  foreach my $name (keys %$pkg) {
    local *sym = $$pkg{$name};
    my $type = (defined(&sym) ? 'Function' : 'Other');

    # print "found $name in hndl as $type\n" if $type eq 'Function';
    ($type, $name) =  $name =~ /^(_cmd_)(.*)/;
    $hndl->FTPCsend("214-" . $name) if $type and $type eq '_cmd_' and defined(&sym);
  }
  # print "scan of $pkg ended\n";
  $hndl->FTPCsend("214-For information about a particular command, type");
  return $hndl->FTPCsend("214 HELP SERVER command or QUOTE HELP command");
}

sub _cmd_USER {
  my $hndl = shift;
  if ($_[0] =~ /^_dohelp/) {
    $hndl->FTPCsend("214 USER Userid: Identify yourself to the FTP Server");
    return 1;
  }
  (my $user, undef) = split / /, shift, 2;
  delete($hndl->{'FTPusertemp'});
  delete($hndl->{'FTPuser'});
  if ( $user =~ /\(none\)/i ) {
    my $hlpmsg = "USER $hndl->{'FTPusertemp'} is invalid. specify a valid user name";
    $hndl->DoLog($hlpmsg);
    $hndl->FTPCsend("530-$hlpmsg") || return undef;
    return $hndl->FTPCsend("530 Not logged in.");
  }
  $hndl->{'FTPusertemp'} = $user;
  return $hndl->FTPCsend("331 user name ok, need password.");
}

sub _cmd_PASS {
  my $hndl = shift;
  if ($_[0] =~ /^_dohelp/) {
    return $hndl->FTPCsend("214 PASS password:");
  }
  (my $passw, undef) = split / /, shift, 2;

  if ( !$hndl->{'FTPusertemp'} ) {
    my $hlpmsg = "no USER specified. specify a valid user name first";
    $hndl->DoLog($hlpmsg);
    $hndl->FTPCsend("530-$hlpmsg") || return undef;
    return $hndl->FTPCsend("530 Not logged in.");
  }
  elsif ($passw) {
    $hndl->{'FTPuser'} = $hndl->{'FTPusertemp'};
    $hndl->{'cinfo'}->{'OWNER'} = $hndl->{'FTPusertemp'};
    $hndl->{'cinfo'}->{'P'} = $hndl->{'FTPusertemp'};
    return $hndl->FTPCsend("230 user ".$hndl->{'FTPuser'}." logged in.");
  }
  else {
    $hndl->FTPCsend("331 user name ok, need password.");
    return 0;
  }
}

sub _cmd_ACCT {
  my $hndl = shift;
  if ($_[0] =~ /^_dohelp/) {
    return $hndl->FTPCsend("214 ACCT account:");
  }
  (my $acct, undef) = split / /, shift, 2;
  if ($acct) {
    $hndl->{'FTPuser'} = $hndl->{'FTPusertemp'};
    return $hndl->FTPCsend("230 user ".$hndl->{'FTPuser'}." logged in.");
  }
  else {
    $hndl->FTPCsend("331 user name ok, need password.");
    return 0;
  }
}

sub _cmd_SYST {
  my $hndl = shift;
  if ($_[0] =~ /^_dohelp/) {
    return $hndl->FTPCsend("214 SYST:");
  }
  return $hndl->FTPCsend("215 XReport system type");
}

sub _cmd_NOOP {
  my $hndl = shift;
  if ($_[0] =~ /^_dohelp/) {
    return $hndl->FTPCsend("214 NOOP: No Operation statement");
  }
  return $hndl->FTPCsend("200 OK");
}

sub _cmd_ALLO {
  my $hndl = shift;
  if ($_[0] =~ /^_dohelp/) {
    return $hndl->FTPCsend("214 ALLO: No Operation statement");
  }
  return $hndl->FTPCsend("200 OK");
}

sub _cmd_MODE {
  my $hndl = shift;
  if ($_[0] =~ /^_dohelp/) {
    $hndl->FTPCsend("214-MODE S|C|B: the default mode is S-Stream.");
    return $hndl->FTPCsend("214 The implemented transfer modes are Stream(MODE S), Compress (MODE C), Block (MODE B)");
  }
  my ($reqmode, undef) = split / /, shift, 2;
  my $currmode = $hndl->{'cinfo'}->{Mode};
  return $hndl->FTPCsend("200 Transfer mode is $currmode") unless $reqmode;

  if ( length($reqmode) != 1 ) {
    $hndl->FTPCsend("501-Mode command syntax error: Mode invalid");
  }
  elsif ( $reqmode !~ /C|S|B/i ) {
    $hndl->FTPCsend("501-unknown Mode $reqmode");
  }
  elsif ($reqmode !~ /$currmode/i) {
#    $hndl->_cmd_SITE("PRMOD=FTPC") if $reqmode eq 'C';
    if ($reqmode eq 'C') {
      my $coderef = $site_parms->{PRMOD}->[-1];
      return $hndl->FTPCsend("501 SITE command was ignored")
		unless &$coderef($hndl, 'PRMOD=FTPC');
    }
    elsif ($reqmode eq 'B') {
      my $coderef = $site_parms->{PRMOD}->[-1];
      return $hndl->FTPCsend("501 SITE command was ignored")
		unless &$coderef($hndl, 'PRMOD=FTPB');
    }
    elsif ($reqmode eq 'S') {
      my $coderef = $site_parms->{PRMOD}->[-1];
      return $hndl->FTPCsend("501 SITE command was ignored")
		unless &$coderef($hndl, 'PRMOD=FTPS');
    }
    $hndl->{'cinfo'}->{Mode} = uc($reqmode);
    return $hndl->FTPCsend("200 Transfer Mode is now " . ( $reqmode eq 'C' ? 'Compress' : $reqmode eq 'B' ? 'Block' : 'Stream' ));
  }
  
  return $hndl->FTPCsend("501 Transfer Mode is currently $currmode");
}

sub _cmd_TYPE {
  my $hndl = shift;
  if ($_[0] =~ /^_dohelp/) {
    $hndl->FTPCsend("214-TYPE {representation-type} {format}: the default type is ASCII.");
    return $hndl->FTPCsend("214 The implemented data types are ASCII(TYPE A <N>), IMAGE (TYPE I), EBCDIC (TYPE E)");
  }
  my $currtype = (!$hndl->{'cinfo'}->{Type} ? 'Ebcdic' :
		  $hndl->{'cinfo'}->{Type} =~ /^I/ ? 'Image' : 
		  $hndl->{'cinfo'}->{Type} =~ /^A/ ? 'Ascii' : 
		  'Ebcdic');
  $currtype .= ' '.(!$hndl->{'cinfo'}->{Format} ? 'NonPrint' :
		    $hndl->{'cinfo'}->{Format} =~ /^T/ ? 'Telnet' : 
		    $hndl->{'cinfo'}->{Format} =~ /^A/ ? 'ASA' :
		    'NonPrint' );

  my ($reqtype, $format) = split / /, shift, 3;
  $format = substr($format, 0, 1) if $format;
  return $hndl->FTPCsend("200 Representation TYPE is $currtype") unless $reqtype;

  my $currmode = $hndl->{'cinfo'}->{'Mode'};
  if ( length($reqtype) != 1 ) {
    $hndl->FTPCsend("504-Type command syntax error: type invalid");
  }
  elsif ( $reqtype !~ /A|I|E/i ) {
    $hndl->FTPCsend("504-unknown type $reqtype");
  }
  elsif ( $currmode =~ /^C$/ and  $reqtype =~ /^A$/i ) {
    $hndl->FTPCsend("504-TYPE $reqtype is invalid with current mode 'C'");
  }
  elsif ( $reqtype =~ /^A|E$/i  ) {
    if ($format and $format !~ /^N|T|C$/i ) {
      $hndl->FTPCsend("504-TYPE format $format is invalid");
    }
    else {
      $format = 'N' unless $format;
      if ( $reqtype =~ /^E$/i and $format =~ /^T$/i ) {
	print '==>'.$reqtype.'::'.$format."<==\n";
	$hndl->FTPCsend("504-FORMAT $format is invalid with TYPE Ebcdic");
      }
      else {
	my $reqtf = ''  
	  . ($hndl->{'cinfo'}->{Type} = uc($reqtype))
	  . ($hndl->{'cinfo'}->{Format} = uc($format));
	my ($prmod, $descr) = (
			       $reqtf eq 'AN' ? ('ASCII', 'Ascii Nonprint') :
			       $reqtf eq 'AT' ? ('ASCII', 'Ascii Telnet'  ) : ## (0c, 0d, 0a, etc.)
			       $reqtf eq 'AC' ? ('ASCII', 'Ascii ASA'     ) : ## ( 1, 0, +, -, etc. )
			       $reqtf eq 'EC' ? ('PSF', 'Ebcdic ASA'      ) : 
			       ('AFPSTREAM', 'Ebcdic NonPrint' )
			      );
#   $prmod = 'FTPC' if $currmode =~ /^C$/;
    $prmod = ($currmode eq 'C' ? 'FTPC' : $currmode eq 'B' ? 'FTPB' : $prmod);
	my $coderef = $site_parms->{PRMOD}->[-1];
	return $hndl->FTPCsend("501 SITE command was ignored")
	  unless &$coderef($hndl, 'PRMOD='.$prmod);
	return $hndl->FTPCsend("200 Representation type is now $descr");
      }
    }
#    elsif (!$format or $format =~ /^N/i ) {
#      $hndl->{'cinfo'}->{Type} = 'A';
#      $hndl->{'cinfo'}->{Format} = 'N';
#      $hndl->_cmd_SITE("PRMOD=ASCII");
#      return $hndl->FTPCsend("200 Representation type is Ascii NonPrint");
#    }
#    if ( $format =~ /^T/i ) {
#      $hndl->{'cinfo'}->{Type} = 'A';
#      $hndl->{'cinfo'}->{Format} = 'T';
#      $hndl->_cmd_SITE("PRMOD=ASCII");
#      return $hndl->FTPCsend("200 Representation type is Ascii Telnet");
#    }
#    else {
#      $hndl->{'cinfo'}->{Type} = 'A';
#      $hndl->{'cinfo'}->{Format} = 'T';
#      $hndl->_cmd_SITE("PRMOD=ASCII");
#      return $hndl->FTPCsend("200 Representation type is Ascii Telnet");
#    }
  }
  elsif ( $reqtype =~ /^I$/i ) {
    if ($format) {
      $hndl->FTPCsend("504-TYPE has unsupported format $format");
    }
    else {
      $hndl->{'cinfo'}->{Type} = 'Image';
      delete($hndl->{'cinfo'}->{Format});
      return $hndl->FTPCsend("200 Representation type is Image");
    }
  }
  
  return $hndl->FTPCsend("504 Type remains $currtype");
}

sub _cmd_PORT {
  my $hndl = shift;
  if ($_[0] =~ /^_dohelp/) {
    return $hndl->FTPCsend("214 PORT: ");
  }
  my @dp = split /\s*,\s*/, shift;
  $hndl->{dataport} = $dp[5]+(256*$dp[4]);
  $hndl->{dataaddr} = join('.', @dp[0,1,2,3]);
  $hndl->logDebug("data: ", join('.', @dp[0,1,2,3]), " $hndl->{dataport}");
  return $hndl->FTPCsend("200 Port command OK.");
}

sub _cmd_PASV {
  my $hndl = shift;
  if ($_[0] =~ /^_dohelp/) {
    return $hndl->FTPCsend("214 PASV: Enter passive mode ");
  }
  $hndl->{dataaddr} = $hndl->{sock}->sockhost();
  my $cmdport = $hndl->{sock}->sockport();
  $hndl->{dataport} = $cmdport - 1 unless $hndl->{dataport};
  $hndl->{passive} = 1;
  ($hndl->{localip} = $hndl->{dataaddr}) =~ s/\./,/g;
  $hndl->FTPCsend("227 Entering Passive Mode ($hndl->{localip}," . int($hndl->{dataport}/256) . "," . ($hndl->{dataport} % 256) .  ")");
  $hndl->{dataserv} = new IO::Socket::INET(Listen => 1, 
				     LocalPort => $hndl->{dataport},
				     LocalAddr => $hndl->{dataaddr},
				     Proto => 'tcp', 
				     Reuse => 1);
  if ( !$hndl->{dataserv} ) {
    $hndl->DoLog("unable to listen on $hndl->{dataport} - $!");
  	$hndl->FTPCsend("501 error during PASV socket INIT");				     
    return undef;
  }
  $hndl->{localport} = $hndl->{dataserv}->sockport();
  $hndl->{localip} = $hndl->{dataserv}->sockhost();
  $hndl->DoLog("Listener now waiting on $hndl->{localip} ($hndl->{localport})") if $hndl->{dataserv};
  $hndl->{localip} =~ s/\./,/g;
  my $sel = new IO::Select($hndl->{dataserv});
  if ( !$sel ) {
    $hndl->DoLog("unable to listen on $hndl->{dataport} - $!");
    $hndl->FTPCsend("501 error during Connection wait");                     
    return undef;
  }
  $hndl->{sel}->add($hndl->{dataserv});
  return 1;
}

sub _cmd_STOU {
	return _cmd_STOR(@_);
}

sub _cmd_STOR {
  my $hndl = shift;
  if ($_[0] =~ /^_dohelp/) {
    return $hndl->FTPCsend("214 STOR name: ");
  }
  (my $fname, undef) = split / /, shift, 2;

  if ( !$hndl->StartJobRecv($fname) ) {
    return $hndl->FTPCsend("501 Store rejected by filename validation") || return undef;
  }

  $hndl->DoLog("starting ".($hndl->{passive} ? 'passive ' : '')."data transfer "  );
  if (!$hndl->{passive}) {
    $hndl->DoLog("Connecting now to $hndl->{dataaddr} ($hndl->{dataport})");
    my $cmdport = $hndl->{sock}->sockport();
    $hndl->{datasock} = new IO::Socket::INET(
					     LocalPort => ($cmdport - 1),
					     LocalAddr => $hndl->{localip},
					     PeerPort => $hndl->{dataport},
					     PeerAddr => $hndl->{dataaddr},
					     Proto => 'tcp', 
					     Reuse => 1,
					    );
    if (!$hndl->{datasock}) {
      $hndl->DoLog("unable to connect to $hndl->{dataaddr} ($hndl->{dataport}) - $!");
      return undef;
    }
    $hndl->{localport} = $hndl->{datasock}->sockport();
    $hndl->{localip} = $hndl->{datasock}->sockhost();
    $hndl->{sel}->add($hndl->{datasock});
    $hndl->DoLog("$hndl->{localip} ($hndl->{localport}) Connected now to $hndl->{dataaddr} ($hndl->{dataport})");

  }
  return undef unless $hndl->{outfh} = $hndl->initJobReport();
  $hndl->FTPCsend("125 Transfer starting for FILE: ".(split(/[\\\/]+/, $hndl->{dfilen}))[-1].'.');
  
  return 1; #$hndl->FTPCsend("150 opens data connection for FILE: "..(split(/[\\\/]+/, $hndl->{dfilen}))[-1].".");
}

sub StartJobRecv {
  use POSIX qw(strftime);

  my $hndl = shift;
  my $reqname = shift;
 
  $hndl->DoLog("Starting to receive job control data req: $reqname");
#  print 'cc==>', join('::', ($hndl->SetReportName($reqname))), "<==\n";
  if (exists($hndl->{cinfo}->{reqname})) {
  	$reqname = $hndl->{cinfo}->{reqname} unless $reqname;
    my $coderef = $site_parms->{RemoteFileName}->[-1];
    &$coderef($hndl, 'RemoteFileName='.$reqname);  
    $hndl->SetReportName($hndl->{cinfo}->{reqname});
  }
  else {
    $hndl->SetReportName($reqname);
  }
  if ( my $notdef = $hndl->verifySParms() ) {
    $hndl->FTPCsend("501-Following Site Sub Parameters are required to proceed:") || return undef;
    $hndl->FTPCsend("501-" . $notdef . '.');
    return undef;
  }

  $hndl->{'cinfo'}->{'CRDTIM'} = strftime("%Y-%m-%d %H:%M:%S.%U", localtime) unless exists($hndl->{'cinfo'}->{'CRDTIM'});
  $hndl->{'XferStartTime'} = strftime("%Y-%m-%d %H:%M:%S.%U", localtime) unless exists($hndl->{'XferStartTime'});

  return 1;
}

sub notifyError {
  my $hndl = shift;
  foreach my $msg ( @_ ) {
    $hndl->FTPCsend("501-" . $msg . '.') || return undef;
  }
  return undef;
}

1;
