package XReport::Receiver::GTWServ;

use strict 'vars';
use UNIVERSAL;

use Carp;

use base qw(XReport::Receiver);
use Time::Hires ();
use POSIX ();

our $VERSION = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

##---- Report Format Values ----
use constant RESP_OK                 => "\000";
use constant REQ_ACK                 => "\000";

use constant RECEIVE_JOBS            => "\002";
use constant STATUS_SHORT            => "\003";
use constant STATUS_LONG             => "\004";
use constant REMOVE_JOBS             => "\005";

use constant ABORT_JOB               => "\001";
use constant START_JOB_CONTROL     => "\002";
use constant START_JOB_DATA        => "\003";

use constant CMD_END                 => "\n";

#use constant XF_LPR => 1;
#use constant XF_PSF => 2;
#use constant XF_FTPB => 3;
#use constant XF_FTPC => 4;
#use constant XF_ASPSF => 5;
#use constant XF_AFPSTREAM => 6;

sub new {
  my $proto = shift;
  my $class = ref($proto) || $proto;
  my $parms = { @_ };
  my $self = {port      => 9515,
	      eof       => 0,
	      sel       => new IO::Select(),
	      sock      => new IO::Socket::INET(),
	      socktimeout => 2, # 240 was too much to handle hardware load balancer,
	      ackstr    => '', ackmsg    => '',
	      prtname   => '', queuetype => '', queuename => '',
	      peeraddr  => '', peerport  => '', loclport  => '',
	      outbytes  => 0,  rbytes    => 0,  wbytes => 0,
	      dbytes    => 0,  cbytes    => 0,
	      dfilen    => '', cfilen    => '', cdata => '',  dbuff => '',
          cntr      => 0,  dcntr => 0,
	      cinfo     => {
			    Type      => 'I',
			    Format    => 'N',
			    XferMode  => (exists($parms->{XferMode}) ? delete($parms->{XferMode}) : XReport::ENUMS::XF_PSF),
			    Mode      => 'S',
			   }, 
#	      sdata => {},
	      logrtn    => sub {
		my $flag = 0;
		if ($_[0] eq "N2O") {
		  shift;
		  $flag = 1;
		}
		print "XRC:", @_, "\n" if ($main::debug == 1 or $flag == 1);
		return;
	      },
	     };

  $self = { %{$self},
	    %{$parms} };

  bless $self, $class;
  
  $self->logDebug( "New session started");
  return $self;
}

sub DoTrace {
    return unless $main::debug;
    my $self = shift;
    return $self->DoLog(@_);
}

sub recvcmd {
  my $self = shift;
  my $dbuff = \$_[0]; shift;
  my $maxlen = shift || 250;
  my ($sel, $sock, $ackmsg, $socktimeout) = @{$self}{qw(sel sock ackmsg socktimeout)};

  $$dbuff = '';

  $self->{'cntr'} += 1;
  $self->DoTrace("trying to receive cntl packet $self->{cntr}");
  if (! $sel->can_read($socktimeout)) {
    $self->DoLog( "Connection timed out during cntl pkt $self->{cntr}");
    return undef;
  }

  my $blen = sysread($sock, $$dbuff, $maxlen);
  my $info = ( (defined($blen) and $blen > 0) ? "len: $blen buff:" . unpack("H*", $$dbuff) : '');
  croak "Some error encounterd during cntl pkt $self->{cntr} sysread - errinfo: \{$? - $!\} info: \{$info\} - ackmsg: \{$ackmsg\}\n"
                                                                                                       unless (defined($blen) and $blen > 0);

  $self->{'rbytes'} += $blen;
  $self->{'ackstr'} = pack("nNN", $blen, int($self->{'rbytes'} / 16777216), ($self->{'rbytes'} % 16777216));
  $self->DoTrace("cntl pkt $self->{cntr} received: \{$info\} ackstr: ".unpack('H*', $self->{'ackstr'}));
#  $self->logDebug( "cntl pkt received: $info");
  return length($$dbuff);

}

sub recvdata {
  my $self = shift;
  my $dbuff = \$_[0];

  $$dbuff = '';

  my $blen = recvcmd($self, my $cmdbuff, 16);
  return undef unless defined($blen);
  
  my ($pklen, $info) = unpack("nH*", $cmdbuff);

  my $tobrcv = $pklen;
  my ($sel, $sock, $socktimeout) = @{$self}{qw(sel sock socktimeout)};

  if ( $pklen > 0 ) {
    return undef unless $self->sendack("lgt info ($blen bytes) received");
    $self->{'dcntr'} += 1;
    while ( $pklen > 0 ) {
      $self->DoTrace("trying to receive data pkt $self->{dcntr}");
      if (! $sel->can_read($socktimeout)) {
        $self->DoLog( "Connection timed out during data pkt $self->{dcntr}");
		return undef;
      }
      $blen = sysread($sock, my $rcvbuff, $pklen);
      if (! defined($blen) ) {
        $self->DoLog( "Some error encounterd during sysread during data pkt $self->{dcntr} - $!\n");
		return undef;
      }
      $self->DoTrace("Data pkt $self->{dcntr} received: blen: $blen pklen: $pklen");
#      $self->logDebug( "Data pkt $self->{dcntr} received: blen: $blen pklen: $pklen") if defined($blen);
      $$dbuff .= $rcvbuff;
      $pklen -= $blen;
      $self->{'rbytes'} += $blen;
    }
    
    $pklen = length($$dbuff);
  } else {
    $self->{'eof'} = 1;
    $self->DoLog( "Data Stream EOF Received");
  }

  if ($pklen != $tobrcv ) {
    $self->DoLog( "Receive Failure - bytes expected: $tobrcv - Received: $pklen");
    return undef;
  }
  $self->{'ackstr'} = pack("nNN", $pklen, int($self->{'rbytes'} / 16777216), ($self->{'rbytes'} % 16777216));
  return $pklen;
}

sub startJobRecv {
  my $self = shift;
  my $cmd = '';
  my $reqname = shift;

  $self->DoLog( "Starting to receive job control data");
  return undef unless recvcmd($self, $cmd);

  if ($cmd eq ABORT_JOB . CMD_END) {
    sendack($self, "Abort Command Aknowledged - aborting job receive");
    $self->DoLog( "Aborting jobs due to client request");
    return undef;
  }
  
  if (! (($self->{'cfilen'}) = ($cmd =~ /^\x020\s(.*)\x0a$/)) ) {
    $self->DoLog( "Client sent invalid Cmd - aborting job ");
    return undef;
  }
  
  return undef unless sendack($self, "Starting to receive Control Stream");
  while ( ! $self->{'eof'} ) {
    my $cflen = recvdata($self, my $inbuff);
    return undef unless defined($cflen);

    last unless $cflen > 0;

    return undef unless sendack($self);
    
    $self->logDebug( "Filling up buffer: copying $cflen bytes");
    $self->{'cdata'} .= $inbuff;
    $self->{'cbytes'} += $cflen;
    
  }

  return undef unless $self->parseCtlStream($self->{'cdata'});

  return undef unless sendack($self, "Cntl Stream Received");
  return undef unless recvcmd($self, $cmd);

  if ($cmd eq ABORT_JOB . CMD_END) {
    sendack($self, "Abort Command Aknowledged - aborting job receive");
    $self->DoLog( "Aborting jobs due to client request");
    return undef;
  }
  
  if (! (($self->{cinfo}->{'dfilen'}) = ($cmd =~ /^\x030\s(.*)\x0a$/)) ) {
    $self->DoLog( "Client sent invalid Cmd - aborting job ");
    return undef;
  }
  
  #TODO chkattrib: A questo punto bisogna interrompere la ricezione per SetReportName = UNDEF
  $self->SetReportName($reqname);
 
  $self->{'cinfo'}->{'CRDTIM'} = strftime("%Y-%m-%dT%H:%M:%S.%U", localtime) unless exists($self->{'cinfo'}->{'CRDTIM'});
  $self->{'XferStartTime'} = strftime("%Y-%m-%dT%H:%M:%S.%U", localtime) unless exists($self->{'XferStartTime'});

  return $self->{cinfo}->{'dfilen'};
}

sub sendack {

  my ($self, $msg, $usrdata) = (shift, shift, shift);
  $msg = '' unless $msg;
  $usrdata = pack("N", 0) unless $usrdata;

  $self->{'ackmsg'} = $msg;
  my ($sel, $sock, $ackstr, $socktimeout) = @{$self}{qw(sel sock ackstr socktimeout)};
  $ackstr = $ackstr . pack("a16",$usrdata);

  $self->DoTrace( "Wait for ready to write ACK:" .unpack("H*", $ackstr)." - {".$msg.'}' );
  if (! $sel->can_write($socktimeout)) {
    $self->DoLog( "Connection timed out");
    return undef;
  }
  #$self->{'cntr'} = 0;
  $self->DoTrace("Sending ACK");
  return syswrite($sock, $ackstr, length($ackstr));
}

sub sendlastack {

  my ($self, $msg) = (shift, shift);
  my ($sel, $sock, $ackstr, $rbytes, $socktimeout) = @{$self}{qw(sel sock ackstr rbytes socktimeout)};

  if (! $sel->can_write($socktimeout)) {
    $self->DoLog( "Connection timed out");
    return undef;
  }
    
  my $eojack = pack("NN", int($self->{'dbytes'} / 16777216), ($self->{'dbytes'} % 16777216));
  $self->logDebug( "Sending EOJ ACK: ", unpack("H*", $eojack));
  return syswrite($sock, $eojack, length($eojack));

}

sub SendStat {
  my ($self, $req) = (shift, shift);
  syswrite($self->{'sock'}, 
	   "$^O $^X $0 $$ $self->{cinfo}->{reqname} job will have generic: $self->{cinfo}->{rpttype}  specific: $self->{cinfo}->{rptname}\n" ) if ($req eq STATUS_LONG);
  syswrite($self->{'sock'}, 
	   "$^O $^X $0 $$ $self->{cinfo}->{'reqname'} Receiver Ready\n");
  
}

sub parseCtlStream {
  use POSIX qw(strftime);
  use Time::Local qw(timegm_nocheck timelocal_nocheck);
  my ($vkey) = grep {/verifyctlstream/i} (keys %$XReport::cfg);
  my $verify = {};
  $verify = $XReport::cfg->{$vkey} if $vkey;
  my ($self, $CtlStream) = (shift, shift);
#  print "VERIFY $vkey: ", Dumper($verify), "\n";
#  my $cinfo = { %{$self->{cinfo}} };
  my $cinfo = $self->{cinfo};
  foreach ( split(/\n/, $CtlStream) ) {
    /^\-oJ(\w+)=(\w+).*$/ && do {
      $cinfo->{$1} = $2; next; };
    
    /^\-o(\w+)=(\w+).*$/ && do {
      $cinfo->{$1} = $2; next; };
    
    /^J([\w\.\?]+)$/     && do {
      $cinfo->{'J'} = $1;
      (undef, undef, $cinfo->{jname}, $cinfo->{jnum}, undef) = split /\./, $cinfo->{'J'}, 5; 
      next; };
    
    /^(\w)(.*)$/         && do {
      $cinfo->{$1} = $2; next;};
  }
  if (!$cinfo->{'JOBNM'} && ($cinfo->{N} =~  /^([\w\.\?]+)$/)) {
    (undef, undef, $cinfo->{'JOBNM'}, $cinfo->{'OJBID'}, undef) = split /\./, $1, 5; 
  }
  
  if ($cinfo->{'TIME'} && $cinfo->{'DATE'}) {
    my ($jcent,$jyr,$jdays) = ($cinfo->{'DATE'} =~ /^(\d{2})(\d{2})(\d{3})$/);
    my ($nsec, $hsec) = ($cinfo->{'TIME'} =~ /^(\d{5})(\d{2})$/);
#    print "c=$jcent, y=$jyr, d=$jdays, n=$nsec, h=$hsec\n";
    if ($jcent && $jyr && $jdays && defined($nsec) && defined($hsec)) {
      $cinfo->{'CRDTIM'} = strftime("%Y-%m-%dT%H:%M:%S.$hsec", 
				    localtime timelocal_nocheck $nsec, 0, 0, $jdays, 0, (($jcent + 19) * 100) + $jyr)
    }      
  }
  $cinfo->{'CRDTIM'} = strftime("%Y-%m-%dT%H:%M:%S.%U", localtime) unless defined($cinfo->{'CRDTIM'});
  @{$cinfo}{qw(JobName JobNumber JobOrigin)} = @{$cinfo}{qw(JOBNM OJBID ORIGIN)};

  $self->LogStruct($self->{'cinfo'}, 'new_cinfo') if $main::debug;
#  print "CINFO: ", Dumper($cinfo), "\n";
  while ( my ($var, $val) = each %$verify ) {
    print "VERIFY: var: $var val: $val cfgval: $cinfo->{$var}\n";
    return notifyError($self, "$var verify failed (VAL: $cinfo->{$var})") unless exists($cinfo->{$var}) 
      && $cinfo->{$var} =~ /$val/;
#    print "PASSED\n";
  }

#  $self->{'cinfo'} = $cinfo;
#  return $self->{'cinfo'};
  return $cinfo;
}

sub notifyError {
  my $hndl = shift;
  $hndl->DoLog($_[0]);
  $hndl->{'ackstr'} = "\xFF\xFF\x00\x00\x00\x00\x00\x00\x00\x00";
  $hndl->sendack($_[0]);
  return undef;
}

1;
