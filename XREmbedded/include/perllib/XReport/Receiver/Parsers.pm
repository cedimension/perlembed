package XReport::Receiver::Parsers;

use strict;

sub _parseFTPB {
    my $buff = shift;
    my $reccount = 0;
    my $xdata = '';
    my $maxreclen = 0;
    while ( $buff && length($buff) >= 3  ) {
        my $len = unpack('xn', $buff); 
        last if $len > (length($buff) - 3);
        (my $flag, my $recdata, $buff) = unpack('a1 n/a a*', $buff);
        $reccount++;
        $xdata .= pack('na*', $len, $recdata);
        $maxreclen = $len if $len && $len > $maxreclen; 
        $buff = '' if ( $flag eq "\x40");
    }
    return ($xdata, $buff, $maxreclen);
}

sub _parseFTPC {
	my $fillchar = shift;
    my $byteCache = shift;
    my $xdata = '';
    my $incomplete = '';
    my $pos = 0;
    my $reccount = 0;
#    my $records = [];
    my $maxreclen = 0;
    
  my ($inrec, $outrec, $b1, $rc); my $incntrl = "";
  my ($at, $at_max) = (0, length($byteCache)); 
  
  while( $at_max > $at ) {
    
    my $ch = substr($byteCache,$at++,1);
    $incomplete .= $ch;
    
    $b1 = unpack("C", $ch);
    # blocks begins all times with a control byte
    # meanings:
    # byte with two high order bit on ( mask 0xC0 ): 
    #      filler bytes ( type I -> 0x00, type e/a -> 0x40 ) must be repeated (control Byte & 0x3f) times
    # byte with high order bit on ( mask 0x80 ): 
    #      following byte must be repeated (control Byte & 0x3f) times
    # 0x00: 
    #      following is a flag byte: 0x80 -> end of record
    #                                0x40 -> end of file
    
    if ( $b1 == 0x00 ) {
        last unless $at_max > $at;
        
        $incntrl = substr($byteCache,$at,1);
        $incomplete .= $incntrl;
        if ( $incntrl ne "\x80" and $incntrl ne "\x40" ) {
          die("UserError: INVALID FLAG BYTE AFTER CONTROL BYTE at buffer pos ".($at - 1).": "
              .unpack("H80", substr($byteCache,$at-1, 20))
              ."\nFIRST 40 Bytes of BUFFER: ".unpack("H80", $byteCache)
              );
        }
        $at++;
        # we must now process last record block
        next;
    }
    elsif ( ($b1 & 0xc0) == 0x80 ) {
      $inrec = substr($byteCache,$at,1);
      $incomplete .= $inrec;
      $rc = $b1 & 0x3f;
      $outrec .= $inrec x $rc;
      $at++;
    }
    elsif ( ($b1 & 0xc0) == 0xc0 ) {
      $rc = $b1 & 0x3f;
#      my $fillchar = ($self->{'cinfo'}->{Type} =~ /^I/i ? "\x00" : 
#                      $self->{'cinfo'}->{Type} =~ /^A/i ? "\x20" : "\x40"); # Last if type EBCDIC
      $outrec .= $fillchar x $rc; 
    }
    elsif ( ($b1 & 0x80) == 0x00 ) {
        last if $at_max < ($at + $b1);
        $inrec = substr($byteCache,$at,$b1);
        $incomplete .= $inrec;
        $outrec .= $inrec;
        $at += $b1;
    }
    else {
      die("UserError: INVALID MODEC MASK at buffer pos $at: "
          .unpack("H80", substr($byteCache,$at-1, 20))
          ."\nFIRST 40 Bytes of BUFFER: ".unpack("H80", $byteCache)
          );
    }
    next if $incntrl eq "";
    $xdata .= pack("n/a*", $outrec);
    $reccount++;
    my $reclen = length($outrec);
    $maxreclen = $reclen if $reclen && $reclen > $maxreclen; 
    $incomplete = '';
    $incntrl = '';
  }
  
  $incomplete .= substr($byteCache, $at) if $at < $at_max;
  return ($xdata, $incomplete, $maxreclen);
}

sub _parseFTPCB { return _parseFTPC("\x00", @_ ); }
sub _parseFTPCE { return _parseFTPC("\x40", @_ ); }

sub _parseLPR{
    my $buff = shift;
    my $maxreclen = 0;
    my $incomplete = '';
    die "LPR of ascii data not supported";
    my @lines = split /((?:\x0d\x0a|\x0c|\x0d(?!\x0a)|(?<!\x0d)\x0a))/, $buff;
    $incomplete = pop @lines if $lines[-1] !~ /(?:\x0a|x0d|\x0c)$/;
    my $records = [ map { my $reclen = length($_); 
        $maxreclen = $reclen if $reclen && $reclen > $maxreclen; 
        [length($_), $_ ] } @lines ];
    
    return (join('', map { pack("n/a*", $_) } @lines ), $incomplete, $records); 
}

sub _parseSTREAM {
    my $buff = shift;
    my ($records, $incomplete) = ([], '');
    my $maxreclen = $self->{cinfo}->{lrecl};
    $records = [ map { [ $maxreclen, $_ ] } unpack("(a$maxreclen)*", $buff) ];
    $incomplete = pop @$records if length($records->[-1]->[1]) != $maxreclen;
    return (join('', map {pack("n a*", @{$_} )} @{$records}), $incomplete, $maxreclen);
}

sub _parseFIXEDLEN {
   return _parseSTREAM(@_);
}

sub _parseAFPSTREAM {
    my $buff = shift;
    my $xdata = '';
    my $pos = 0;
    my $reccount = 0;
    my $maxreclen = 0;
    my $len = unpack('n', substr($buff, $pos+1));
    my $blen = length($buff) - 3;
    return ('', $buff, []) if $len > $blen;
    return ($buff, '', [$len]) if $len == $blen;

    while ( $blen > $len ) {
        my $outrec = substr($buff, $pos, $len + 3);
        $xdata .= pack('n/a*', $outrec);
        $reccount++;
        $maxreclen = $len + 3 if ($len + 3) > $maxreclen; 
        $blen -= $len;
        $pos += $len + 3;
        last if $blen < 7;
        $len = unpack('n', substr($buff, $pos+1));
        $blen -= 3;
        
    }   
    return ($xdata, ($blen ? substr($buff, $pos) : ''), $maxreclen);
}

sub _parsePSF {
    my $self = shift;
    my $buff = shift;
    return ('', $buff, []) if length($buff) < 16000;
    my $reccount = 0;
    my $xdata = '';
    my $maxreclen = 0;
    while ( $buff && length($buff) >= 2 && (my $len = unpack('xn', $buff)) > (length($buff) - 2) ) {
        (my $recdata, $buff) = unpack('n/a a*', $buff);
        $reccount++;
        $xdata .= pack('na*', $len, $recdata);
        $maxreclen = $len if $len && $len > $maxreclen; 
    }
    return ($xdata, $buff, $maxreclen);
}

1;