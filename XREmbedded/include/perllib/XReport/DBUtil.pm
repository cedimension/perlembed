#######################################################################
# @(#) $Id: DBUtil.pm,v 1.2 2002/10/23 22:50:18 Administrator Exp $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
package XReport::DBUtil;

use strict 'vars';
use Carp;

use Win32::OLE;
use Win32::OLE::Variant;

use XReport;
use XReport::Util;

#------------------------------------------------------------
use constant adStateClosed => 0;

use constant adOpenForwardOnly => 0;
use constant adOpenStatic => 3;
use constant adOpenDynamic => 2;

use constant adLockReadOnly => 1;
use constant adLockPessimistic => 2;
use constant adLockOptimistic => 3;
#------------------------------------------------------------

use vars qw($VERSION @ISA @EXPORT_OK);
our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);

require Exporter;

@ISA = qw(Exporter);
# Items to export into callers namespace by default. Note: do not export
# names by default without a very good reason. Use EXPORT_OK instead.
# Do not simply export all your public functions/methods/constants.
@EXPORT = qw(
	     &dbExecute &dbExecuteReadOnly &dbExecuteForUpdate &dbGetStaticRs 
         &dbGetDynamicRs &_SQL &new &GetLock &ReleaseLock &dateTimeSQL
	    );
@EXPORT_OK = qw(GetServerLock);
%EXPORT_TAGS = (  none => [], );
$VERSION = do { my @rv = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#rv, @rv }; 

my ($dbc_xr, @ldbc_xr, %dbcTable);

# Preloaded methods go here.

my @INIT_COMMANDS = (
 "SET LANGUAGE ENGLISH",
 "SET DATEFORMAT YMD",
 "SET ARITHABORT ON",
 "SET ANSI_NULLS ON",
 "SET ANSI_PADDING ON",
 "SET ANSI_WARNINGS ON",
 "SET ARITHABORT ON",
 "SET CONCAT_NULL_YIELDS_NULL ON",
 "SET QUOTED_IDENTIFIER ON", 
 "SET NUMERIC_ROUNDABORT OFF"
);

sub check_for_sql_status {
  my ($sql, $dbc) = @_; my $errors = $dbc->Errors()->Count(); return if !$errors; my $descr = "";

  for(0..($errors-1)) {
    $descr .= ($descr ne "" ? "\n" : "") . $dbc->Errors($_)->Description();
  }
  $main::debug && i::logit("Some error during SQL execution - descr: $descr");

  croak 
    join('::', (caller(1))[0,2])." GOT SQL ERRORS DETECTED:\n sql(\n$sql\n) descr(\n$descr\n).\n"
  ;
}

sub _SQL {
  my $db = shift;
  my @SQL = ( @_ );
  $main::debug && i::logit("DBUTIL::_SQL invoking SQL: ".join('+', @SQL));

#  print "Doing SQL $db --- ", $db->{'dbc'}, " ---- ",  join('+', @SQL), "\n";
  $db->CONNECT() if !$db->CONNECTED();
  my $dbc = $db->{'dbc'};

# print "before execute: @SQL\n";
  my $rs = $dbc->Execute( @SQL ) ;
  $rs = undef unless ($SQL[0] =~ /(?:\A\s*SELECT\s|\sOUTPUT\s)/is);
#  print "Done SQL ", ($rs ? $rs->eof() : 'UNDEF') , " eof after Execute ",  join('+', @SQL), "\n";
#  print "Done SQL ", $dbc->state() , " state after Execute ",  join('+', @SQL), "\n";
  
  if ( $dbc->Errors()->Count() > 0 ) {
    check_for_sql_status($_[0], $dbc);
  }

  $db->{'lastR'} = $rs;
  $main::debug && i::logit("DBUtil::_SQL SQL executed Result:".defined($rs) ? ref($rs) : 'UNDEFINED' );
  return $rs;
}

sub CONNECT {
  my $self = (ref($_[0])) ? shift : $dbc_xr; my ($dbstr, $dbc) = ($self->{dbstr}, undef);

  unless ( exists($self->{dbc}) ) {
    $dbc = $main::Server->CreateObject("ADODB.Connection");
    croak "DBERROR: ADODB Create Connection ERROR" if !$dbc;
    $self->{dbc} = $dbc;
  }
  $dbc = $self->{dbc};
  my $cstr_dbstr = $main::Application->{"cfg.db.$dbstr"};
  my $Provider = $self->{Provider};
#  i::logit("ABOUT TO CONNECT TO THE $dbstr DATABASE requested by ". join('::', (caller(1))[0,2])) if $dbc->State() == adStateClosed;
  
#  i::logit("ABOUT TO CONNECT TO THE $dbstr DATABASE requested by ". join('::', (caller())[0,2])) if 1;

#  my $cstr_dbstr = $XReport::cfg->{'dbase'}->{$dbstr};
  
#  $dbc = Win32::OLE->CreateObject("ADODB.Connection");
#  if ( !$dbc ) {
#    croak "DBERROR: ADODB Create Connection ERROR" if !$dbc;
#  }
#  $self->{'dbc'} = $dbc;

#  my ($Provider) = $cstr_dbstr =~ /PROVIDER=([^;]+?);/i; 

#  $Provider = $self->{'Provider'} = uc($Provider);
  
  while($dbc->State() == adStateClosed) {
    $dbc->Open($cstr_dbstr);
    do {
      if ( $dbc->Errors()->Count() > 0 ) {
        i::logit("DB OPEN ERROR - error count detected: ". $dbc->Errors(0)->Description());
      }
      else {
        i::logit("DB OPEN ERROR using connection: $cstr_dbstr - Error Description NOT available"); 
      }
      sleep(60);
    }
    if ($dbc->State() == adStateClosed);
  }

  $dbc->LetProperty("CommandTimeout", 7200 );

  if ( $Provider =~ /^(?:SQLOLEDB$|SQLNCLI)/i ) {
    for (@INIT_COMMANDS) {
      $dbc->Execute($_);
    }
  }
#  GetServerLock() if $self eq $dbc_xr;
  
#  i::logit("CONNECTED TO THE $dbstr DATABASE") if ($dbc->State() != adStateClosed);
}

sub CONNECTED {
  my $self = shift; my $dbc = $self->{'dbc'}; return 0 if !$dbc; 
  
  return 0 if $dbc->State() == adStateClosed; 

  return 1 if $self->{'Provider'} ne 'SQLOLEDB';

  eval { 
    $dbc->Execute( "SELECT GETDATE()"); 
  };

  return ($@ || $dbc->Errors()->Count() > 0 ? 0 : 1);
}


sub DISCONNECT {
  my $self = shift(); my ($dbc, $dbstr) = @{$self}{qw(dbc dbstr)};
  
  if ( $self->CONNECTED() ) {
    i::logit("ABOUT TO DISCONNECT FROM THE $dbstr DATABASE");
	
    $dbc->Close(); 

    i::logit("DISCONNECTED FROM THE $dbstr DATABASE");
  }
}

my @HistoryTables = qw(
  tbl_JobReports
  tbl_PhysicalReports
  tbl_LogicalReports
  tbl_JobReportsElabOptions
);

sub getSql {
  my ($self, $sql, $HIST) = (shift, shift); 

  $HIST = getConfValues('db_HIST') if !defined($HIST);
  
  return $sql if ( !$HIST );
  
  for my $tbl (@HistoryTables) { $sql =~ s/$tbl(?=\s)/$tbl$HIST/sgi }; return $sql; 
}

sub dbExecute_NORETRY {
  my $self = (ref($_[0])) ? shift : $dbc_xr; $self->CONNECT() if !$self->CONNECTED(); 
  
  my $dbc = $self->{'dbc'}; my $sql = $self->getSql(@_);
  
#  if ( $_[0] =~ /^\s*(?:(?:INSERT)|(?:UPDATE)|(?:DELETE)) /i ) {
#    i::logit("EXEC SQL BEGIN\n$_[0]\nEXEC SQL END");
#  }

  my $rs; eval { $rs = $dbc->Execute( "$sql" ); };
  
  if ( $@ ) {
    i::logit("DBEXECUTE_1 ERROR: $@"); $dbc = undef; 
#	sleep 5;
    croak "DBERROR: Exiting ON DataBase CONNECTION ERROR !!!";
  }
  elsif ( $dbc->Errors()->Count() > 0 ) {
    check_for_sql_status($sql, $dbc);
  }
  elsif ( !$rs ) { $dbc = undef; croak "DBERROR: ADODB Open Recordset Error !?"; }
  
  return XReport::DBUtil::RECORDSET->new($rs);
}

sub dbExecute {
  my $self = (ref($_[0])) ? shift : $dbc_xr; $self->CONNECT() if !$self->CONNECTED(); 
  
  my $sql = $self->getSql(@_); my $rs;
  #open(OUT, ">c:/temp/wsdl/".time()."test3.xml"); binmode(OUT); print OUT "$sql\n"; close(OUT);  #miro
#  if ( $_[0] =~ /^\s*(?:(?:INSERT)|(?:UPDATE)|(?:DELETE)) /i ) {
#    i::logit("EXEC SQL BEGIN\n$_[0]\nEXEC SQL END");
#  }

  while(1) {
    my $dbc = $self->{'dbc'}; eval { $rs = $dbc->Execute( "$sql" ); };

    if ( $@ =~ /General network error\./i) {
      i::logit("DBEXECUTE NETWORK ERROR: $@"); $dbc = undef; 
      sleep 5;
	  $self->DISCONNECT(); $self->CONNECT(); next;
	}
    elsif ( $@ ) {
      i::logit("DBEXECUTE ERROR: $@"); $dbc = undef; 
#	  sleep 5;
      i::logit("DBERROR: Exiting ON DataBase CONNECTION ERROR !!!");
      croak "DBERROR: Exiting ON DataBase CONNECTION ERROR !!!";
    }
    elsif ( $dbc->Errors()->Count() > 0 ) {
      check_for_sql_status($sql, $dbc);
    }
    elsif ( !$rs ) { $dbc = undef; croak "DBERROR: ADODB Open Recordset Error !?"; }

    last;
  }
  
  return XReport::DBUtil::RECORDSET->new($rs);
}

sub dbExecuteReadOnly {
  my $self = (ref($_[0])) ? shift : $dbc_xr; $self->CONNECT() if !$self->CONNECTED(); 
  
  my $sql = $self->getSql(@_);
  
  my $rs = Win32::OLE->CreateObject("ADODB.Recordset");
  if ( !$rs ) {
    croak "DBERROR: ADODB Create Recordset ERROR !?";
  }

  while(1) {
    my $dbc = $self->{'dbc'}; eval { $rs->Open("$sql", $dbc, adOpenDynamic, adLockReadOnly); };

    if ( $@ =~ /General network error\./i) {
      i::logit("DBEXECUTE NETWORK ERROR: $@"); $dbc = undef;
      sleep 5; 
	  $self->DISCONNECT(); $self->CONNECT(); next;
	}
    elsif ( $@ ) {
      i::logit("DBEXECUTE_2 ERROR: $@"); $dbc = undef; 
	  sleep 5;
      croak "DBERROR: Exiting ON DataBase CONNECTION ERROR !!!";
    }
    elsif ( $dbc->Errors()->Count() > 0 ) {
      check_for_sql_status($sql, $dbc);
    }
    elsif ( !$rs ) { $dbc = undef; croak "DBERROR: ADODB Open Recordset Error !?"; }

    last;
  }
  
  return XReport::DBUtil::RECORDSET->new($rs);
}

sub dbExecuteForUpdate {
  my $self = (ref($_[0])) ? shift : $dbc_xr; $self->CONNECT() if !$self->CONNECTED(); 
  
  my $sql = $self->getSql(@_);
  
  my $rs = Win32::OLE->CreateObject("ADODB.Recordset");
  if ( !$rs ) {
    croak "DBERROR: ADODB Create Recordset ERROR !?";
  }

  while(1) {
    my $dbc = $self->{'dbc'}; eval { $rs->Open("$sql", $dbc, adOpenForwardOnly, adLockPessimistic); };

    if ( $@ =~ /General network error\./i) {
      i::logit("DBEXECUTE NETWORK ERROR: $@"); $dbc = undef;
      sleep 5; 
	  $self->DISCONNECT(); $self->CONNECT(); next;
	}
    elsif ( $@ ) {
      i::logit("DBEXECUTE_2 ERROR: $@"); $dbc = undef; 
	  sleep 5;
      croak "DBERROR: Exiting ON DataBase CONNECTION ERROR !!!";
    }
    elsif ( $dbc->Errors()->Count() > 0 ) {
      check_for_sql_status($sql, $dbc);
    }
    elsif ( !$rs ) { $dbc = undef; croak "DBERROR: ADODB Open Recordset Error !?"; }

    last;
  }
  
  return XReport::DBUtil::RECORDSET->new($rs);
}

sub dbGetStaticRs {
  my $self = (ref($_[0])) ? shift : $dbc_xr; $self->CONNECT() if !$self->CONNECTED(); 
  
  my $sql = $self->getSql(@_);
  
  my $rs = Win32::OLE->CreateObject("ADODB.Recordset");
  if ( !$rs ) {
    croak "DBERROR: ADODB Create Recordset ERROR !?";
  }
  
  while(1) {
    my $dbc = $self->{'dbc'}; eval { $rs->Open("$sql", $dbc, adOpenStatic, adLockPessimistic); };

    if ( $@ =~ /General network error\./i) {
      i::logit("DBEXECUTE NETWORK ERROR: $@"); $dbc = undef; 
      sleep 5;
	  $self->DISCONNECT(); $self->CONNECT(); next;
	}
    elsif ( $@ ) {
      i::logit("DBEXECUTE_3 ERROR: $@"); $dbc = undef; 
	  sleep 5;
      croak "DBERROR: Exiting ON DataBase CONNECTION ERROR !!!";
    }
    elsif ( $dbc->Errors()->Count() > 0 ) {
      check_for_sql_status($sql, $dbc);
    }
    elsif ( !$rs ) { $dbc = undef; croak "DBERROR: ADODB Open Recordset Error !?"; }

    last;
  }
  
  return XReport::DBUtil::RECORDSET->new($rs);
}

sub dbGetDynamicRs {
  my $self = (ref($_[0])) ? shift : $dbc_xr; $self->CONNECT() if !$self->CONNECTED(); 

  my $sql = $self->getSql(@_);
  
  my $rs = Win32::OLE->CreateObject("ADODB.Recordset");
  if ( !$rs ) {
    croak "DBERROR: ADODB Create Recordset ERROR !?";
  }
  
  while(1) {
    my $dbc = $self->{'dbc'}; eval { $rs->Open("$sql", $dbc, adOpenDynamic, adLockPessimistic); };
  
    if ( $@ =~ /General network error\./i) {
      i::logit("DBEXECUTE NETWORK ERROR: $@"); $dbc = undef; 
      sleep 5;
	  $self->DISCONNECT(); $self->CONNECT(); next;
	}
    elsif ( $@ ) {
      i::logit("DBEXECUTE_4 ERROR: $@"); $dbc = undef; 
	  sleep 5;
      croak "DBERROR: Exiting ON DataBase CONNECTION ERROR !!!";
    }
    elsif ( $dbc->Errors()->Count() > 0 ) {
      check_for_sql_status($sql, $dbc);
    }
    elsif ( !$rs ) { $dbc = undef; croak "DBERROR: ADODB Open Recordset Error !?"; }

    last;
  }
  
  return XReport::DBUtil::RECORDSET->new($rs);
}

sub GetLock {
  my $self = (ref($_[0])) ? shift : $dbc_xr; $self->CONNECT() if !$self->CONNECTED(); 

  my $dbc = $self->{'dbc'};
  
  my ($LockName, $LockMode, $LockOwner, $LockTimeout) = @_;

  $LockMode = "Exclusive" if !defined($LockMode);
  $LockOwner = 'Session' if !defined($LockOwner);
  $LockTimeout = -1 if !defined($LockTimeout);
  
  my $dbr = $self->dbExecute(
    "EXEC XRGetAppLock "
   ."\@Resource = '$LockName', "
   ."\@LockMode = '$LockMode', "
   ."\@LockOwner = '$LockOwner', "
   ."\@LockTimeout = $LockTimeout "
  );

  my $rc = $dbr->Fields()->Item(0)->Value();

  if ( $rc == 999 ) {
    $dbc = undef; croak "DBERROR: XRGetAppLock ERROR rc=$rc !?";
  }
  
  return $rc;
}

sub ReleaseLock {
  my $self = (ref($_[0])) ? shift : $dbc_xr; $self->CONNECT() if !$self->CONNECTED(); 

  my $dbc = $self->{'dbc'}; my ($LockName, $LockOwner) = @_;
  
  $LockOwner = 'Session' if !defined($LockOwner);
  
  my $dbr = $self->dbExecute(
    "EXEC XRReleaseAppLock "
   ."\@Resource = '$LockName', "
   ."\@LockOwner = '$LockOwner'"
  );

  my $rc = $dbr->Fields()->Item(0)->Value();

  if ( $rc == 999 ) {
    $dbc = undef; croak "DBERROR: XRReleaseAppLock ERROR rc=$rc !?";
  }
  
  return $rc;
}

sub GetServerLock {
  my ($XreportName, $SrvName) = getConfValues(qw(XreportName SrvName)); return if $SrvName eq "";

  $XreportName = "NOTSET" unless $XreportName;;
  return if ref($main::Application) eq 'Win32::OLE';
  return if $main::Application->{NoApplicationLock};
  i::logit("Setting Lock for $SrvName requested by " . join('::', (caller(1))[0,2]));

  my $rc = $dbc_xr->GetLock("LK_SERVER_$XreportName\_$SrvName", "Exclusive", "Session", 0);
  if ( $rc == -1 ) {
    die("UserError: SERVER $SrvName ALREADY ACTIVE !!!");
  }
}

sub dateTimeSQL {
  my ($year, $month, $day, $hour, $minute, $seconds) = @_;
  if ( $year > 50  and $year <= 99) {
    $year = "19$year";
  }
  elsif ( $year <= 50 ) {
  	$year = "20$year";
  }
  $hour = "00" if $hour eq "";
  $minute = "00" if $minute eq "";
  $seconds = "00" if $seconds eq "";
  return "$year/$month/$day $hour:$minute:$seconds.000";
}


sub get_ix_dbc {
  my $IndexName = $_[-1];  my ($dbName, $dbc, $dbr); # get rid of className or self
  
  $dbr = dbExecute(
    "SELECT * from tbl_IndexTables WHERE IndexName = '$IndexName'"
  );

  if ( !$dbr->eof() ) {
    $dbName = $dbr->Fields()->Item('DatabaseName')->Value();
    $dbc = getHandle($dbName); 
    $dbr->Close(); 
  }
  else {
    die("UserError: INDEX \"$IndexName\" NOT DEFINED IN tbl_IndexTables")
  }

  return $dbc;
}

sub get_dbname {
  my $self = (ref($_[0])) ? shift : $dbc_xr; $self->CONNECT() if !$self->CONNECTED(); 

  my $dbc = $self->{'dbc'};

  my ($db_name) = $dbc->ConnectionString() =~ /(?:DATABASE|initial catalog)=([^;"]+)[;"]/i; 
  return $db_name;
}

sub get_dbname_2 {
  shift if scalar(@_) > 1; my $dbstr = ref($_[0]) ? $_[0]->{dbstr} : $_[0] ? $_[0] : "XREPORT";

  my $cstr_dbstr = c::getValues('dbase')->{$dbstr};
	
  if (!$cstr_dbstr) {
    die "ApplicationError: MISSING DB DEFINITION FOR $dbstr";
  }

  my ($db_name) = ($cstr_dbstr =~ /(?:DATABASE|initial catalog)=([^;"]+)[;"]/i); 
  return $db_name;
}

sub get_cfgdbname { return get_dbname_2(@_); }
 
sub closeIndexConnections {
  for (keys(%dbcTable)) {
    next; 
  }
}

sub getHandle {
  my $dbstr = $_[-1]; 
  return XReport::DBUtil->new(DBNAME=>$dbstr);
#  return $dbcTable{$dbstr} || XReport::DBUtil->new(DBNAME=>$dbstr);
}

sub push_dbxr {
  my ($className, %args) = @_; my $dbstr = uc($args{DBNAME}) || "XREPORT"; 
 
  my $cstr_dbstr = $XReport::cfg->{'dbase'}->{$dbstr};

  if (!$cstr_dbstr) {
    die "ApplicationError: MISSING DB DEFINITION FOR $dbstr";
  }
  
  if (exists($dbcTable{$dbstr}) and $dbcTable{$dbstr}) {
    my $self = {'dbstr' => $dbstr}; bless $self, $className; $dbcTable{$dbstr} = $self;
  }

  push @ldbc_xr, $dbc_xr; $dbc_xr = $dbcTable{$dbstr}; return $ldbc_xr[-1];
}

sub pop_dbxr {
  $dbc_xr = pop @ldbc_xr;
}

sub new {
  my ($className, %args) = @_; my $dbstr = uc($args{DBNAME} || "XREPORT");
  my @dblist = split /\t/, $main::Application->{'cfg.db.__keys'};
  die "ApplicationError: MISSING DB DEFINITION FOR $dbstr" unless grep /^$dbstr$/, @dblist;

  my $cstr_dbstr = $main::Application->{"cfg.db.$dbstr"};
  my $SrvName = $main::Application->{ApplName};
  (my $Provider) = $cstr_dbstr =~ /PROVIDER=([^;]+);/i; 
  $Provider = uc($Provider);

  return bless { dbstr => $dbstr, Provider => $Provider }, $className;

#  if ( exists($dbcTable{$dbstr}) and $dbcTable{$dbstr} ) {
#    return $dbcTable{$dbstr} 
#  }

#  my $cstr_dbstr = $XReport::cfg->{'dbase'}->{$dbstr};

#  if (!$cstr_dbstr) {
#    die "ApplicationError: MISSING DB DEFINITION FOR $dbstr";
#  }

#  my $self = {'dbstr' => $dbstr}; bless $self, $className; $dbcTable{$dbstr} = $self; return $self;
}

sub LoadTableFromTsv {
  my ($dbc, $FileName, $TableName, $sqlserv) = (shift, shift, shift, shift); 
  $FileName =~ s/\\/\//g;

#  print "DBC: ", ref($dbc), " FN: $FileName, TN: $TableName, SERV: $sqlserv\n";
  my $tsvfh = new FileHandle("<$FileName") or die "Unable to open File \"$FileName\" to load into $TableName";
  
  my (@tblcols, @rows) = ((), ());
  while ( <$tsvfh> ) {
    chomp;
    if (!scalar(@tblcols)) {
      @tblcols = split(/\t/, $_);
    }
    else {
      push @rows, "SELECT '"  . join("', '",  split /\t/) . "'";
      if ( scalar(@rows) > 299 or $tsvfh->eof() ) { 
	my $cmd = "INSERT INTO $TableName ([". join('], [', @tblcols) ."]) " ."\n" . join("\n UNION ALL ", @rows) . "\n";
	&$logrRtn("Starting to insert " . scalar(@rows) . " rows into $TableName");
	$dbc->dbExecute($cmd);
	@rows = ();
      }
    }
  }
  
  $tsvfh->close();
}

sub LoadTableFromLoH {
  my ($dbc, $listref, $TableName ) = (shift, shift, shift, shift); 

  die "reference not a pointer to an ARRAY - ", ref($listref) unless ref($listref) eq 'ARRAY';
  die "reference not a pointer to a valid list of hashes" unless ref($listref->[0]) eq 'HASH'; 

  my (@tblcols, @rows) = ((keys %{ $listref->[0] }), ());
  while ( scalar(@$listref) ) {
    my $rowref = shift @$listref;
#    my @rowsrefs = splice ( @$listref, 0, (scalar(@$listref) > 300 ? 300 : scalar(@$listref)));
    push @rows, "SELECT "  . join(", ",  map { (defined($rowref->{$_}) ? "'$rowref->{$_}'" : "NULL" ) } @tblcols );
    if ( scalar(@rows) > 299 or !scalar(@$listref) ) { 
      my $cmd = "INSERT INTO $TableName ([". join('], [', @tblcols) ."]) " ."\n" . join("\n UNION ALL ", @rows) . "\n";
#      &$logrRtn(ref($dbc) ." requesting T-SQL to insert " . scalar(@rows) . " rows into $TableName");
      my $rs = $dbc->dbExecute_NORETRY($cmd);
#      $rs->Update();
#      $rs->Close();
      @rows = ();
#      &$logrRtn(ref($dbc) ." T-SQL returned after update on $TableName");
    }
    
  }
  
}



$dbc_xr = XReport::DBUtil->new(DBNAME =>'XREPORT');

#------------------------------------------------------------

package XReport::DBUtil::RECORDSET;

use XReport::Util;
use Win32::OLE::Variant;
#$Win32::OLE::Variant::LCID = $Win32::OLE::LCID;

sub Variant_TimeStamp {
  return variant(VT_DATE, shift);
}

sub Variant_DateTime {
  #return $_[0]->Date('yyyy/MM/dd')." ".$_[0]->Time('hh:mm:ss tt'); # 12 hours clock and AM/PM
  return $_[0]->Date('yyyy/MM/dd')." ".$_[0]->Time('HH:nn:ss'); # 24 hours clock
}

sub newBinaryVariant {
  return Win32::OLE::Variant->new( Win32::OLE::Variant::VT_UI1() | Win32::OLE::Variant::VT_ARRAY(), shift );
}

sub variantBinary {
  return Win32::OLE::Variant->new(VT_UI1, shift);
}

sub _retrieveBlob {
  my $fld = shift;
  my($buffer, $blob_size, $read_size, $bytes_to_read) = ('', $fld->{ActualSize}, 0, 4*1024);
  main::debug2log("retrieveBlob: blob_size: $blob_size");

  $bytes_to_read = $blob_size unless $blob_size > $bytes_to_read;

  while($read_size < $blob_size) {
#    my $value = $fld->GetChunk($bytes_to_read);
#    my $img = Win32::OLE::Variant->new( VT_UI1, $value );
#    print localtime() . " read blob: value len:". length($value) . " img len:". length($value) . " bytes to read: $bytes_to_read - value: ". unpack("H40", $img) ."\n"; 
    
    $buffer .= Win32::OLE::Variant->new( VT_UI1, $fld->GetChunk($bytes_to_read) );
#    print localtime() . "write blob: $bytes_to_read blob_size:". length($buffer). "\n"; 
    $read_size += $bytes_to_read;
    if($read_size+$bytes_to_read > $blob_size) {
      $bytes_to_read = $blob_size - $read_size; 
    }
  }
  return $buffer;
}

sub _storeBlob {
  
  my ($fld, $data_size) = @_[0,2];
  my $data = ( ref($_[1]) ? $_[1] : new IO::String($_[1]) );
  my ($write_size, $bytes_to_write) = (0, 4*1024);
  $bytes_to_write = $data_size unless $data_size > $bytes_to_write;
  main::debug2log("storeBlob: blob_size: $data_size data: ". unpack("H40", $data));

  while($write_size < $data_size) {
    my $bnum = $data->read(my $buffer, $bytes_to_write);
#    print localtime() . "write blob: ". length($buffer). "\n"; 
    $fld->AppendChunk($buffer);
    $write_size += $bytes_to_write;
    if($write_size+$bytes_to_write > $data_size) {
      $bytes_to_write = $data_size - $write_size; 
    }
  }
  return $write_size;
}

sub GetFieldsValues {
  my $dbr = shift->[0]; my ($fields, $field, $field_value); my @rs = (); 
  
  $fields = $dbr->Fields();
  
  for (@_) {
    $field = $fields->Item($_); $field_value = $field->Value();
    if (  ref($field_value) ) {
      $field_value = [];
      push @$field_value, $fields->Item($_)->Value()->Date('yyyy-MM-dd') if $fields->Item($_)->Value()->can('Date');
      push @$field_value, $fields->Item($_)->Value()->Time('HH:mm:ss') if $fields->Item($_)->Value()->can('Time');
      push @$field_value, $fields->Item($_)->Value() unless scalar(@$field_value);
      $field_value = join(' ', @$field_value);
    }

#    if (ref($field_value) && $field->Type() == 135) {
#      $field_value = Variant_DateTime($field_value);
#    }
    push @rs, $field_value;
  }
  return wantarray ? @rs : $rs[0];
}

sub ImportFieldsValues {
  my ($self, $dbr) = (shift, shift); $self->SetFieldsValues(%{$dbr->GetFieldsHash()});
}

sub GetFieldsHash {
  my $self = shift; my $dbr = $self->[0]; my ($fields, $field, $field_value); my %rs = (); 

  return $self->GetFieldsHash($self->GetFieldsNames()) if !@_;
  
  $fields = $dbr->Fields();
  
  for (@_) {
    $field = $fields->Item($_); $field_value = $field->Value();
  
    if (  ref($field_value) ) {
      $field_value = [];
      push @$field_value, $fields->Item($_)->Value()->Date('yyyy-MM-dd') if $fields->Item($_)->Value()->can('Date');
      push @$field_value, $fields->Item($_)->Value()->Time('HH:mm:ss') if $fields->Item($_)->Value()->can('Time');
      push @$field_value, $fields->Item($_)->Value() unless scalar(@$field_value);
      $field_value = join(' ', @$field_value);
    }

#    if (ref($field_value) && $field->Type() == 135) {
#      $field_value = Variant_DateTime($field_value);
#    }
    $rs{$field->Name()} = $field_value;
  }

  return \%rs;
}

sub GetFieldsNames {
  my $dbr = shift->[0]; my @t = (); my $fields = $dbr->Fields(); 

  for (0..$fields->Count()-1) { push @t, $fields->Item($_)->Name(); } return @t;
}

sub SetFieldsValues {
  my $dbr = shift->[0];
  while(@_) {
    my ($k, $v) = (shift, shift);
    $dbr->Fields->Item($k)->{Value} = $v;
  }
}

sub get_dbname {
  my $self = shift; 
  my $dbc = $self->ActiveConnection();
  my ($db_name) = ($dbc->ConnectionString() =~ /(?:DATABASE|initial catalog)="?([^;"]+)[;"]/i); 
  return $db_name;
}

sub Fields {
  $_[0]->[0]->Fields();
}

sub MoveFirst {
  $_[0]->[0]->MoveFist();
}

sub MoveNext {
  $_[0]->[0]->MoveNext();
}

sub MoveLast {
  $_[0]->[0]->MoveLast();
}

sub Sort {
  my $self = shift; my ($dbr, $dbc) = @$self[0,1]; my $rc;

  $rc = $dbr->Sort(@_);

  if ( Win32::OLE->LastError() ) {
    i::logit("Recordset Sort\n". Win32::OLE->LastError()); 
    die("DBERROR: Exiting ON DataBase ERROR !!!");
  }

  return $rc;
}

sub AddNew {
  my $self = shift; my ($dbr, $dbc) = @$self[0,1]; my $rc;

  $rc = $dbr->AddNew(@_);

  if ( Win32::OLE->LastError() ) {
    i::logit("Recordset AddNew\n". Win32::OLE->LastError()); 
    die("DBERROR: Exiting ON DataBase ERROR !!!");
  }

  return $rc;
}

sub Update {
  my $self = shift; my ($dbr, $dbc) = @$self[0,1]; my $rc;

  $rc = $dbr->Update(@_);

  if ( Win32::OLE->LastError() ) {
    i::logit("Recordset Update\n". Win32::OLE->LastError()); 
    die("DBERROR: Exiting ON DataBase ERROR !!! - " .Win32::OLE->LastError());
  }

  return $rc;
}

sub Delete {
  my $self = shift; my ($dbr, $dbc) = @$self[0,1]; my $rc;

  $rc = $dbr->Delete(@_);

  if ( Win32::OLE->LastError() ) {
    i::logit("Recordset Delete\n". Win32::OLE->LastError()); 
    die("DBERROR: Exiting ON DataBase ERROR !!!");
  }

  return $rc;
}

sub eof {
  $_[0]->[0]->eof();
}

sub Close {
  $_[0]->[0]->Close();
}

sub ActiveConnection {
	return $_[0]->[1];
}

sub new {
  my ($className, $dbr) = (shift, shift);

  my $self = [$dbr, $dbr->ActiveConnection()];
  
  bless $self, $className;
}

*getValues = *GetFieldsValues; *setValues = *SetFieldsValues;

1;

