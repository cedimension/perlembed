package XReport::Receiver;
#######################################################################
# @(#) $Id: Receiver.pm 2176 2008-02-27 16:15:49Z Administrator $ 
#
# Copyrights(c) XSOFT s.r.l.
#######################################################################

use strict;
use Carp;

use IO::Socket;
use IO::Select;

use constant SOCKBUFL                => 16384;

use XReport::ENUMS qw(:XF_ENUMS);
use XReport::Util;
use XReport::JUtil qw();
use XReport::QUtil qw();
use XReport::ARCHIVE qw();
use XReport::Storage::IN;

BEGIN {
  use Exporter ();
  our ($VERSION, @ISA, @EXPORT, @EXPORT_OK, %EXPORT_TAGS);
  
  $VERSION = do { my @r = (q$Revision: 1.5 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r }; 

  @ISA = qw(Exporter);
  
  @EXPORT = qw( );
  
  @EXPORT_OK = qw( );
  
  %EXPORT_TAGS = (  );
  
}
#our @EXPORT_OK;  

$main::debug = 0;

sub DoLog {
  my $self = shift;
  my $dolog = $self->{'logrtn'};
  print @_, "\n";
  &$dolog(@_);
}

sub logDebug {
    return unless ($main::debug and $main::debug == 1);
  my $self = shift;
  return $self->DoLog(@_);
}

sub new { 
    my $class = shift;
    bless { logrtn => sub {
                            my $flag = 0;
                            if ($_[0] eq "N2O") {
                              shift;
                              $flag = 1;
                            }
#                           warn "XRC:", @_, "\n" if ($main::debug == 1 or $flag == 1);
                            return;
                          },
            @_
           }, $class; 
}

sub LogStruct {
  my ($self, $hndl, $descr) = (shift, shift, shift);
  foreach my $nm ( keys %{ $hndl } ) {
    my $val = ($hndl->{$nm} ? $hndl->{$nm} : ":UNDEF:");
    $self->DoLog(ref($self)." hndl.$descr $nm:" . $val) unless $nm =~ /cdata|ackstr|cinfo/i;
#    $self->DoLog("XRG hndl.$descr $nm:" . $val) unless $nm =~ /cdata|ackstr|sdata/i;
    $self->DoLog(ref($self)." hndl.$descr $nm:" . unpack("H*",$val) ) if ($nm eq 'ackstr');
  }
}

sub SetUpSock {
  my ($self, $sock) = (shift, shift);
#  my $sock = $self->{'sock'};
  $sock->sockopt( SO_RCVBUF, pack("l", SOCKBUFL) )  || croak "Setsockopt error: $!";
  $sock->autoflush( 1 );
  $self->{'sock'} = $sock;
  $self->{'sel'} = new IO::Select( $self->{'sock'} );
  
  @{$self}{qw(peeraddr peerport localip loclport)}= ( $sock->peerhost(), 
                              $sock->peerport(),
                              $sock->sockhost(),
                              $sock->sockport(),
                            );
  
  $self->DoLog("Serving connection from $self->{peeraddr} ($self->{peerport})");
  
  return $sock;
}

sub Port {
  my ($self, $uval) = (shift, shift);
  $self->{'port'} = $uval if $uval;

  return $self->{'port'};
}

#--------------------------------------------------------------------
# This should init the handle buckets for sockets
#--------------------------------------------------------------------

sub Sock {
  my ($self, $uval) = (shift, shift);
  $self->{'sock'} = $uval if $uval;

  return undef unless $self->SetUpSock( $self->{'sock'} );

  return $self->{'sock'};
}

use POSIX ();
use Time::HiRes ();

#-- TCP Listener
use IO::Socket;
use IO::Select;

sub TCPListener {

  if (!$main::SrvSock) {
    $main::SrvSock = new IO::Socket::INET(Listen => 5, 
                       LocalPort => $main::SrvPort,
                       LocalAddr => $main::SrvAddr,
                       Proto => 'tcp', 
                       Reuse => 1) || &$logrRtn("unable to listen on $main::SrvPort - $!");
    &$logrRtn("$^O server $main::SrvName Started - HOME set to $ENV{XREPORT_HOME}...") if $main::SrvSock;
  }
  die "TCP Socket unavailable - process aborted" unless $main::SrvSock;  
  
  &$logrRtn("XREPORT_HOME set to ".$ENV{'XREPORT_HOME'});
  &$logrRtn("Listener ready to accept requests on port $main::SrvPort");

  my ($sock, $Cli, $currSt);
  my $sel = new IO::Select( $main::SrvSock ) || die "Unable to set Socket Selector - process aborted";

  &$logrRtn("now entering main loop");
  my $con = 1;
  while ( 1 ) {
    $sock = $sel->can_read(15);
    next unless ($sock);
    $Cli = $main::SrvSock->accept() || &$logrRtn("unable to accept connection - $! - $?");
    if ($Cli) {
      $Cli->autoflush( 1 ); 
      my ($cliport, $iaddr) = sockaddr_in(getpeername($Cli));
      my $peer = inet_ntoa($iaddr);
#      &$logrRtn($main::SrvName . " accepted connection from " . $peer);
      my $t0 = Time::HiRes::gettimeofday();
      my $localTime = POSIX::strftime("%Y-%m-%dT%H:%M:%S", localtime(int($t0))).sprintf(".%03d", int(($t0-int($t0))*1000));
      XReport::SUtil::spawnProcess($0, 
                   "-N", $main::SrvName, 
                   ($main::daemon eq 0 ? '-_' : '-@'), 
                   "-P", "CHILD", 
                   "$$", 
                   XReport::SUtil::getFileToken($Cli), 
                   $con++, $localTime
                  );
    } else {
      last;
    }
  }
  &$logrRtn("now leaving main loop: $currSt ", (($sock) ? "Cli pending" : "Cli Exhausted"));
  $main::SrvSock->close();
  return 0;
}

sub processRequest {
 #TODO chkattrib: inserire supporto per diversi tipi di connettori (TCP, MQ, FILE EVENT, etc..)
 #$main::exetype = \&Server;
  $main::exetype = \&XReport::Receiver::TCPListener;
  $main::con     = 0;
  $main::daemon  = 0;
  $main::prId    = $$;
  
  $main::debug   = 0;
  my $xrcfg = $XReport::cfg;
  $main::debug = $xrcfg->{debug} if exists($xrcfg->{debug});
  
  foreach my $i (0 .. $#ARGV) {
    if ($ARGV[$i] eq "-P") { # execution type 
      if ($ARGV[$i+1] =~ /^CHILD/) {
         $main::exetype = main->can('Child') || die "Child Routine not found in $0 - process aborted";
         XRprId($main::prId = "$$(".$ARGV[$i+2].")");
         $main::CliFno  = $ARGV[$i+3];
         $main::con     = $ARGV[$i+4];
         my $startReqTime = $ARGV[$i+5];
         $main::CliSock = new IO::Socket::INET();
         &$logrRtn("CHILD Start begin to serve prid $main::prId CLIFno: $main::CliFno con: $main::con requested at $startReqTime");
         XReport::SUtil::openFileToken($main::CliSock, $main::CliFno) || die("Client Socket OPEN ERROR <$!>\n");
      }
    }
    elsif ($ARGV[$i] eq '--debug' or $ARGV[$i] eq '-d' or $ARGV[$i] eq '-dd' ) {
      $main::debug = 1;
      $main::veryverbose = 1 if $ARGV[$i] eq '-dd'
    }
  }
  
  InitServer('isdaemon' => 1); 
  
  $main::SrvName = $XReport::cfg->{SrvName};
  
  ($main::daemon, $main::SrvPort, $main::msgmax, $main::WorkClass, $main::SrvAddr) = 
    getConfValues(qw(daemonMode port maxdata WorkClass addr));
  $main::daemon = 1 unless defined($main::daemon);
  $main::SrvAddr = '0.0.0.0' unless $main::SrvAddr;
  $main::SrvPort = 9515 unless $main::SrvPort;
  
  return &$main::exetype();
}

sub ParseQue {
  my ($self, $string) = (shift, shift);
  my ($reqname, $rpttype, $reportname) = ($string, split(/\./, $string, 2)) if ($string);

  ($reportname, $rpttype) = ($rpttype, '.default')  unless $reportname;

  return @{$self->{'cinfo'}}{qw(reqname rpttype reportname)} = ($reqname, $rpttype, $reportname);
}

sub parseRptConf {
#  use XML::Simple;
  use Data::Dumper;

  my ($request, $xml) = (shift, shift);
  my $newvals = {};
#  my $debug = 0;
  return $newvals unless ( $xml && ($xml =~ /^</ or -e $xml) );
    my $xmlconf = c::parseXmlConfig($xml);
    my ($ck) = grep {/Receiver/i} (keys %$xmlconf);
    my ($xrcfgk) = grep /Receiver/i, keys %{$XReport::cfg}; 
    return $newvals unless ($ck || $xrcfgk);
   
    my $conf = ($ck ? c::mergeStruct($XReport::cfg->{$xrcfgk}, $xmlconf->{$ck}) : $XReport::cfg->{$xrcfgk});
    $xmlconf = undef;

#  my $conf = c::mergeWithConfig($xml, 'receiver');
#  my $conf = XMLin($xml, ForceArray => ['CASE', 'ASSERT']);
#  my ($ck) = grep {/Receiver/i} (keys %$conf);

#  return $newvals unless ( $conf );
 
#  $conf = $conf->{$ck};
  $main::debug && &$main::logrRtn("CONF: ", Dumper($conf), "");

  if ( exists($conf->{DEBUG}) && $conf->{DEBUG}) {
    $main::debug = 1;
#    $request->DoLog("Config" . "-" x 60, "\n",
#         Dumper($conf), "\n",
#          "-------------" . "-" x 50);
    $request->DoLog("Request Infos" . "-" x 50, "\n",
          Dumper($request), "\n",
          "-------------" . "-" x 50);
  }

  if ( exists($conf->{SELECT}) ) {
    my $testval0; eval  "\$testval0  = \"$conf->{SELECT}\";";
    my $matchval0; eval "\$matchval0 = \"$conf->{MATCH}\";";
    my $testre0 = qr/$matchval0/;
    if (!$testval0 or $testval0 !~ /$testre0/) {
      $main::debug && $request->DoLog("SELECT $testval0 NOT MATCH $testre0");
      return $newvals;
    }
  }
  
    my $casecnt = 0; my @matched = ();
    foreach my $case (@{$conf->{CASE}}) {
        $casecnt++;
        my $testval; eval "\$testval = \"$case->{ATTRIB}\";";
        my $matchval; eval "\$matchval = \"$case->{MATCH}\";";
        my $testre = qr/$matchval/;
        $main::debug 
           && $request->DoLog("case item $casecnt - Value to be checked: $testval ($case->{ATTRIB}) - matching value: $testre ($case->{MATCH})");
        next if $testval and $testval !~ /$testre/;
        return undef if (exists($case->{REJECT}) && $case->{REJECT} );
        push @matched, $casecnt;
        foreach my $assert ( @{$case->{ASSERT}} ) {
            foreach my $akey ( keys %$assert ) {
                my $nvkey = (
                    $akey =~ /reportname/i ? 'reportname' :
                    $akey =~ /recipient/i  ? 'XferRecipient' :
                    $akey =~ /typeofwork/i ? 'TypeOfWork' :
                    $akey =~ /workclass/i  ? 'WorkClass' :
                    $akey =~ /datetime/i   ? 'XferStartTime' :
                    $akey =~ /cinfo/i      ? 'usrcinfo' :
                    $akey
                    );
                next if exists($newvals->{$nvkey});
                my $cod = $assert->{$akey};
                $cod = '"'.$cod.'"' if $assert->{$akey} !~ /^[^\(]+\([^\)]+\)$/;
                $main::debug && $request->DoLog("Setting key $nvkey eval'ing $cod");
                eval "\$newvals->{\$nvkey} = $cod;";
            }
        }
    }

  if ( $main::debug ) {
    $request->DoLog("New values matched in cases (", join(', ', @matched), ")");
    $request->DoLog("----------", "-" x 60, "");
    $request->DoLog(Dumper($newvals));
    $request->DoLog("----------", "-" x 60, "");
  }

  return $newvals;
}

sub SetReportName {
  my ($self, $string) = (shift, shift);
  my ($rpttype, $reportname) = split(/\./, $string, 2) if ($string);
#print 'aa==>',$rpttype,'::',$reportname,"<==\n";
  ($reportname, $rpttype) = ($rpttype, '.default')  unless $reportname;
 #print 'bb==>',$rpttype,'::',$reportname,"<==\n";
  my ($vkey) = (grep {/reportxml/i} (keys %$XReport::cfg))[0];
  $rpttype = $XReport::cfg->{$vkey} if $vkey;

 #  use Data::Dumper;
 #  print "CONFIG:\n", Dumper($XReport::cfg), "\n";
  my $confdir = $XReport::cfg->{confdir};
  $confdir = $XReport::cfg->{userlib}."/rptconf" unless $confdir && -d $confdir;
  $main::debug && $self->DoLog("seeking $rpttype or $reportname in \"$confdir\" - ");
  my $rptconf = ( -f $confdir.'/'.$reportname.'.xml' ? $confdir.'/'.$reportname.'.xml' : 
          -f $confdir.'/'.$rpttype.'.xml' ? $confdir.'/'.$rpttype.'.xml' : 
          undef);
 #TODO chkattrib: merge rptfconf con XReport/service configuration
  $main::debug && $self->DoLog("Input Config File is: \"$rptconf\"") if $rptconf;
  my $override = $self->parseRptConf($rptconf) if ( $rptconf );
 $main::debug && $self->DoLog("Input Config File \"$rptconf\" Parsed") if $rptconf;
 #TODO chkattrib: spostare qui logica di CHECK/CASE/ASSERT/REJECT  
 #  return undef unless $override;

  @{$self->{'cinfo'}}{qw(reqname rpttype reportname)} = ($string, $rpttype, $reportname);
  $self->{'cinfo'} = { %{$self->{'cinfo'}}, %$override } if $override;
  return @{$self->{'cinfo'}}{qw(reqname rpttype reportname)};
}

sub ParseFN {
  my ($self, $filen) = (shift, shift);
  return $filen;
}

sub FillBuff {
  my ($self, $string) = (shift, shift);
  $self->{'dbuff'} .= $string if $string;
  return $self->{'dbuff'};
}

sub CtlStream2xml {
  my ($self, $string) = (shift, shift);
  $self->{'cdata'} = $string if $string;
  foreach ( keys %{ $self->{'cinfo'} } ) {
    $string .= (
            /^([HPJCLfUN])$/ ? $_.$self->{'cinfo'}->{$_} :
            /^.$/            ? '-o'.$_.'='.$self->{'cinfo'}->{$_} :
            '-oJ'.$_.'='.$self->{'cinfo'}->{$_}
           )."\n";
  
  } 
  return $string;
}

sub CtlStream {
  my ($self, $string) = (shift, shift);
  $self->{'cdata'} = $string if $string;
  foreach ( keys %{ $self->{'cinfo'} } ) {
    $string .= (
            /^([HPJCLfUN])$/ ? $_.$self->{'cinfo'}->{$_} :
            /^.$/            ? '-o'.$_.'='.$self->{'cinfo'}->{$_} :
            '-oJ'.$_.'='.$self->{'cinfo'}->{$_}
           )."\n";
  
  } 
  return $string;
}

sub write {
    return shift->{outfh}->write(@_);
}

sub Eof {
 my ($self, $flag) = (shift, shift);
 $self->{'eof'} = $flag if $flag;
 return $self->{'eof'};
}

sub dbytes {
  my ($self, $cnt) = (shift, shift);
  $self->{'dbytes'} = $cnt if $cnt;
  return $self->{'dbytes'};
}

sub wbytes {
  my ($self, $cnt) = (shift, shift);
  $self->{'wbytes'} = $cnt if $cnt;
  return $self->{'wbytes'};
}

sub outbytes {
  my ($self, $cnt) = (shift, shift);
  $self->{'outbytes'} = $cnt if $cnt;
  return $self->{'outbytes'};
}

#sub _innerhash {
#  my ($self, $struct) = (shift, shift);
#  my $xx = { @_ };

#  if ( scalar(keys %$xx) ) {
#    foreach my $kknm ( keys %{ $xx } ) {
#      $self->{$struct}->{$kknm} = $xx->{$kknm};
#    }
#  }     
  
#  return $self->{$struct};
  
#}

#sub sdata {
#  my $self = shift;
#  return $self->_innerhash('sdata', @_);
#} 

#sub cinfo {
#  my $self = shift;
#  return $self->_innerhash('cinfo', @_);
#} 

sub addNewRequest {

  my $hndl = shift;

  my ($SrvName, $conid, $prId, $peeraddr) = @{$hndl}{qw(SrvName conid prId peeraddr)};
  my ($JobOrigin, $JobName, $JobNumber, $JobExecutionTime) = @{$hndl->{'cinfo'}}{qw(JobOrigin JobName JobNumber CRDTIM)};
  my($reportname, $rpttype, $remotefile, $xfermode) = @{$hndl->{'cinfo'}}{qw(reportname rpttype RemoteFileName XferMode)}; 
  my $xferstart = (exists($hndl->{XferStartTime}) ? $hndl->{XferStartTime} : 'GETDATE()');

  $hndl->DoLog("$SrvName serving $JobName ($JobNumber) conid: $conid prId: $prId xm: $xfermode st: $CD::stAccepted xferstart: $xferstart");
  my ($reqid, $TargetLocalPathId_IN) = QCreate XReport::QUtil(
                     SrvName        => $SrvName,
                     JobReportName  => $reportname,
                     RemoteFileName => $remotefile,
                     LocalFileName  => $rpttype . $$ . $conid,
                     RemoteHostAddr => $peeraddr,
                     XferStartTime  => $xferstart,
                     XferMode       => $xfermode,
                     XferDaemon     => $SrvName,
                     JobOrigin      => $JobOrigin,
                     JobName        => $JobName,
                     JobNumber      => $JobNumber,
                     JobExecutionTime => $JobExecutionTime,
                     Status         => $CD::stAccepted,
                     XferId         => $prId,
                     );
  if ( !$reqid ) {
    $hndl->DoLog("QCreate Failure - receiver aborting");
    return undef;
  }

  @{$hndl}{qw(dbid LocalPathId_IN)} = ( $reqid, $TargetLocalPathId_IN );
  return $reqid;
}

sub setXferFileA {
  my $hndl = shift;
  my ($rpttype, $reportname, $progr, $datetime) = (shift, shift, shift, shift);
#  my $PathId = shift || 'L1';
  my $PathId = shift || '';
#print 'xferfilea ==>', join('::', @_), "<==\n";

  $datetime = GetDateTime() unless ($datetime && $datetime =~ /^\d{14}$/);
  my ($curryear, $currday, undef) = unpack("a4a4a*", $datetime);

#  my $xrspool = getConfValues('LocalPath')->{$PathId}."/IN";
#  $xrspool =~ s/^file:\/\///;
#
#  if (! -e $xrspool."/".$curryear."/".$currday ) {
#    mkdir $xrspool."/".$curryear if (! -e $xrspool."/".$curryear);
#    mkdir $xrspool."/".$curryear."/".$currday;
#    die "unable to create $xrspool $curryear $currday dir\n" unless ( -e $xrspool."/".$curryear."/".$currday )
#  }

  my $filen = "$reportname.$datetime.$progr";
  $hndl->DoLog("Stream Archive Name will be $filen");
  my $tgt;
  eval { $tgt = XReport::ARCHIVE::newTargetPath($PathId, "IN/$curryear/$currday"); }; #, $hndl->{'logrtn'}); };
  if ( $@ ) {
  	my $archmsg = $@;
  	$hndl->DoLog("Target Path Error: $archmsg");
  	die "$archmsg";
  }
  $hndl->{LocalPathId_IN} = $tgt->{LocalPathId};
  $hndl->DoLog("Stream Data will be stored in $tgt->{fqn}");
  
  my @xrspool = split /(?<![\/\\])[\/\\](?![\/\\])/, $tgt->{fqn};
  
 # return ($curryear."/".$currday."/".$filen, $xrspool, $datetime);
   return (join('/', splice(@xrspool, -2))."/".$filen, join('/', @xrspool), $datetime);
}

sub setRequestOK {
  my ($hndl, $code, $cntlfile, $datafile) = (shift, shift, shift, shift);

  my ($SrvName, $conid, $prId, $peeraddr, $pagcnt) = @{$hndl}{qw(SrvName conid prId peeraddr pagcnt)};
  my ($dbid, $dfilen, $dfilesz, $databytes, $outpdir) = @{$hndl}{qw(dbid dfilen dfilesz dbytes dbodir)};
  my ($JobOrigin, $JobName, $JobNumber, $JobExecutionTime) = @{$hndl->{'cinfo'}}{qw(JobOrigin JobName JobNumber CRDTIM)};
  my($reportname, $rpttype, $remotefile, $xfermode) = @{$hndl->{'cinfo'}}{qw(reportname rpttype RemoteFileName XferMode)}; 

  # my $CtlStream = $hndl->CtlStream();
  my $cinfo = $hndl->{'cinfo'};

  $hndl->{Recipient} = '';
  $hndl->{Recipient} = $hndl->{cinfo}->{XferRecipient} if exists($hndl->{cinfo}->{XferRecipient});
  my $Recipient = $hndl->{Recipient};

  $hndl->{XferDestArea} = $hndl->{cinfo}->{XferDestArea} if exists($hndl->{cinfo}->{XferDestArea});
#  open(my $fhOut, ">$cntlfile") || do {
#    $hndl->DoLog("CNTL FILE OPEN ERROR: $!");
#    return undef;
#  };
#  binmode $fhOut;
#  my $wb = syswrite($fhOut, $CtlStream);
#
#  close($fhOut);
#
#  return undef if ($wb ne length($CtlStream));
#
  (my $ofil = $dfilen) =~ s/^$outpdir\///; 
  $ofil = join("/", (split(/[\/\\]/, $datafile))[-3..-1]);
  if (!$dfilesz && -e "$outpdir/$ofil.tar") {
     $dfilesz = -s "$outpdir/$ofil.tar" ;
     $hndl->{dfilesz} = $dfilesz;
  }
  $JobName = $cinfo->{'J'} unless $JobName;
  $JobName .= '.'.$cinfo->{STEPN} if (exists($cinfo->{STEPN}));
  $JobName .= '.'.$cinfo->{DDNAM} if (exists($cinfo->{DDNAM}));

  my %tbvals = (
                   XferEndTime    => ($cinfo->{XferEndTime} || 'GETDATE()'),
                   Status         => $code,
                   LocalPathId_IN => $hndl->{LocalPathId_IN},
                   JobName        => $JobName,
                   JobNumber      => ($JobNumber || substr($cinfo->{U},3,8)),
                   JobReportName  => $reportname,
                   XferRecipient  => $Recipient,
                   RemoteFileName => ($remotefile || $cinfo->{N}),
                   LocalFileName  => $ofil.'.tar',
                   XferInPages    => ($cinfo->{'PAGCNT'} || 0),
                   XferInLines    => ($cinfo->{'LINCNT'} || 0),
                   XferInBytes    => $databytes,
                                   (($reportname ne 'CDAMFILE' && $databytes && $databytes > 100000000) ? ( WorkClass => 128 ) : ()),
                   XferOutBytes   => $dfilesz,
                   XferMode       => $xfermode,
                   Id             => $dbid,
                   (exists($cinfo->{HoldDays}) ? (HoldDays => $cinfo->{HoldDays}) : ()),
                   (exists($cinfo->{HasIndexes}) ? (HasIndexes => $cinfo->{HasIndexes}) : ()),
                   (exists($cinfo->{OrigJobReportName}) ? (OrigJobReportName => $cinfo->{OrigJobReportName}) : ()),
                  );
  $tbvals{UserTimeRef} = $hndl->{UserTimeRef} if exists($hndl->{UserTimeRef});
  $tbvals{UserRef} = $hndl->{UserRef} if exists($hndl->{UserRef});
  $tbvals{XferDestArea} = $hndl->{XferDestArea} if exists($hndl->{XferDestArea});

  $hndl->DoLog("$SrvName store of \"$ofil\" ($dfilesz bytes) completed into \"$outpdir\" - updating Jobreports");
  $main::debug && $hndl->DoLog("$SrvName QUTIL vals:", Dumper(\%tbvals));

  my $req = QUpdate XReport::QUtil( %tbvals );

  return $req;
}

sub setRequestErr {
  my ($hndl, $errmsg) = (shift, shift);
  $hndl->DoLog($errmsg) if $errmsg;
  
  my ($databytes, $dbid) = @{$hndl}{qw(dbytes dbid)};
  my $reportname = $hndl->{cinfo}->{reportname};
  $hndl->{XferEndTime} = 'GETDATE()' unless exists($hndl->{XferEndTime});
  XReport::QUtil->QUpdate(
              XferEndTime    => $hndl->{XferEndTime},
              Status         => $CD::stRecvError,
              ReportName     => $reportname,
              XferInBytes    => $databytes,
              Id             => $dbid,
             );
  
}

sub initJobReport {
  my $hndl = shift;
  $hndl->DoLog("Entering initJobReport");
  if ( $main::globals->{XRDBASE} ) {
    $hndl->{JCH} = $main::globals->{XRDBASE};
  }
  else {
    $hndl->{JCH} = new XReport::JUtil();
  }
  my $dbc = $hndl->{JCH};
  my $cinfo = $hndl->{'cinfo'};  

  if (my $oldid = $dbc->JCExist($hndl->CtlStream())) {
    $hndl->DoLog(my $msg = "$cinfo->{JOBNM} ($cinfo->{OJBID}) appears to be duplicated ($oldid)- process terminated");
    return $hndl->notifyError($msg);
  }

  #TODO chkattrib: add handling of different queue types
  my $confdir = getConfValues('confdir');
  my $rptconf = $confdir.'/'.$hndl->{cinfo}->{reportname}.'.xml';
  $hndl->DoLog("Checking rptconf $rptconf");
  # TODO chkattrib: merge rptconf with XReport/service config
  my $override = -f $rptconf ? $hndl->parseRptConf($rptconf) : {};

  if (!defined($override) ) {
  $hndl->DoLog(my $msg = "$cinfo->{JOBNM} ($cinfo->{OJBID}) rejected by rptconf - process terminated");
  return $hndl->notifyError($msg)
  }

  $hndl->{'cinfo'} = { %{$hndl->{'cinfo'}}, %$override } if $override;
  ##mpezzi add handling of different queue types
  
#  @{$hndl}{qw(JobOrigin JobName JobNumber JobExecutionTime)} = @{$cinfo}{qw(ORIGIN JOBNM OJBID CRDTIM)};
  
  my $reqid = $hndl->addNewRequest() or return undef;
  my ($SrvName, $conid, $prId, $peeraddr) = @{$hndl}{qw(SrvName conid prId peeraddr)};
  
  $hndl->{'dbid'} = $reqid;
  $hndl->DoLog("Added $reqid to Work Queue for $cinfo->{JOBNM} ($cinfo->{OJBID})");
  
  (my $xferstart = (exists($hndl->{XferStartTime}) ? $hndl->{XferStartTime} : '')) =~ s/[\-\s\.\:]//g; 
  my ($filen, $outpdir, $datetime) 
      = $hndl->setXferFileA((@{$hndl->{'cinfo'}}{qw(rpttype reportname)}), $reqid, $xferstart, $hndl->{LocalPathId_IN});
#print 'dd==>>', join('::', ($filen, $outpdir, $datetime)), "<==\n";
  @{$hndl}{qw(dfilen dbodir dbdtime)} = ($filen, $outpdir, $datetime);
  
  my $datafile = $outpdir."/".$filen.".DATA.TXT.gz";
  my $md = $hndl->{'cinfo'}->{'XferMode'};
  my $pars_sfx = $md == XReport::ENUMS::XF_PSF ? 'PSF'
                        : $md == XReport::ENUMS::XF_AFPSTREAM ? 'AFPSTREAM'
                        : $md == XReport::ENUMS::XF_ASCII ? 'LPR'
                        : $md == XReport::ENUMS::XF_LPR ? 'LPR'
                        : $md == XReport::ENUMS::XF_FTPB ? 'FTPB'
                        : $md == XReport::ENUMS::XF_FTPC? 'FTPC'
                        : 'STREAM';
  if ( $md == XReport::ENUMS::XF_BINSTREAM && (!exists($hndl->{cinfo}->{FIXRecfm}) || !exists($hndl->{cinfo}->{LRECL})) ) {
     $hndl->DoLog("BINSTREAM sets FIXRecfm and LRECL 65535");
     $hndl->{cinfo}->{FIXRecfm} = 1;
  	 $hndl->{cinfo}->{LRECL} = 65535; 
  }                      
  $hndl->DoLog("Locating RECEIVER record parser for MODE $pars_sfx");
  my $recordparser = XReport::Storage::IN->can('_fix'.$pars_sfx);
  
  die "record handler not found for $pars_sfx (ENUM $md) " unless ref($recordparser) eq 'CODE';
  my $fhOut = XReport::Storage::IN
              ->Create($datafile, 'cinfo' => $hndl->{cinfo}, 'recordparser' => $recordparser )
                    || do {
                        $hndl->setRequestErr("DATA FILE OPEN ERROR for $datafile - $!");
                        return undef;
                        };
  die "record handler not found for $hndl->{'cinfo'}->{'XferMode'} " unless $fhOut->{recordparser};
  $hndl->DoLog("$SrvName starting to store $cinfo->{JOBNM} ($cinfo->{OJBID}) data into "
           . $filen .".DATA.TXT.gz (" . $hndl->{LocalPathId_IN} . ")");
  @{$hndl}{qw(outfh dfilen)} = ($fhOut, $datafile);
  return $fhOut;
}

sub Close {
       my ($self, $data, $errmsg) = (shift, shift, shift);
       return undef unless $self->{outfh}; 
       my $nbytes = 0;
       $nbytes = $self->{outfh}->write( $data ) if $data && length($data);
       $self->{dbytes} += $nbytes;
       $self->{outfh}->Close() if $self->{outfh}->can('Close');
       $self->{outfh}->close() if $self->{outfh}->can('close');
       delete $self->{outfh};
       return $self->setRequestErr($errmsg) if ( defined($errmsg) && $errmsg );
#       $self->{dfilesz} = -s $self->{dfilen};
       return $self->setRequestOK(16, '', $self->{dfilen});
}

sub DESTROY {
  my $self = shift;
  my ($SrvName, $sock, $rbytes, $dbytes, $dfilesz ) = @{$self}{qw(SrvName sock rbytes dbytes dfilesz)};
  my $prtname = $self->{cinfo}->{reportname};
  shutdown($sock, 2) if ($sock and $sock->connected());
  close($sock) if $sock;

  if ($main::debug) {
#    $self->LogStruct($self, 'root');
    $self->LogStruct($self->{'cinfo'}, 'cinfo');
    #  $self->LogStruct($self->{'sdata'}, 'sdata');
  }
  $self->DoLog("Queue SERVER $SrvName ENDED. $rbytes bytes processed, $dbytes INPUT BYTES written, $dfilesz FILE DATA Stored");

}

1;
