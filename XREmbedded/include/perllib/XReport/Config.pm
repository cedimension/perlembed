
#------------------------------------------------------------
package XReport::Config;

use strict;
use XML::Simple;

sub mergeStruct {
# given 2 complex structure a and b will merge b into a recursively for hashes 
# arrays referenced by the same key will be joined togheter 
	my ($s_out, $s_in) = @_;
	foreach my $en ( keys %{$s_out} ) {
		my ($ik) = grep /$en/i, keys %{$s_in};
		$s_in->{$en} = delete($s_out->{$en}) unless $ik;
	}
	foreach my $en ( keys %{$s_in} ) {
		my ($ok) = grep /$en/i, keys %{$s_out};
		if ( !$ok || !ref($s_in->{$en}) || ref($s_in->{$en}) !~ /^(HASH|ARRAY)$/ || ref($s_out->{$ok}) ne ref($s_in->{$en}) ) {
              $s_out->{$ok || $en} = $s_in->{$en};
		}
		elsif ( ref($s_out->{$ok}) eq 'HASH' ) {
			  mergeStruct($s_out->{$ok}, $s_in->{$en});
	    }
	    else {
			  push @{$s_out->{$ok}}, @{$s_in->{$en}};
		}
	}
	return $s_out;
}

sub mergeWithConfig {
	my ($xml, $section, @xmlinparms) = @_;
	my ($xrcfgk) = grep /^$section$/i, keys %{$XReport::cfg}; 
    $XReport::cfg->{$section} = {} unless $xrcfgk;

    my $conf = parseXmlConfig($xml, @xmlinparms);
    my ($ck) = grep {/^$section$/i} (keys %$conf);
    return ($ck ? return $XReport::cfg->{$xrcfgk} = mergeStruct($XReport::cfg->{$xrcfgk}, $conf->{$ck}) 
    		: $XReport::cfg->{$xrcfgk});
}

sub getValues {
  my $SrvName = $XReport::cfg->{SrvName}; my $ComputerName = uc($ENV{COMPUTERNAME}); my @r;
 
  ### todo: make merge for hashes
  for (@_) {
    if ( $SrvName and exists($XReport::cfg->{daemon}->{$SrvName}->{$_}) ) {
      push @r, $XReport::cfg->{daemon}->{$SrvName}->{$_};
    }
    elsif ( $ComputerName and exists($XReport::cfg->{host}->{$ComputerName}->{$_}) ) {
      push @r, $XReport::cfg->{host}->{$ComputerName}->{$_};
    }
    elsif ( exists($XReport::cfg->{$_}) ) {
      push @r, $XReport::cfg->{$_};
    }
    else {
      push @r, undef;
    }
  }
  return wantarray ? @r : $r[0];
}

sub setValues {
  while (@_) {
    my ($prmName, $prmValue) = (shift, shift);

    $XReport::cfg->{$prmName} = $prmValue;
  }
}

sub varxlate {
  my ($varxlate, $varvalue) = @_;

  !exists($c::VarXlates{$_[0]}) ? $varvalue :
  !exists($c::VarXlates{$varxlate}->{$varvalue}) ? $varvalue :

  $c::VarXlates{$varxlate}->{$varvalue}
}

sub parseXmlConfig {
	my $xmlcfg = shift;
	my $newcfg;
    eval { $newcfg = XML::Simple::XMLin($xmlcfg, ForceArray => [qw(daemon host webappl plugin job step CASE ASSERT remotesetup)], 
		KeyAttr=> [daemon => 'name', 'webappl' => 'name', 'plugin' => 'name']) } if $xmlcfg;
      warn "error during xml config parse: $@" if $@;
	return $newcfg;
}

sub buildApplConfig {
  my ($xmlcfg, $applname, $cfg, $xmlFile) = @_;
      my $newcfg;
      eval { $newcfg = XML::Simple::XMLin($xmlcfg, ForceArray => [qw(daemon host webappl plugin job step)], 
                      KeyAttr=> [daemon => 'name', 'webappl' => 'name', 'plugin' => 'name']) } if $xmlcfg;
      my $parserr = $@ || '';
      warn "Processing FILE: $xmlFile ", ($parserr ? " - $parserr" : ''), "\n" if $main::debug;
      next unless $newcfg;

      $main::Application->{'path.configs'} .= $xmlFile.';';

      $cfg = { %$cfg, %$newcfg };

      my $hostcfg;
      if ( ref($cfg->{host}) eq "HASH" && exists( $cfg->{host}->{$ENV{COMPUTERNAME}}) && ref($cfg->{host}->{$ENV{COMPUTERNAME}}) eq "HASH" ) {
        $hostcfg = delete( $cfg->{host}->{$ENV{COMPUTERNAME}} );
        $cfg = { %$cfg, %$hostcfg };
      }
      delete $cfg->{host} if ( exists( $cfg->{host}) );
    
      if ( exists( $cfg->{webappl} )) { 
        if (my ($cfgkey) = grep /^$applname$/i, keys %{$cfg->{webappl}} ) {
          #$hostcfg = delete( $cfg->{webappl}->{$cfgkey} );
           $hostcfg = $cfg->{webappl}->{$cfgkey};
           $cfg = { %$cfg, %$hostcfg, 'this.webappl.config' => $hostcfg  };
        }
        delete $cfg->{webappl};
      }
      if ( exists( $cfg->{daemon} )) { 
        if (my ($cfgkey) = grep /^$applname$/i, keys %{$cfg->{daemon}} ) {
          #$hostcfg = delete( $cfg->{webappl}->{$cfgkey} );
           $hostcfg = $cfg->{daemon}->{$cfgkey};
           delete $hostcfg->{host};
           $cfg = { %$cfg, %$hostcfg, 'this.service.config' => $hostcfg  };
        }
      # delete $cfg->{webappl};
      }
  
}

*c::parseXmlConfig  = *XReport::Config::parseXmlConfig;
*c::mergeWithConfig = *XReport::Config::mergeWithConfig;
*c::setValues       = *XReport::Config::setValues;
*c::getValues       = *XReport::Config::getValues;
*c::varxlate        = *XReport::Config::varxlate;
*c::mergeStruct     = *XReport::Config::mergeStruct;

1;
