package XReport::Server;

use base Win32::OLE;

our $VERSION = do { my @r = (q$Revision: 2.1 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r }; 

sub CreateObject { shift; #print "XRServer - ", (caller(1))[0], ": ", @_, " -\n";
return Win32::OLE->new(@_) };

1;


package XReport;

require 5.005_62;

use strict;
use POSIX qw(strftime);

our $VERSION = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r }; 

our ($runmode, $initialized); our $cfg;

BEGIN {
  require XReport::Logger;
  *i::DESTROY = sub { };
   require XReport::Config;
}

sub runmode_iis {
  return 1 if (exists($main::{Application}) && exists($main::{Server}) && ref($main::Application) eq 'Win32::OLE' && ref($main::Server) eq 'Win32::OLE');
  return 0;
}

sub _estrusion() {
  my ($l1, $l2) = (shift, shift);
  my $re;
  eval "\$re = qr/^(?:".join('|', @$l1).")\$/";
#  print "REF L1 L2:", ref($l1), " - ", ref($l2), " => ", join('|', @$l1), " <= $re - $@\n";
  return ( grep !/$re/, @$l2 );
}


sub setApplNode {
  my ($hash, $pfx, $dflt) = (shift, shift, shift);
  if ( ref($hash) ) {
    foreach my $varnm ( keys %{ $hash } ) {
      if ($pfx eq 'cfg.db.') {
	my ($Provider) = ($hash->{$varnm} =~ /PROVIDER=([^;]+);/i); 
	
	$Provider = uc($Provider);
	###mpezzi add application name if provider = SQLOLEDB
	if ( $Provider eq 'SQLOLEDB') {
	  $hash->{$varnm} .= ";Application Name=$main::Application->{ApplName};" if $main::Application->{ApplName};
	  $hash->{$varnm} =~ s/;;/;/g;
	}  
	###
	
      }
      $main::Application->{$pfx.$varnm} = $hash->{$varnm};
    }
    if ( $dflt ) {
      if (exists($hash->{$dflt}) && !exists($hash->{'default'})) {
        $main::Application->{$pfx.'default'} = $hash->{$dflt} ;
        $hash->{'default'} = $hash->{$dflt};
      }
    }
    $main::Application->{$pfx.'__keys'} = join("\t", keys %{ $hash } );
  }
}

sub import {
#  warn "XREPORT:import starting for ", (caller())[0], " initialized: ", ($initialized ? "1" : "0"), "\n";
  
  my ($class, %largs) = @_; 
  return if $initialized; #exists($main::{Application}) && $main::Application->{'cfg.processed'};
  warn "XREPORT:import proceeding for ", (caller())[0], " initialized: ", ($initialized ? "1" : "0"), "\n" if $largs{debug};

  #return if $initialized; 
  my $package = (caller())[0]; 
  my $StartTime = strftime "%Y-%m-%d %H:%M:%S", localtime;

  require Win32::OLE;
  require Win32::OLE::Const;
  require XML::Simple;

#----------------------------------------------------------------------------------
  require FileHandle;
  require File::Basename;
  require XReport::Logger;
  require Storable;

  $runmode = ($largs{'runmode'} || '');

  $main::logger = new XReport::Logger($main::Application->{'ApplName'});  
  if ( !$main::Application->{'iis.cfg.perl'} ) {
    my ($applname, $applpath, $appluser, $parentpath, $scriptname ) = ('', '', Win32->LoginName(), '' );
    
    my $XREPORT_HOME = $ENV{'XREPORT_HOME'};
    my $XREPORT_SITECONF = $ENV{'XREPORT_SITECONF'};
    my $confvar = 1 if $XREPORT_SITECONF;

      my @scriptcalled = split /[\/\\]/, $0;
      ($scriptname = pop @scriptcalled) =~ s/\.[^\.\s]+$//;
      $applname = $scriptname;
      $applpath = join('/', @scriptcalled);
      $applpath = '.' unless $applpath;
#      chomp($applpath = `pwd`) if $applpath eq '.';
    
      foreach my $i (0 .. $#main::ARGV) {
	     if ($main::ARGV[$i] eq "-N") { # Server Name 
	        $applname = $ARGV[$i+1]; 
	        $i++;
	     }
	     elsif ($main::ARGV[$i] eq "-SITECONF") { # SITECONF to be set 
	       ($XREPORT_SITECONF = $ARGV[$i+1]) =~ s/\\/\//g;;
	       $i++;
	       $confvar = 0;
	     }  
	     elsif ($main::ARGV[$i] eq '-@') { # Daemon Mode 
	       $main::daemonMode = 1;
	     }
	     elsif ($main::ARGV[$i] eq '-d') { # Debug mode
	       $main::debug = 1;
           $largs{debug} = 1;
	     }
         elsif ($main::ARGV[$i] eq '-dd') { # veryverbose mode
           $main::debug = 1;
           $main::veryverbose = 1;
           $largs{debug} = 1;
         }
      }
      $main::Server = bless {}, 'XReport::Server';
      $main::Session = Win32::OLE->new('Scripting.Dictionary');
      $main::ObjectContext = bless {}, 'XReport::Server';
#      $main::ScriptingNameSpace = Win32::OLE->new("ScriptControl");

    $parentpath = File::Basename::dirname($applpath);

    die "XREPORT_SITECONF not defined" unless $XREPORT_SITECONF;
    die "UNABLE TO ACCESS \"$XREPORT_SITECONF\"" if !-d $XREPORT_SITECONF;
    
    warn "SITECONF: $XREPORT_SITECONF HOME: $XREPORT_HOME\n" if $main::debug;

    $ENV{'XREPORT_SITECONF'} = $XREPORT_SITECONF;

    @{$main::Application}{qw(ApplUser ApplPath ApplName ApplParentPath XREPORT_HOME XREPORT_SITECONF)} = 
      ($appluser, $applpath, $applname, $parentpath, $XREPORT_HOME, $XREPORT_SITECONF);
    my @applperllib = (); 
    my $loclperllib = "$parentpath\\perllib";
    unshift @applperllib, $loclperllib if (-d $loclperllib );  
    $main::Application->{'ApplPerllib'} = join(';', @applperllib) if scalar(@applperllib);
  
    $cfg = {};

    $main::Application->{'path.configs'} = '';
    my $triedxml = [];
    foreach my $xmlfnam ( ('xreport.cfg', "$scriptname.xml", "$applname.xml")) {
      $main::logger->log("searching $xmlfnam");
      my $xmlFile = "$XREPORT_SITECONF/$appluser/xml/$xmlfnam";
      $xmlFile =~ s/\\/\//g;
      $xmlFile = "$XREPORT_SITECONF/xml/$xmlfnam" unless $xmlFile && -f $xmlFile;
      $xmlFile =~ s/\\/\//g;
      $xmlFile = $applpath."/$xmlfnam" unless -f $xmlFile;
      $xmlFile =~ s/\\/\//g;
      $main::logger->log("found $xmlFile");

      my $xmlcfg = ''; # = "\<cfg\>\<configloaderror \>$xmlfnam FILE NOT FOUND\</configloaderror\>\</cfg\>";
      push @{$triedxml}, $xmlFile;
      if (-f $xmlFile) {
	    my $xfh = new FileHandle("<$xmlFile");
	    binmode $xfh;
	    $xfh->read($xmlcfg, -s $xmlFile);
	    close $xfh;
	    next unless length($xmlcfg) == -s $xmlFile;
	    $xmlcfg =~ s/\$(\w+)\$/$main::Application->{$1}/g;
#	warn "XML: $xmlcfg\n";
      }

      my $newcfg;
      eval { $newcfg = XML::Simple::XMLin($xmlcfg, ForceArray => [qw(daemon host webappl plugin job step)], 
					  KeyAttr=> [daemon => 'name', 'webappl' => 'name', 'plugin' => 'name']) } if $xmlcfg;
      my $parserr = $@ || '';
      warn "Processing SITECONF: $XREPORT_SITECONF FILE: $xmlFile ", ($parserr ? " - $parserr" : ''), "\n" if $main::debug;
      next unless $newcfg;

      $main::Application->{'path.configs'} .= $xmlFile.';';

      $cfg = { %$cfg, %$newcfg };

      my $hostcfg;
      if ( ref($cfg->{host}) eq "HASH" && exists( $cfg->{host}->{$ENV{COMPUTERNAME}}) && ref($cfg->{host}->{$ENV{COMPUTERNAME}}) eq "HASH" ) {
	    $hostcfg = delete( $cfg->{host}->{$ENV{COMPUTERNAME}} );
	    $cfg = { %$cfg, %$hostcfg };
      }
      delete $cfg->{host} if ( exists( $cfg->{host}) );
    
      if ( exists( $cfg->{webappl} )) { 
	    if (my ($cfgkey) = grep /$applname/i, keys %{$cfg->{webappl}} ) {
		  #$hostcfg = delete( $cfg->{webappl}->{$cfgkey} );
	       $hostcfg = $cfg->{webappl}->{$cfgkey};
	       $cfg = { %$cfg, %$hostcfg, 'this.appl.config' => $hostcfg  };
	    }
	    delete $cfg->{webappl};
      }
      if ( exists( $cfg->{daemon} )) { 
	    if (my ($cfgkey) = grep /$applname/i, keys %{$cfg->{daemon}} ) {
		  #$hostcfg = delete( $cfg->{webappl}->{$cfgkey} );
	       $hostcfg = $cfg->{daemon}->{$cfgkey};
	       delete $hostcfg->{host};
	       $cfg = { %$cfg, %$hostcfg, 'this.appl.config' => $hostcfg  };
	    }
      #	delete $cfg->{webappl};
      }

    }
    die "Unable to load Config from \"".join(';', @{$triedxml})."\"" unless scalar(keys %$cfg);
    $cfg->{'SrvName'} = $main::Application->{'ApplName'};
    if ( !exists($cfg->{workdir}) || !$cfg->{workdir} ) {
       my $lclloc = $ENV{HOMEPATH} || '';
       if ($lclloc) {
          my $appdir = "\\Local Settings\\Application Data";
          unless (-e $cfg->{'workdir'}.$lclloc.$appdir && -d $cfg->{'workdir'}.$lclloc.$appdir) {
             $appdir = "\\Application Data";
          }
          $lclloc = '' unless -e $cfg->{'workdir'}.$lclloc.$appdir && -d $cfg->{'workdir'}.$lclloc.$appdir;
       }
       $lclloc = "\\WINDOWS\\Temp" unless $lclloc;
       if ( $lclloc ) {
       	  $lclloc = ($ENV{HOMEDRIVE} || 'C:').$lclloc;
          die "Unable to set Workdir \"$lclloc\\XReport\" - file exists with same name" if (-e "$lclloc\\XReport" && !-d "$lclloc\\XReport");
          mkdir "$lclloc\\XReport" unless ( -e "$lclloc\\XReport" );
          $cfg->{'workdir'} = $lclloc."\\XReport\\$applname";
       }
    }
    die "Unable to determinate Workdir " unless (exists($cfg->{workdir}) && $cfg->{'workdir'});
    die "Invalid Workdir \"$cfg->{'workdir'}\" - not a directory" if (-e $cfg->{workdir} && !-d $cfg->{'workdir'});
    mkdir $cfg->{'workdir'} unless -d $cfg->{'workdir'};
    die "unable to access $cfg->{'workdir'}" unless (-e $cfg->{workdir} && -d $cfg->{'workdir'});

    $cfg->{'logsdir'} = $cfg->{'workdir'}.'/logs' unless $cfg->{'logsdir'};
    $cfg->{'logsdir'} .= "/webappls" if $runmode eq 'iis' or $runmode eq 'apache';
    $main::Application->{'cfg.logsdir'} = $cfg->{'logsdir'};

    $main::Application->{'cfg.workdir'} = $cfg->{'workdir'}   if exists $cfg->{'workdir'};
  
#    setApplNode(delete($cfg->{'LocalPath'}), 'cfg.localpath.', 'L1') if exists($cfg->{'LocalPath'});
#    setApplNode(delete($cfg->{'dbase'}), 'cfg.db.')                  if exists($cfg->{'dbase'});
    setApplNode($cfg->{'LocalPath'}, 'cfg.localpath.', 'L1') if exists($cfg->{'LocalPath'});
    setApplNode($cfg->{'dbase'}, 'cfg.db.')                  if exists($cfg->{'dbase'});
    setApplNode(delete($cfg->{'AppOperVar'}), 'opr.')                if exists($cfg->{'AppOperVar'});
    setApplNode(delete($cfg->{'this.appl.config'}), 'cfg.')          if exists($cfg->{'this.appl.config'});

    $main::Application->{'iis.cfg.perl'} = Storable::freeze($cfg); #XML::Simple::XMLout($cfg);

  }

  if ($largs{debug}) {
      require Data::Dumper;
      my $applcfg = $main::Application;
      print Data::Dumper::Dumper($applcfg, $cfg);
  }
  unless ( $runmode eq 'iis' or $runmode eq 'apache'  or $runmode eq 'fastcgi' ) {
    $main::logger->AddFile('STDOUT', \*STDOUT) if !$main::daemonMode;
  }

  (my $logpath = $cfg->{"logsdir"}) =~ s/\//\\/g;
  mkdir $logpath unless -d $logpath;
  die "unable to access $logpath" unless -d $logpath;

  $main::logger->AddFile('ServerLog', $logpath."\\".$cfg->{'SrvName'}.".LOG");
  *i::logit = sub { $main::logger->log(@_) if $main::logger; };
  *i::warnit = sub { $main::logger->logwarn(@_) if $main::logger; };
  $main::logrRtn = sub { $main::logger->log(@_); };
  die "PROGRAM ERROR: logrRtn eval ERROR at InitServer $@\n" if $@;
      

#-----------------------------------------------------------------------------------

  ##---- Report Format Values ----
  $CD::rfAscii  = 1;
  $CD::rfEBCDIC = 2;
  $CD::rfAFP    = 3;
  $CD::rfVipp   = 4;
  
  ##---- Xfer Mode Values ----
  $CD::xmLpr  = 1;
  $CD::xmPsf  = 2;
  $CD::xmFTPb = 3;
  $CD::xmFTPc = 4;
  
  ##---- Status Values ----
  $CD::stAccepted   = 0;
  $CD::stReceiving  = 1;
  $CD::stRecvError  = 15;
  $CD::stReceived   = 16;
  $CD::stQueued     = 16;
  $CD::stProcessing = 17;
  $CD::stCompleted  = 18;
  $CD::stProcError  = 31;
  
  map { eval "*$package\::$_ = *main::$_"; } (qw(logger logrRtn Server Application Session Request Response)) if $package ne 'main';

  $initialized = 1;
  $main::Application->{'cfg.processed'} = 1;

}

sub DESTROY {
}

1;

=pod

#------------------------------------------------------------
package UNIVERSAL;

no strict qw(refs);

use Carp;

my %table_iis = (
  dbConnect => ['XReport::IIS::DBUtil', 'dbConnect'],
  dbExecute => ['XReport::IIS::DBUtil', 'dbExecute'],
  dbGetStaticRs => ['XReport::IIS::DBUtil', 'dbGetStaticRs'],
  dbExecuteReadOnly => ['XReport::IIS::DBUtil', 'dbExecuteReadOnly'],
  dbExecuteForUpdate => ['XReport::IIS::DBUtil', 'dbExecuteForUpdate'],
);

my %table_fcgi = (
  dbConnect => ['XReport::FCGI::DBUtil', 'dbConnect'],
  dbExecute => ['XReport::FCGI::DBUtil', 'dbExecute'],
  dbGetStaticRs => ['XReport::FCGI::DBUtil', 'dbGetStaticRs'],
  dbExecuteReadOnly => ['XReport::FCGI::DBUtil', 'dbExecuteReadOnly'],
  dbExecuteForUpdate => ['XReport::FCGI::DBUtil', 'dbExecuteForUpdate'],
);

my %table_default = (
  getConfValues => ['XReport::Util', 'getConfValues'],
  getPlugin => ['XReport::Plugin', 'getPlugin'],
  InitServer => ['XReport::Util', 'InitServer'],
  TermServer => ['XReport::Util', 'TermServer'],
  GetDateTime => ['XReport::Util', 'GetDateTime'],
  Dumper => ['Data::Dumper', 'Dumper'],
  getcwd => ['Cwd', 'getcwd'],
  mkpath => ['File::Path', 'mkpath'],
  rmtree => ['File::Path', 'rmtree'],
  gzopen => ['Compress::Zlib', 'gzopen'],
  gzclose => ['Compress::Zlib', 'gzclose'],
  open3 => ['IPC::Open3', 'open3'],
  gensym => ['Symbol', 'gensym'],
  dirname => ['File::Basename', 'dirname'],
  basename => ['File::Basename', 'basename'],
  min   => ['List::Util', 'min'],
  max   => ['List::Util', 'max'],
  md5_hex => ['Digest::MD5', 'md5_hex'],
  uri_unescape => ['URI::Escape', 'uri_unescape'],
  dbConnect => ['XReport::DBUtil', 'dbConnect'],
  dbExecute => ['XReport::DBUtil', 'dbExecute'],
  dbGetStaticRs => ['XReport::DBUtil', 'dbGetStaticRs'],
  dbExecuteReadOnly => ['XReport::DBUtil', 'dbExecuteReadOnly'],
  dbExecuteForUpdate => ['XReport::DBUtil', 'dbExecuteForUpdate'],
);

our ($AUTOLOAD);

sub AUTOLOAD {
  my ($tmodule, $tsub) = $AUTOLOAD =~ /^(.*)::(\w+)$/; my ($amodule, $asub, @autoload_tables);
  
  *{$AUTOLOAD} = \&i::DESTROY, return undef if $tsub eq 'DESTROY';

#  if ($runmode eq "iis") {
#    push @autoload_tables, \%table_iis;
#  }
#  if ($runmode eq "fastcgi") {
#    push @autoload_tables, \%table_fcgi;
#  }
  push @autoload_tables, \%table_default;

  for (@autoload_tables) {
    ($amodule, $asub) = @{$_->{$tsub}}, last if exists($_->{$tsub})
  }

  if ($amodule) {
  }
  elsif (
    $tsub =~ /^[A-Z_]+$/ 
    and require XReport::CONSTANTs 
    and exists($XReport::CONSTANTs::{$tsub})
  ) {
    ($amodule, $asub) = ('XReport::CONSTANTs', $tsub); 
  }
  elsif ( $tmodule eq "dbutil") {
    $amodule = ( $runmode eq "iis" ? "XReport::DBUtil" 
#               : $runmode eq "fastcgi" ? "XReport::FCGI::DBUtil"
               : "XReport::DBUtil" );
    ; 
    $asub = $tsub;
    if ($_[0] eq $tmodule) {
      shift @_; unshift @_, $amodule;
    }
  }
  else {
    croak("Undefined subroutine \&$tmodule\:\:$tsub called");
  }
  
  if ("$tmodule\:\:$tsub" eq "$amodule\:\:$asub") {
    croak('RECURSIVE UNIVERSAL::AUTOLOAD CALL DETECTED'.$tmodule." ".$tsub);
  }
  
  #print ("UNIVERSAL AUTOLOAD module is \"$amodule\"  sub is \"$asub\"\n"); 

  eval "require $amodule"; *{$AUTOLOAD} = eval "\\&$amodule\:\:$asub"; &$AUTOLOAD(@_);
}

1;

=cut

__END__
# Below is stub documentation for your module. You better edit it!

=head1 NAME

XReport - Perl extension for blah blah blah

=head1 SYNOPSIS

  use XReport;
  blah blah blah

=head1 DESCRIPTION

Stub documentation for XReport, created by h2xs. It looks like the
author of the extension was negligent enough to leave the stub
unedited.

Blah blah blah.

=head2 EXPORT

None by default.


=head1 AUTHOR

A. U. Thor, a.u.thor@a.galaxy.far.far.away

=head1 SEE ALSO

perl(1).

=cut

=general
    require Config::General;
my $config = <<'EOF';

FoldersTreesMethod  "db:tbl_FoldersTrees"
FoldersTreesXsl  "deeptree_FULL.xsl"
WorkDir  "c:/xreport/wwwroot/WORK"
userlib  "c:/xreport/userlib"
AuthMethod  "NTLM"

<dbase>
  XREPORT PROVIDER=SQLOLEDB;DATA SOURCE=USSQLW0A2\USSQLW0A2;DATABASE=u0xreport;USER ID=u0xreportoa;PASSWORD=password
  XRINDEX PROVIDER=SQLOLEDB;DATA SOURCE=USSQLW0A2\USSQLW0A2;DATABASE=u0xreport;USER ID=u0xreportoa;PASSWORD=password
</dbase>

<plugins>
  <plugin get.centera.parameters>
    ref "c:/xreport/userlib/perllib/centera.parameters.pm" 
  </plugin>
  <plugin pippo.pluto.paperino>
    ref "c:/xreport/userlib/perllib/centera.parameters.pm" 
  </plugin>
  <plugin qui.quo.qua>
    ref "pippo"
  </plugin>
</plugins>

EOF

  my $cfg_general = Config::General->new(-String => $config );
  my %cfg = $cfg_general->getall();
  $cfg = \%cfg;
=cut
