#!/usr/local/bin/perl -w
#######################################################################
# @(#) $Id: xrLpdDump.pl,v 1.2 2001/04/20 20:23:54 mpezzi Exp $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################

my $version = do { my @r = (q$Revision: 1.2 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };
use strict;

use XReport;
use XReport::Util;
use XReport::QUtil;

sub Server;
sub Child;
sub GetDateTime;
sub queueForProcess;
sub LPRQUEGZ;
sub readCtlStream;
sub receivePrinterJob;

my $debug   = 0;
my $exeType = \&Server;
my $con = 0;
$REQ::daemon = 0;
my $CliFno;
my $SrvName;

foreach my $i (0 .. $#ARGV) {
  if 	($ARGV[$i] eq "-d") { # enable debug mode
    $REQ::debug = $debug = 1;
  }
  elsif ($ARGV[$i] eq "-N") { # Server Name 
    $REQ::SrvName = $SrvName = $ARGV[$i+1];
  }
  elsif ($ARGV[$i] eq '-@') { # Daemon Mode 
    $REQ::daemon = 1;
  }
  elsif ($ARGV[$i] eq "-P") { # execution type 
    if ($ARGV[$i+1] eq "CHILD") {
      $exeType = \&Child;
      XRprId($REQ::prId = "$$(".$ARGV[$i+2].")");
      $CliFno  = $ARGV[$i+3];
      $REQ::conid = $con     = $ARGV[$i+4];
    }
  }
}
if (!$SrvName) {
  &$logrRtn("Server Name not specified - unable to continue");
  exit;
}

$REQ::SrvPort   = $XReport::cfg->{'daemon'}->{$SrvName}->{'port'} || 515;
$REQ::msgmax    = $XReport::cfg->{'daemon'}->{$SrvName}->{'maxdata'} || 32768;
#$REQ::WorkClass = $XReport::cfg->{'daemon'}->{$SrvName}->{'WorkClass'} || 32768;
my $log_file    = $XReport::cfg->{'logsdir'}."/".$SrvName.".log";


XRopenLog($log_file) if ($REQ::daemon ne 0);

exit (&$exeType);

sub Server {
  use IO::Socket;
  use IO::Select;

  my ($sock, $Cli, $currSt);
  &$logrRtn("$^O server Started ...");

  my $SrvSock = new IO::Socket::INET(Listen => 5, 
				     LocalPort => $REQ::SrvPort,
				     Proto => 'tcp', 
				     Reuse => 1) || &$logrRtn("unable to listen on $REQ::SrvPort - $!");

  &$logrRtn("Listener now waiting on port $REQ::SrvPort") if ($SrvSock);

  my $sel = new IO::Select( $SrvSock );

  &$logrRtn("now entering main loop") if ($SrvSock);
  for (my $con = 0; ; $con++) {
    $sock = $sel->can_read(15);
    next unless ($sock);
    $Cli = $SrvSock->accept || &$logrRtn("unable to accept connection - $! - $?");
    if ($Cli) {
      my ($cliport, $iaddr) = sockaddr_in(getpeername($Cli));
      $REQ::peer = inet_ntoa($iaddr);
      &$logrRtn($SrvName, " accepted connection ", $REQ::peer);
      spawnProcess($0, "-N", $SrvName, ($REQ::daemon eq 0 ? '-_' : '-@'), "-P", "CHILD", "$$", getFileToken($Cli), $con);
    } else {
      return 1;
    }
  }
  &$logrRtn("now leaving main loop: $currSt ", (($sock) ? "Cli pending" : "Cli Exhausted"));
  $SrvSock->close;
  return 0;
}

sub doLog {
  if ($REQ::LpdSock && $REQ::xrClient) {
    syswrite($REQ::LpdSock, "INFO " . join('', @_) ."\n");
  }
  &$logrRtn(@_);
}

sub Child {

  use IO::Socket;
  use IO::Select;

  %REQ::queRtns = buildRtnHash($XReport::cfg->{'basedir'} . "/" . 
			     ($XReport::cfg->{'daemon'}->{$REQ::SrvName}->{'bindir'} or 
			      "QUERTNS")
			    );
  %REQ::queRtns = (
	      '.default'  => \&LPRQUEGZ,     # $REQ::queRtns{'LPRQUEGZ'},
	      'LPRQUEGZ'  => \&LPRQUEGZ,
	      'LPRQUE'    => \&LPRQUEGZ,     # $REQ::queRtns{'LPRQUEGZ'},
	      'GETFTPCQ'  => \&GETFTPCQ,
	      'GETFTPQ'   => \&GETFTPCQ,     # $REQ::queRtns{'GETFTPCQ'},
#	      'CTMPMFTP'  => \&CTMPMFTP,
	     );

  $REQ::prtname = "undefined";

  my $valPrtn = qr/([\w\.]+)/;

  $REQ::LpdSock = new IO::Socket;

  openFileToken($REQ::LpdSock, $CliFno) || die("CliSock OPEN ERROR <$!>\n");
#  $REQ::peer = $REQ::LpdSock->PeerHost;
#  $REQ::port = $REQ::LpdSock->PeerPort;

  setsockopt($REQ::LpdSock, SOL_SOCKET, SO_RCVBUF, pack("l", $REQ::msgmax))   || die "Setsockopt error: $!";

  select($REQ::LpdSock); $| = 1; select(STDOUT);

  ($REQ::port, $REQ::iaddr) = sockaddr_in(getpeername($REQ::LpdSock));

  $REQ::peer = inet_ntoa($REQ::iaddr);

  &$logrRtn("Serving connection $con from $REQ::peer at $REQ::port");
  $REQ::InEof = 0;
#  my $sel = new IO::Select( $REQ::LpdSock );
  my $cmd = <$REQ::LpdSock>;
  if ( $cmd =~ /^([\x02\x03\x04\x1a])([\w\.]+)\x0a$/o) {
    (my $req, $REQ::prtname) = ($1, $2);
    ($REQ::queuetype, $REQ::queuename) = split(/\./,$REQ::prtname,2);
    unless ($REQ::queuename) {
      $REQ::queuename = $REQ::queuetype;
      $REQ::queuetype = '.default';
    }

    if ( $req =~ /[\x02\x1a]/ ) {
      $REQ::xrClient = ($req eq "\x1a");
      &$logrRtn("Peer requester xrClient: $REQ::xrClient");
      syswrite($REQ::LpdSock,"\x00",1);
      $REQ::DtaBytes = $REQ::OutBytes = 0;
    JOB: for (my $jobn = 0; ; $jobn++ ) {
	$REQ::currJob = $jobn;
	last JOB unless recvJob($jobn);
      }
      doLog("Bytes received: $REQ::InBytes Written: $REQ::OutBytes");
    }
    elsif ( $req =~ /[\x03\x04]/ ) {
      &$logrRtn("Queue status request received. Queue: $REQ::prtname");
      syswrite(LPRSOCK, "$^O $REQ::prtname next job will have " . ($con + 1) . " id" ) if ($req eq "\x04");
      syswrite(LPRSOCK, "$^O $REQ::prtname Receiver Ready");
      syswrite($REQ::LpdSock,"\x00",1);
    }
  }
  elsif ($cmd) {
    &$logrRtn("Unexpected cmd received: ", unpack("H*", $cmd), "");
    syswrite($REQ::LpdSock,"\x7F",1);
  }
  else {
    &$logrRtn("No Data received from requester at $REQ::peer");
  }

  shutdown($REQ::LpdSock,2);
  close($REQ::LpdSock);

  &$logrRtn("Queue SERVER $con for $REQ::prtname ENDED.");

  return(0);

}

sub readLpdSock {
  use IO::Select;

  my $sel = new IO::Select( $REQ::LpdSock );

  if (! $sel->can_read(240)) {
    &$logrRtn("Connection with $REQ::peer($REQ::port) timed out");
    return undef;
  }

#  my $dbuff;
  my $blen = sysread($REQ::LpdSock, my $dbuff, ($REQ::msgmax > $REQ::bleft ? $REQ::bleft : $REQ::msgmax));
  die "Some error encounterd during port $REQ::port sysread - $!\n" unless defined($blen);
  if ($blen > 0) {
    $REQ::rbytes += $blen;
    $REQ::bleft  -= $blen;

    if ($REQ::bleft == 0) {
      $blen = sysread($REQ::LpdSock, my $null, 1);
      die "Some error encounterd during port $REQ::port sysread - $!" unless defined($blen);
      $REQ::InEof = 1;
    }

    ($REQ::bpos, $REQ::sol, $REQ::blen) = (($REQ::bpos - $REQ::sol), 0, length($REQ::buffer));
    $REQ::buffer = substr($REQ::buffer . $dbuff, $REQ::sol);
    return ($REQ::bpos, $REQ::sol, $REQ::blen, $REQ::buffer);
  }

  &$logrRtn("EOF received from requester at $REQ::peer($REQ::port)");
  return undef;

};

sub readFtpSocket {
  &$logrRtn("Filling up buffer $REQ::pagcnt $REQ::lines $REQ::sol $REQ::bpos $REQ::blen") if ($REQ::debug);
  my $rb = $REQ::FtpDCH->read(my $NewBuffer, $::msgmax, 15);
  $REQ::ftpEof = (! $rb );
  $REQ::ftprb += $rb if ($rb);
  $REQ::buffer = substr($REQ::buffer.$NewBuffer, $REQ::sol);
  ($REQ::bpos, $REQ::sol, $REQ::blen) = (($REQ::bpos - $REQ::sol), 0, length($REQ::buffer));
  &$logrRtn("Received $rb bytes $REQ::pagcnt $REQ::lines $REQ::sol $REQ::bpos $REQ::blen") if ($REQ::debug);
  return ($REQ::blen, $REQ::bpos, $REQ::sol, $REQ::buffer);
}

sub recvJob {
  my $jobn = shift;
  my $cmd = <$REQ::LpdSock>;
  return undef unless $cmd;

  if ($cmd eq "\x01\x0a") {
    doLog("Client sent Abort Command - aborting jobs 0 - $#REQ::job ");
    syswrite($REQ::LpdSock,"\x00",1);
    return undef;
  }

  if ($cmd =~ /^([\x02\x03])(\d+)\s(\w+)\x0a$/) {
    my ($cc, $numb, $infnam) = ($1, $2 , $3);
    if ($cc eq "\x02") {
      
      if ($REQ::job[$jobn]{CtlFile}) {
	doLog("more than one Cntl File in the same job - aborting");
	syswrite($REQ::LpdSock,"\x7F",1);
	return undef;
      }
      
      doLog("Client sent Cntl file $REQ::currJob - name: $infnam bytes: $numb ");
      syswrite($REQ::LpdSock,"\x00",1);
      
      $REQ::job[$jobn]{CtlFile} = $infnam;
      $REQ::job[$jobn]{CtlBytes} = $REQ::bleft = $numb;
      $REQ::sol = $REQ::bpos = 0;
      ($REQ::rbytes, $REQ::buffer ) = (0, '');
      do {
	(undef, undef, $REQ::sol, undef) = readLpdSock();
	$REQ::job[$jobn]{CtlStream} .= $REQ::buffer;
	$REQ::bpos = $REQ::sol;
      } while ($REQ::rbytes < $numb);
      
      if ($REQ::rbytes ne $numb) {
	doLog("receive failed for $REQ::currJob Cntl file: $numb expected - $REQ::rbytes received");
	syswrite($REQ::LpdSock,"\x7F",1);
	return undef;
      }
      
      doLog("Cntl subcommands received: $numb bytes");
      syswrite($REQ::LpdSock,"\x00",1);
      
      if (! $REQ::job[$jobn]{DtaFile}) {
	return undef unless recvJob($jobn);
      }
      return $REQ::rbytes;
    }
    else { # cc = \x03
      if ($REQ::job[$jobn]{DtaFile}) {
	doLog("more than one Data File in the same job - aborting");
	syswrite($REQ::LpdSock,"\x7F",1);
	return undef;
      }
      
      doLog("Client sent Data file $REQ::currJob - name: $infnam bytes: $numb ");
      syswrite($REQ::LpdSock,"\x00",1);
      ($REQ::rbytes, $REQ::wbytes, $REQ::buffer ) = (0, 0, '');
      my $xfrRtn = ($REQ::queRtns{$REQ::queuetype} or $REQ::queRtns{'.default'});

      my $brcvd = &$xfrRtn($jobn,
			   $REQ::job[$jobn]{DtaFile} = $infnam,
			   $REQ::job[$jobn]{DtaBytes} = $REQ::bleft = $numb);

      return undef unless $brcvd;

      $REQ::DtaBytes += $brcvd;
      return $brcvd
    }
  }
  else {
    doLog("Unknown receive job subcmd received:", unpack("H*", $cmd), " -- ",  $cmd);
    syswrite($REQ::LpdSock,"\x7F",1);
    return undef;
  }
}

sub setXferFileA {

  my ($queuetype, $queuename, $progr, $datetime) = @_;

  $datetime = GetDateTime() unless ($datetime && $datetime =~ /^\d{14}$/);
  my ($curryear, $currday, undef) = unpack("a4a4a*", $datetime);

  my $xrspool = $XReport::cfg->{'xrspool'};
  if (! -e $xrspool."/".$curryear."/".$currday ) {
    mkdir $xrspool."/".$curryear if (! -e $xrspool."/".$curryear);
    mkdir $xrspool."/".$curryear."/".$currday;
    die "unable to create $xrspool $curryear $currday dir\n" unless ( -e $xrspool."/".$curryear."/".$currday )
  }
  my $outpdir = $xrspool."/".$curryear."/".$currday;

  my $filen = "$queuename.$datetime.$progr";
  
  return ($filen, $outpdir, $datetime);
}

sub logNewRequest {

  $REQ::xfermode = shift;

  doLog("Now adding conid: $REQ::conid prId: $REQ::prId xm: $REQ::xfermode st: $CD::stAccepted");
  my $reqid = QCreate XReport::QUtil(
				     SrvName        => $REQ::SrvName,
				     ReportType     => $REQ::queuename,
				     LocalFileName  => $REQ::queuetype . $$ . $REQ::conid,
				     RemoteHostAddr => $REQ::peer,
				     XferStartTime  => "GETDATE()",
				     XferMode       => $REQ::xfermode,
				     XferDaemon     => $REQ::SrvName,
				     Status         => $CD::stAccepted,
				     XferId         => $REQ::prId,
				     );
  $REQ::InBytes = 0;
  return $reqid;
}

sub updateRequest {
  my ($code, $reqid, $cntlfile, $datafile, $CtlStream) = @_;

  open(my $fhOut, ">$cntlfile") || do {
    doLog("DATA FILE OPEN ERROR: $!");
    return undef;
  };
  binmode $fhOut;
  my $wb = syswrite($fhOut, $CtlStream);

  close($fhOut);
  return undef if ($wb ne length($CtlStream));
  my %cinfo = ();
  foreach ( split(/\n/, $CtlStream) ) {

    /^\-ofileformat=(\w+).*$/             && do {
#      print "row o: $1\n";
      $cinfo{'fileformat'} = ($1 eq 'record' ? $CD::xmPsf : $REQ::xfermode); next; };

    /^\-o(\w+)=(\w+).*$/             && do {
#      print "row o: $1 $2\n";
      $cinfo{$1} = $2; next; };

    /^J(\w+)\.(\w+)\.(\w+)\.(\w+)\./ && do {
#      print "row J: $3 $4\n";
      ($cinfo{jname}, $cinfo{jnum}) = ($3, $4); next; };

    /^(\w)(.*)$/                     && do {
#      print "row G: $1 $2\n";
      $cinfo{$1} = $2; next;};
  }

#  print "CtlStream: \n", $CtlStream, "\n";
#  die "-----------------\n";
#				   XferDaemon     => $REQ::SrvName,
  my $req = QUpdate XReport::QUtil(
				   XferEndTime    => 'GETDATE()',
				   Status         => $code,
				   JobName        => ($cinfo{jname} || $cinfo{U}),
				   JobNumber      => ($cinfo{jnum} || substr($cinfo{U},2,4)),
				   ReportType     => $REQ::queuename,
				   RemoteFileName => $cinfo{N},
				   LocalFileName  => $REQ::datafile,
				   XferPages      => ($REQ::pagcnt || 1),
				   XferInBytes    => $REQ::InBytes,
				   XferMode       => ($cinfo{fileformat} || $REQ::xfermode),
				   Id             => $reqid,
				  );


  return $req;
}
sub LPRQUEGZ {
## -*- Mode: Perl -*- #################################################
# @(#) $Id: xrLpdServ.pl,v 1.8 2001/04/20 20:23:54 mpezzi Exp $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
my $version = do { my @r = (q$Revision: 1.8 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

use Compress::Zlib;

my ($jobn, $infnam, $numb) = @_;

$REQ::dbid = my $reqid = logNewRequest($CD::xmLpr);
my ($filen, $outpdir, $datetime) = setXferFileA($REQ::queuetype, $REQ::queuename, $REQ::dbid);

$REQ::datafile = $outpdir."/".$filen.".DATA.TXT.gz";

doLog("LPRQUEGZ " . $version . " receiving $numb data bytes to fullfill req $REQ::dbid");
&$logrRtn("req $REQ::dbid will create file $REQ::datafile");
my $fhOut = gzopen($REQ::datafile, "wb") || die("DATA FILE OPEN ERROR $!"); 

$REQ::sol = $REQ::bpos = 0;
do {
  (undef, undef, $REQ::sol, undef) = readLpdSock();
  my $wb = $fhOut->gzwrite($REQ::buffer) || do {
    doLog("LPRQUEGZ " . $version . " Error during $REQ::datafile write : $fhOut->gzerror");
    last;
  };
  $REQ::wbytes += $wb;
  $REQ::bpos = $REQ::sol;
} while ($REQ::rbytes < $numb);
$REQ::job[$jobn]{bytes}  = $REQ::rbytes;
$REQ::job[$jobn]{wbytes} = $REQ::wbytes;

$fhOut->gzclose();
$REQ::OutBytes = -s $REQ::datafile;

if ($REQ::rbytes ne $numb) {
  doLog("receive failed for Data file: bytes expected $numb " . 
	" recvd: "   . $REQ::rbytes .
	" written: " . $REQ::OutBytes );
  syswrite($REQ::LpdSock,"\x7F",1);
  return undef;
}

$REQ::InBytes = $REQ::rbytes;

doLog("Data file received - bytes expected: $numb" .
      " recvd: " . $REQ::InBytes .
      " written: " . $REQ::OutBytes );

syswrite($REQ::LpdSock,"\x00",1);

if (! $REQ::job[$jobn]{CtlFile}) {
  if (! recvJob($jobn) ) {
    unlink $REQ::datafile;
    return undef;
  }
}

$REQ::pagcnt = 0;
return undef unless updateRequest($CD::stReceived,
				  $REQ::dbid,
				  $outpdir."/".$filen.".CNTRL.TXT",
				  $REQ::datafile,
				  $REQ::job[$jobn]{'CtlStream'});


return $REQ::job[$jobn]{bytes};

}

sub GETFTPCQ {
# -*- Mode: Perl -*- ##################################################
# @(#) $Id: xrLpdServ.pl,v 1.8 2001/04/20 20:23:54 mpezzi Exp $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
  my $version = do { my @r = (q$Revision: 1.8 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

  use Net::Ftp;
  use Compress::Zlib;
  
  my ($jobn, $infnam, $numb) = @_;

  $REQ::dbid = my $reqid = logNewRequest($CD::xmFTPc);
  my ($filen, $outpdir, $datetime) = setXferFileA($REQ::queuetype, $REQ::queuename, $REQ::dbid);

  $REQ::datafile = $outpdir."/".$filen.".DATA.TXT.gz";

  doLog("GETFTPQC " . $version . " receiving $numb data bytes to fullfill req $REQ::dbid");
  &$logrRtn("req $REQ::dbid will create file $REQ::datafile");

  my ($ftpch, $ftpserv, $ftpuser, $ftpport, $ftptype,
      $ftppwd, $ftppasv, @sitecmd, @dsnlist, @quotecmd);
  my $msgmax  = $REQ::msgmax;
  my $debug   = $REQ::debug;
  my $SrvName = $REQ::SrvName;
  my $xferOK = 0;
  
  my ($fhOut, $wrtRtn, $cloRtn);
  my ($dataBuff, @ftpStmt);
  $#sitecmd = -1;
  $#quotecmd = -1;
  $#dsnlist = -1;
  $ftptype = "I";
  
  do {
    (undef, undef, $REQ::sol, undef) = readLpdSock();
    $dataBuff .= $REQ::buffer;
    $REQ::bpos = $REQ::sol;
  } while ($REQ::rbytes < $numb);

  @ftpStmt = split("\n", $dataBuff);
  for ( @ftpStmt ) {
    &$logrRtn($_);
    /^HOST\s+(\S+)\s*$/ && do {
      doLog(" host Name ", $1);
      $ftpserv = $1;
      next;
    };
    /^PORT\s+(\S+)\s*$/ && do {
      doLog(" host Port ", $1);
      $ftpport = $1;
      next;
    };
    /^PASV\s.*$/ && do {
      doLog(" passive mode ");
      $ftppasv = 1;
      next;
    };
    /^USER\s+(\S+)\s*$/ && do {
      doLog(" host user ", $1);
      $ftpuser = $1;
      next;
    };
    /^PWD\s+(\S+)\s*$/ && do {
      doLog(" host password ", $1);
      $ftppwd = $1;
      next;
    };
    /^TYPE\s+(\S+)\s*$/ && do {
      doLog(" Transfer Type ", $1);
      $ftptype = $1;
      next;
    };
    /^SITE\s+(\S+\s*.*)$/ && do {
      doLog(" site cmd ", $1);
      $sitecmd[$#sitecmd+1] = $1;
      next;
    };
    /^QUOTE\s+(\S+\s*.*)$/ && do {
      doLog(" quote cmd ", $1);
      $quotecmd[$#quotecmd+1] = $1;
      next;
    };
    /^DSN\s+(\S+)\s*$/ && do {
      doLog(" host dsn ", $1);
      $dsnlist[$#dsnlist+1] = $1;
      next;
    };
  }
  $ftpserv = 'OS390.BIPOP.IT';
  
  unless ($ftpserv && $ftpuser && (scalar(@dsnlist) > 0)) {
    my $endmsg = "12 file Xfer failed - ";
    $endmsg .= "server unspecified - "  unless ($ftpserv);
    $endmsg .= "user unspecified - "    unless ($ftpuser);
    $endmsg .= "dataset unspecified - " unless (scalar(@dsnlist) > 0);
    $endmsg .= "terminating";
    doLog($endmsg);
    return undef;
  }
  
  if    ($ftpport and $ftppasv) {
    doLog(" logging in to ", $ftpserv, " port ", $ftpport, " pasv mode");
    $ftpch = new Net::FTP($ftpserv, Debug => $debug, Port => $ftpport, Passive => 1);
  }
  elsif ($ftpport) {
    doLog(" logging in to ", $ftpserv, " port ", $ftpport);
    $ftpch = new Net::FTP($ftpserv, Debug => $debug, Port => $ftpport);
  }
  elsif ($ftppasv) {
    doLog(" logging in to ", $ftpserv, " pasv mode");
    $ftpch = new Net::FTP($ftpserv, Debug => $debug, Passive => 1);
  }
  else {
    doLog(" logging in to ", $ftpserv);
    $ftpch = new Net::FTP($ftpserv, Debug => $debug);
  }
  return undef unless ($ftpch);
  if ($ftppwd) {
    $ftpch->login($ftpuser, $ftppwd);
  }
  else {
    $ftpch->login($ftpuser, "xrLpd");
  }
  
  my ($fcode, $fmsg) = ('999', 'no data set requested');
  my @ftpmsgs = $ftpch->message();
  chomp($fmsg = $ftpmsgs[-1]);
  $fcode = $ftpch->code();
  doLog("logon for $ftpserv completed - resp: $fcode - $fmsg");
  if ($fcode ne '230') {
    return undef;
  }
  $ftpch->type($ftptype);
  
  for my $cmd ( @sitecmd ) {
    doLog(" sending cmd ", $cmd);
    $ftpch->site($cmd);
  }
  
  for my $cmd ( @quotecmd ) {
    doLog(" sending cmd ", $cmd);
    $ftpch->quot($cmd);
  }
  
  $fhOut = gzopen($REQ::datafile, "wb") || die("DATA FILE OPEN ERROR $!");
#  $wrtRtn = sub {$fhOut->gzwrite($_[0]);};
#  $cloRtn = sub {$fhOut->gzclose;};
  
  doLog(" now receiving data into ", $REQ::datafile);
  my $bRead = 0;
  $xferOK = 1;
  
  ($fcode, $fmsg) = ('999', 'no data set requested');
  for my $dsn ( @dsnlist ) {
    my $data = $ftpch->retr($dsn);
#    my $fresp = $ftpch->response();
    @ftpmsgs = $ftpch->message();
    chomp($fmsg = $ftpmsgs[-1]);
    $fcode = $ftpch->code();
    
    my $inBuff = '';
    my $outmsg;
    my ($rbytes, $swlen, $dsbytes) = (0, 0, 0);
    if ($data && $fcode eq '125') {
      $outmsg = "receive for $dsn starting - resp: $fcode - $fmsg";
      doLog($outmsg);
      do {
	$rbytes = $data->read($inBuff, $msgmax, 15);
	$bRead += $rbytes;
	$dsbytes += $rbytes;
      }  while($rbytes && defined($swlen = $fhOut->gzwrite($inBuff)) && $swlen == $rbytes);
      $data->close();
#      $fresp = $ftpch->response();
      @ftpmsgs = $ftpch->message();
      chomp($fmsg = $ftpmsgs[-1]);
      $fcode = $ftpch->code();
      $outmsg = "receive for $dsn ended - bytes: $dsbytes resp: $fcode - $fmsg";
      doLog($outmsg);
    } else {
      $outmsg = "receive failed for $dsn - resp: $fcode - $fmsg";
      doLog($outmsg);
      $dsbytes = 0;
    }
    $xferOK = ($fcode eq '250' && $xferOK == 1);
    last unless $xferOK;
  }

  $fhOut->gzclose();
  $REQ::InBytes += $bRead;
  $REQ::job[$jobn]{wbytes} = $REQ::OutBytes = -s $REQ::datafile;
  $REQ::job[$jobn]{bytes}  = $bRead;
  doLog("bytes read: $bRead written: $REQ::OutBytes xferStat: $xferOK");
  $ftpch->quit();

  if ($bRead == 0 or $xferOK == 0) {
    doLog("receive session failed during ftp data transfer " .
	  "- $fcode - $fmsg");
    syswrite($REQ::LpdSock,"\x7F",1);
    return undef;
  }

  syswrite($REQ::LpdSock,"\x00",1);

  if (! $REQ::job[$jobn]{CtlFile}) {
    if (! recvJob($jobn) ) {
      unlink $REQ::datafile;
      return undef;
    }
  }

  $REQ::pagcnt = 0;

  map {s/\'//g} @dsnlist;
  (my $CtlStream = $REQ::job[$jobn]{'CtlStream'}) =~ s/^N.*$/'N'.join(' ', @dsnlist)/me;
#  print $CtlStream;
  return undef unless updateRequest($CD::stReceived,
				    $REQ::dbid,
				    $outpdir."/".$filen.".CNTRL.TXT",
				    $REQ::datafile,
				    $CtlStream);

  doLog("Data file received - bytes" .
	" recvd: "   . $REQ::InBytes .
	" written: " . $REQ::OutBytes .
	" - $fmsg");
#  syswrite($REQ::LpdSock,"\x00",1);


  return $bRead;

}

__END__
sub startFtpXfer{

  my $dataBuff = shift;
  my ($ftpserv, $ftpuser, $ftpport, $ftptype,
      $ftppwd, $ftppasv, @sitecmd, @dsnlist, @quotecmd);

  my @ftpStmt = split("\n", $dataBuff);
  for ( @ftpStmt ) {
    /^HOST\s+(\S+)\s*$/ && do {
      &$logrRtn(" host Name ", $1);
      $ftpserv = $1;
      next;
    };
    /^PORT\s+(\S+)\s*$/ && do {
      &$logrRtn(" host Port ", $1);
      $ftpport = $1;
      next;
    };
    /^PASV\s.*$/ && do {
      &$logrRtn(" passive mode ");
      $ftppasv = 1;
      next;
    };
    /^USER\s+(\S+)\s*$/ && do {
      &$logrRtn(" host user ", $1);
      $ftpuser = $1;
      next;
    };
    /^PWD\s+(\S+)\s*$/ && do {
      &$logrRtn(" host password ", $1);
      $ftppwd = $1;
      next;
    };
    /^TYPE\s+(\S+)\s*$/ && do {
      &$logrRtn(" Transfer Type ", $1);
      $ftptype = $1;
      next;
    };
    /^SITE\s+(\S+\s*.*)$/ && do {
      &$logrRtn(" site cmd ", $1);
      $sitecmd[$#sitecmd+1] = $1;
      next;
    };
    /^QUOTE\s+(\S+\s*.*)$/ && do {
      &$logrRtn(" quote cmd ", $1);
      $quotecmd[$#quotecmd+1] = $1;
      next;
    };
    /^DSN\s+(\S+)\s*$/ && do {
      &$logrRtn(" host dsn ", $1);
      $dsnlist[$#dsnlist+1] = $1;
      next;
    };
  }
  
  unless ($ftpserv && $ftpuser && (scalar(@dsnlist) > 0)) {
    my $endmsg = "12 file Xfer failed - ";
    $endmsg .= "server unspecified - "  unless ($ftpserv);
    $endmsg .= "user unspecified - "    unless ($ftpuser);
    $endmsg .= "dataset unspecified - " unless (scalar(@dsnlist) > 0);
    $endmsg .= "XR COMM " if ($xrClient);
    $endmsg .= "terminating";
    &$logrRtn($endmsg);
    syswrite($CliSock, "$endmsg\n") if ($xrClient);
    return undef;
  }
  
  if    ($ftpport and $ftppasv) {
    &$logrRtn(" logging in to ", $ftpserv, " port ", $ftpport, " pasv mode");
    $REQ::ftpCCH = new Net::FTP($ftpserv, Debug => $debug, Port => $ftpport, Passive => 1);
  }
  elsif ($ftpport) {
    &$logrRtn(" logging in to ", $ftpserv, " port ", $ftpport);
    $REQ::ftpCCH = new Net::FTP($ftpserv, Debug => $debug, Port => $ftpport);
  }
  elsif ($ftppasv) {
    &$logrRtn(" logging in to ", $ftpserv, " pasv mode");
    $REQ::ftpCCH = new Net::FTP($ftpserv, Debug => $debug, Passive => 1);
  }
  else {
    &$logrRtn(" logging in to ", $ftpserv);
    $REQ::ftpCCH = new Net::FTP($ftpserv, Debug => $debug);
  }
  return undef unless ($REQ::ftpCCH);
  if ($ftppwd) {
    $REQ::ftpCCH->login($ftpuser, $ftppwd);
  }
  else {
    $REQ::ftpCCH->login($ftpuser, "xrLpd");
  }
  
  my ($fcode, $fmsg) = ('999', 'no data set requested');
  my @ftpmsgs = $REQ::ftpCCH->message();
  chomp($fmsg = $ftpmsgs[-1]);
  $fcode = $REQ::ftpCCH->code();
  &$logrRtn("logon for $ftpserv completed - resp: $fcode - $fmsg");
  if ($fcode ne '230') {
#    &$logrRtn("Remote client notification in progress") if ($xrClient eq 1);
    syswrite($CliSock, "12 File Xfer failed - $fcode - $fmsg\n") if ($xrClient eq 1);
    return undef;
  }
  $REQ::ftpCCH->type($ftptype);
  
  for my $cmd ( @sitecmd ) {
    &$logrRtn(" sending cmd ", $cmd);
    $REQ::ftpCCH->site($cmd);
  }
  
  for my $cmd ( @quotecmd ) {
    &$logrRtn(" sending cmd ", $cmd);
    $REQ::ftpCCH->quot($cmd);
  }
}

sub CTMPMFTP {
# -*- Mode: Perl -*- ##################################################
# @(#) $Id: xrLpdServ.pl,v 1.8 2001/04/20 20:23:54 mpezzi Exp $ 
#
# Copyrights(c) EURISKOM s.r.l.
#######################################################################
  my $version = do { my @r = (q$Revision: 1.8 $ =~ /\d+/g); sprintf "%d."."%02d" x $#r, @r };

  use Net::Ftp;
  use Compress::Zlib;
  use Convert::EBCDIC;

  my ($CliSock, $to_read, $xrClient, $datafile,
      $queuetype, $queuename, $reqid, $CtlStream) = @_;
  my ($ftpserv, $ftpuser, $ftpport, $ftptype,
      $ftppwd, $ftppasv, @sitecmd, @dsnlist, @quotecmd);
  ($REQ::ftpCCH, $REQ::ftpDCH) = ();

  my $debug   = $REQ::debug;
  my $SrvName = $REQ::SrvName;
  my $xferOK = 0;

  my ($fhOut, $wrtRtn, $cloRtn);
  my ($dataBuff, @ftpStmt);
  $#sitecmd = -1;
  $#quotecmd = -1;
  $#dsnlist = -1;
  $ftptype = "I";
  
  sub GetMcLine {
#    my $xlate = new Convert::EBCDIC;
    my $fhIn = shift;

    my ($inrec, $outrec, $b1, $nc) = ();
    my $Escode = 0;

    #remember start of line position
    $REQ::sol = $REQ::bpos;
    
    do {
      
      #    ($blen, $bpos, $sol, $REQ::buffer) = readFtpSocket($fhIn) if (($REQ::blen - $REQ::bpos) < 1);
      readFtpSocket($fhIn) if (($REQ::blen - $REQ::bpos) < 1);
      return undef unless $REQ::buffer;
      
      $b1 = unpack("C", substr($REQ::buffer, $REQ::bpos, 1));$REQ::bpos++;
      
      if ( $b1 == 0 ) {
	# this is an escape code we need to read the next 2 bytes (block descriptor + data header)
	#      ($blen, $bpos, $sol, $REQ::buffer) = readFtpSocket($fhIn) if (($REQ::blen - $REQ::bpos) < 2);
	readFtpSocket($fhIn) if (($REQ::blen - $REQ::bpos) < 2);
	return undef unless $REQ::buffer;
	
	($Escode, $b1) = unpack('@'.$REQ::bpos." C1 C1", $REQ::buffer);$REQ::bpos += 2;
	# for mode C the descriptor code is valid only if it is EOF(0x40) or EOR(0x80)
	if ( $Escode ne 0x80 and $Escode ne 0x40 ) {
	  &$logrRtn("INVALID CONTROL BYTE ", $Escode , " - REC " , unpack('@'.$REQ::sol." H40", $REQ::buffer));
	  return undef;
	}
      } 
      # first bit of data header indicate following data type
      if ( $b1 & 0x80 ) {
	# compressed data follows
	my $ll = $b1 & 0x3f;
	if ( $b1 & 0x40 ) { 
	  # space byte to be repeated (data header) times
	  $outrec .= "\x40" x $ll;
	} else {
	  # follows byte to be repeated (data header) times 
	  #	($blen, $bpos, $sol, $REQ::buffer) = readFtpSocket($fhIn) if (($REQ::blen - $REQ::bpos) < $1);
	  readFtpSocket($fhIn) if (($REQ::blen - $REQ::bpos) < 1);
	  return undef unless $REQ::buffer;
	  $outrec .= substr($REQ::buffer, $REQ::bpos, 1) x $ll; $REQ::bpos++;
	}
      } else {
	# follows data block of (data header) length
	my $ll = $b1 & 0x7f;
	#	($blen, $bpos, $sol, $REQ::buffer) = readFtpSocket($fhIn) if (($REQ::blen - $REQ::bpos) < $ll);
	readFtpSocket($fhIn) if (($REQ::blen - $REQ::bpos) < $ll);
	return undef unless $REQ::buffer;
	$outrec .= substr($REQ::buffer, $REQ::bpos, $ll); $REQ::bpos += $ll;
      }
    } while ( $Escode eq 0 );
    my $cc = substr($outrec, 0, 1);
    (my $data = substr($outrec,1)) =~ s/\x40+$//;
    return (length($data) + 1, $cc, $cc . $data)
      
  }
  
  sub GetPage {
    my $fhIn = shift;

    my @page = ();
    my $nl = 0;
    while ( ! $REQ::ftpEof ) {
      my ($ll, $cc, $linein) = GetMcLine($fhIn);
#      print STDERR "line skipped", "\n" unless ($ll);
      last unless ($ll);
      if (($cc eq "\xf1" or ($cc eq "\x5a" and substr($linein, 3, 3) =~ /\xd3\xab[\xcc\xca]/)) && $nl > 0) {
	$REQ::pagcnt++;
	$REQ::bpos = $REQ::sol;
#	$REQ::lines--;
	return @page;
      }
      next if ($cc eq "\x5a"); # Eliminati control code 5A ... da includere ??
      $page[$nl++] = $linein;
#      $REQ::lines++;
    }
    &$logrRtn("Skipped $REQ::pagcnt $REQ::sol $REQ::bpos $REQ::blen ", 
	      unpack("\@".$REQ::sol." H".((($REQ::bpos - $REQ::sol)+2)*2), $REQ::buffer), "") if ($::debug);
    return @page ;
  }
  
  sub GetJobList {
    my @IndexPage = @_;

    my $translator = new Convert::EBCDIC;
    my @JobList = ();
    $#JobList = -1;
    &$logrRtn("Reading JobList at page $REQ::pagcnt") if ($::debug);
    for my $jline (reverse(@IndexPage)) {
      #    next if ($jline =~ /^\xf1/);
      my $d = $translator->toascii($jline);
      last if $d =~ /-----------/;
      my ( $ctdfold, $descr, $pages, $lines, $jobname) = unpack("\@9A10\@19A20\@45A10\@58A10\@71A8", $d);
      $lines =~ s/^ +//; 
      $pages =~ s/^ +//;
      $JobList[++$#JobList] = "jobname: $jobname descr: $descr pages: $pages lines: $lines";
    }
    return reverse(@JobList);
  }

  sub GetJobDesc {
    my $fhIn = shift;

    my $xlate = new Convert::EBCDIC;
    my $JobDesc = {};
    my ($foundit, $infos) = (0, 0);
  SEEK: while (! $REQ::ftpEof) {
      my @page = GetPage($REQ::ftpDCH);
      foreach my $row ( 0..$#page ) {
	my $line = $xlate->toascii($page[$row]);$REQ::lines++;
	$foundit = ($line =~ /^.*I S T R U Z I O N I\s+O P E R A T I V E.*$/) unless ( $foundit );
	if ( $foundit ) {
	SWITCH: {
	    ($line =~ /^.*PAGINE\s+:\s+(\d+)\s.*$/)   && do {$JobDesc->{'pages'} = $1; $infos += 1; last SWITCH;};
	    ($line =~ /^.*LINEE\s+:\s+(\d+)\s.*$/)    && do {$JobDesc->{'lines'} = $1; $infos += 2; last SWITCH;};
	    ($line =~ /^.*TABULATO\s+:\s+(\S+)\s.*$/) && do {$JobDesc->{'descr'} = $1; $infos += 4; last SWITCH;};
	    ($line =~ /^.*JOBNAME\s+:\s+(\w+)\s.*$/)  && do {$JobDesc->{'jname'} = $1; $infos += 8; last SWITCH;};
	    ($line =~ /^.*JOB ID\s+:\s+(\d+)\s.*$/)   && do {$JobDesc->{'jobid'} = $1; $infos += 16; last SWITCH;};
	    ($line =~ /^.*DATA\s+:\s+(\S+)\s.*$/)     && do {$JobDesc->{'jdate'} = $1; $infos += 32; last SWITCH;};
	    ($line =~ /^.*ORA\s+:\s+([\d:]+)\s.*$/)   && do {$JobDesc->{'jtime'} = $1; $infos += 64; last SWITCH;};
	  };
	  last SEEK if ($infos eq 127);
	}
      }
      last SEEK if ($foundit);
    }
    return %{ $JobDesc };
  }

  &$logrRtn("CTMPMFTP " . $version . " receiving $to_read data bytes to start ftp receive ($REQ::msgmax)");
  do {
    (undef, undef, $REQ::sol, undef) = readLpdSock();
    $dataBuff .= $REQ::buffer;
    $REQ::bpos = $REQ::sol;
  } while ($REQ::rbytes < $numb);

  
  my $bRead = 0;
  $xferOK = 1;
  
  ($fcode, $fmsg) = ('999', 'no data set requested');
  for my $dsn ( @dsnlist ) {
#    my $fresp = $REQ::ftpCCH->response();
    $REQ::ftpDCH = $REQ::ftpCCH->retr($dsn);
    @ftpmsgs = $REQ::ftpCCH->message();
    chomp($fmsg = $ftpmsgs[-1]);
    $fcode = $REQ::ftpCCH->code();
    
    my $inBuff = '';
    my $currcon = $con;
    my ($rbytes, $swlen, $dsbytes) = (0, 0, 0);
    my ($numrprt, $numfiles);
    if ($REQ::ftpDCH && $fcode eq '125') {
      ($REQ::buffer, $REQ::ftpEof, $REQ::sol, $REQ::bpos, $REQ::blen, $REQ::pagcnt) = ('', 0, 0, 0, 0, 0);
      my $xlate = new Convert::EBCDIC;
      &$logrRtn("receive for $dsn starting - resp: $fcode - $fmsg");
      my @ctmindex = GetJobList(GetPage($REQ::ftpDCH));
      ($numrprt, $numfiles) = (scalar(@ctmindex), 0);

      my $outmsg = "processing Job Index for $dsn ($numrprt reports):";
      send($CliSock, "INFO " . $outmsg . "\n", 0) if ($xrClient);
      &$logrRtn($outmsg);
      send($CliSock, "INFO " . "-" x 70 . "\n", 0) if ($xrClient);

      for my $jdesc ( @ctmindex ) {
	send($CliSock, "INFO INDEX  - " . $jdesc . "\n", 0) if ($xrClient);
	&$logrRtn("INDEX  - $jdesc");
      }
      send($CliSock, "INFO " . "-" x 70 . "\n", 0) if ($xrClient);
      
      while (! $REQ::ftpEof) {
	my %JobDesc = (GetJobDesc($REQ::ftpDCH));
	last if ($REQ::ftpEof eq 1);
	
	&$logrRtn("Starting to process ", $JobDesc{'pages'}, " for ", $JobDesc{'descr'},
		  " starting at $REQ::pagcnt $REQ::lines $REQ::sol $REQ::bpos") if ($::debug);
	
	$REQ::pagcnt = 0; $rbytes = $REQ::bread;$REQ::lines = 0;
	$reqid = logNewRequest($SrvName, $CliSock, 0, $xrClient, $queuetype, $queuename, $CD::xmFTPc) . "#" . $currcon;
	my ($xrspool, $outpdir, $filen, $datetime) = setReqAttr($CliSock, $queuetype, $queuename, $reqid);
	$datafile = $outpdir."/".$filen.".DATA.TXT.gz";
	$fhOut = gzopen($datafile, "wb") || die("DATA FILE OPEN ERROR $!");
	
	for my $numpag (1..$JobDesc{'pages'} ) {
	  my @currpage = GetPage($REQ::ftpDCH);
	  &$logrRtn("page $numpag read ", scalar(@currpage), "lines ($REQ::ftpEof)") if ($::debug);
	  foreach my $row ( @currpage ) {
	    $fhOut->gzwrite( pack("n",length($row)) . $row) if ($row);$REQ::lines++;
	  }
	  last if ($REQ::ftpEof eq 1);
	}
	$fhOut->gzclose();
	
	my $outmsg = "STORED - "
	           . "jobname: $JobDesc{jname} descr: $JobDesc{descr} "
	           . "jdate: $JobDesc{jdate} jtime:$JobDesc{jtime} "
	           . "pages: $REQ::pagcnt of $JobDesc{pages} lines: $REQ::lines of $JobDesc{lines}";
	&$logrRtn($outmsg, " into $datafile");
	syswrite($CliSock, "INFO " . $outmsg . "\n") if ($xrClient);
	if ($REQ::pagcnt ne $JobDesc{'pages'} or $REQ::lines ne $JobDesc{'lines'}) {
	  $xferOK = 0;
	  last;
	} else {

	  $CtlStream =~ s/\nJ(\w+)\.(\w+)\.(\w+)\.(\w+)\./\nJ$1.$2.$JobDesc{'jname'}.JOB$JobDesc{'jobid'}./;
	  updateRequest($CD::stReceived, $reqid, $queuetype, $queuename, $datetime,
		 	$CtlStream .
			"-ofileformat=record\n", 
			$ datafile);
	  $numfiles++;
	}
      }
      
      while(!$REQ::ftpEof eq 1) {
	GetPage($REQ::ftpDCH);
      }
      
      $REQ::ftpDCH->close();

      $dsbytes = $REQ::bread - $rbytes;
#      $fresp = $REQ::ftpCCH->response();
      @ftpmsgs = $REQ::ftpCCH->message();
      chomp($fmsg = $ftpmsgs[-1]) if (scalar(@ftpmsgs));
      $fcode = $REQ::ftpCCH->code();
      $outmsg = "receive for $dsn ended - reports: $numfiles of $numrprt bytes: $dsbytes";
      &$logrRtn($outmsg, " resp: $fcode - $fmsg");
      syswrite($CliSock, "INFO " . $outmsg . "\n") if ($xrClient);
    } else { # if data channel not ready
      &$logrRtn("receive failed for $dsn - resp: $fcode - $fmsg");
    }
    $xferOK = ($fcode eq '250' && $xferOK == 1 && $numfiles == $numrprt);
    last unless $xferOK;
  }
#  &$cloRtn();
  &$logrRtn(" bytes read: $REQ::bread xferStat: $xferOK");
  $REQ::ftpCCH->quit();
  
  if ($REQ::bread == 0 or $xferOK == 0) {
    &$logrRtn(" Receive session failed due to error during data transfer") ;
    if ($xrClient eq 1) {
      &$logrRtn("Remote client notification in progress");
      syswrite($CliSock, "12 File Xfer failed - $fcode - $fmsg\n");
    }
    return undef;
  } else {
    syswrite($CliSock, (($xrClient) ? "00 $REQ::bread bytes transferred - $fmsg\n" : "\x00"));
  }
  return $to_read;
}

