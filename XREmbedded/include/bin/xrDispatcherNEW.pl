#perl -w 

package main;

use strict vars;

use strict;
use Data::Dumper;
use XML::Simple;
use POSIX qw(strftime);
use File::Path qw();
use File::Basename;
use IO::File;

use Win32::OLE;
use Win32::OLE::Variant;
use Win32::Job;
use Win32::EventLog;

#use Digest::MD5;

use XReport;
use XReport::DBUtil;

$|=1;
#i::logit('begin');
#die 'begin';

INIT {
  $|=1;
  i::logit("starting execution of ".__FILE__." DEBUG: $main::debug VERBOSE: $main::veryverbose");
  $main::donotiterate = 1 if (grep /^\-donotiterate$/, @ARGV );
}


$main::SrvName = $main::Application->{ApplName}; #$ARGV[0] || 'CERE0-PDF1';
$main::XR_WRK  = $XReport::cfg->{'workdir'};
#$main::mcsock  = new IO::Socket::INET( Proto => 'udp', PeerAddr => '226.1.1.2:2000', Reuse => 1 );

$main::myName = ( split( /[\/\\]/, $0 ) )[-1];
$main::myName = ( split( /\./, $main::myName ) )[0];

my $XREPORT_HOME = $main::Application->{XREPORT_HOME};
my $XREPORT_SITECONF = $main::Application->{XREPORT_SITECONF};
my $sh_obj       = Win32::OLE->new("WScript.Shell");
my $env_obj      = $sh_obj->Environment("Process");
$env_obj->{'XREPORT_HOME'} = $XREPORT_HOME;
$env_obj->{'XREPORT_SITECONF'} = $XREPORT_SITECONF;

my $cfg_jobs = $XReport::cfg->{'jobs'}->{job};
my $glblsetenv = $XReport::cfg->{'jobs'}->{'setenv'} || {};
die "DISPATCHER: jobs config not found or no jobs found" unless ($cfg_jobs && scalar(@{$cfg_jobs})) ;
	
$main::EventId = $XReport::cfg->{useeventlog} || '10000' if (exists($XReport::cfg->{useeventlog}) && $XReport::cfg->{useeventlog} ne "-1");
	
my $dfltcfg = (XMLin( q{
<GlobalWorkQueues>
<table name="tbl_WorkQueue" handler="XReport::QUtil" tbalias="wq" >
<columns>ExternalTableName ExternalKey TypeOfWork WorkId InsertTime WorkClass Priority Status SrvName SrvParameters</columns>
</table>
<table name="tbl_JobReports" handler="XReport::JobReports" tbalias="jr" >
<columns>JobReportName JobReportId DateTime LocalFileName LocalPathId_IN LocalPathId_OUT OrigJobReportName RemoteFileName 
                 JobName JobNumber XferStartTime XferRecipient XferMode XferDaemon isAFP isFullAFP Status PendingOp TarFileNumberOUT TarRecNumberOUT ElabOutBytes</columns>
<subtable name="tbl_JobReportNames" tbalias="jn" joincondition="jr.JobReportName=jn.JobReportName" idcol="JobReportName" >
<columns>JobReportName JobReportDescr ReportFormat ElabFormat CharsPerLine LinesPerPage PageOrient HasCc FontSize FitToPage 
                 ParseFileName TargetLocalPathId_IN TargetLocalPathId_OUT TypeOfWork WorkClass Priority IsActive</columns>
</subtable>
</table>
<table name="tbl_JobSpool" handler="XReport::JobSpool" tbalias="js" >
<columns>JobSpoolId InsertTime JobReportId SpoolPathId InputFileName LocalFileName ContentType RemoteHostName RemoteHostAddr
                 RemoteQueueName JobName JobNumber XferDaemon XferStartTime XferRecipient XferMode RetryCount</columns>
</table>
</GlobalWorkQueues> 
}, ForceArray=>[qw(table subtable job step)]))->{table};

my $cfg_tables = { %{$dfltcfg}, %{( exists($XReport::cfg->{GlobalWorkQueues}->{table}) ? $XReport::cfg->{GlobalWorkQueues}->{table} : $dfltcfg)} } ;
foreach my $reqtab ( keys %{$cfg_tables} ) {
     my $pkgnm = 'XReport::'.do { $reqtab =~ /tbl_(\w+)/; $1; };
     $cfg_tables->{$reqtab}->{handler} = $pkgnm unless exists($cfg_tables->{$reqtab}->{handler});
}

my $sep = 'VALUES'; my $selvals = ''; my $reqtables = {};
foreach my $defid ( 0..$#{$cfg_jobs} ) { 
  $main::veryverbose && i::logit( "DISPATCHER: processing JOB $defid: ". Dumper($cfg_jobs->[$defid]));
  $cfg_jobs->[$defid]->{status} = 16 unless defined($cfg_jobs->[$defid]->{status});
  $cfg_jobs->[$defid]->{workdir_suffix} = "\${QR#wq.ExternalTableName}\\\${QR#wq.ExternalKey}" unless ( defined($cfg_jobs->[$defid]->{workdir_suffix}) && $cfg_jobs->[$defid]->{workdir_suffix} );
  $cfg_jobs->[$defid]->{jobtable} = "tbl_JobReports" unless defined($cfg_jobs->[$defid]->{jobtable});
  my $jtname = $cfg_jobs->[$defid]->{jobtable}; 
  my ( $tbname ) = grep /^$jtname$/i, keys %{$cfg_tables}; 

  if ( $tbname ) {
    $cfg_jobs->[$defid]->{jobtable_cfg} = $cfg_tables->{$tbname};
    $reqtables->{$tbname} = $cfg_tables->{$tbname} unless exists($reqtables->{$tbname});
    $cfg_jobs->[$defid]->{jobtable_handler} = $cfg_tables->{$tbname}->{handler};
    $cfg_jobs->[$defid]->{jobtable} = $tbname;
  }
  else {
       $cfg_jobs->[$defid]->{jobtable_cfg} = undef;
       $cfg_jobs->[$defid]->{jobtable_handler} = 'XReport::'.$jtname;
  }
  $cfg_jobs->[$defid]->{jobtable_handler} = $cfg_jobs->[$defid]->{handler} if exists($cfg_jobs->[$defid]->{handler});

  my $pkgnm = $cfg_jobs->[$defid]->{jobtable_handler};
  my $handler;
  eval  "\$handler = require $pkgnm;";
  my $errmsg = $@;
  if ($errmsg) {
     my $endmsg = "Load for $pkgnm failed - $errmsg";
     i::logit($endmsg);
     $cfg_jobs->[$defid]->{jobtable_handler} = undef;
  }
  else {
       $cfg_jobs->[$defid]->{jobtable_handler} = $pkgnm if (!$handler || $handler =~ /^\d+$/);
	   i::logit(__PACKAGE__." $pkgnm loaded successfully");
	}

  my $startstatus = $cfg_jobs->[$defid]->{status} + 1;
  if ( exists($cfg_jobs->[$defid]->{statusCodes}) ) {
     $startstatus = $cfg_jobs->[$defid]->{statusCodes}->{start} if exists($cfg_jobs->[$defid]->{statusCodes}->{start});
     $cfg_jobs->[$defid]->{statusCodes}->{end} = $startstatus + 1 unless exists($cfg_jobs->[$defid]->{statusCodes}->{end}); 
     $cfg_jobs->[$defid]->{statusCodes}->{retry} = $startstatus + 2 unless exists($cfg_jobs->[$defid]->{statusCodes}->{retry}); 
     $cfg_jobs->[$defid]->{statusCodes}->{error} = $startstatus + 14 unless exists($cfg_jobs->[$defid]->{statusCodes}->{error}); 
	}
  else {
     $cfg_jobs->[$defid]->{statusCodes} = { end => $startstatus + 1, retry => $startstatus + 2, error => $startstatus + 14 };
  }
  $cfg_jobs->[$defid]->{onerror} = 'HOLD' unless exists($cfg_jobs->[$defid]->{onerror});
  my ($on_normal, $on_error) = split(/,/, $cfg_jobs->[$defid]->{DISP}, 2)  if ( exists($cfg_jobs->[$defid]->{DISP}) );
  $on_normal ||= 'DELETE';
  $on_error ||= $cfg_jobs->[$defid]->{onerror};
  @{$cfg_jobs->[$defid]}{qw(DISP onerror)} = ($on_normal, $on_error);
  $selvals .= "$sep ".'('.join(',', map { (defined($_) ? "'$_'" : 'NULL') } ($defid, @{$cfg_jobs->[$defid]}{qw(jobtable type status WorkClass)}, $startstatus ) ).')';                    
  $sep = ',';
  foreach my $step ( @{$cfg_jobs->[$defid]->{step}} ) {
     for my $regexid ( qw(ABORTRE OKRE) ) {
       if ( exists($step->{$regexid}) ) {
         my $regex = delete($step->{$regexid});
         $step->{$regexid} = qr/$regex/;
       }
     }
     $step->{cond} = 'OK' if (!exists($step->{cond}) || $step->{cond} eq '');
     $step->{okrc} = '0' if (!exists($step->{okrc}) || $step->{okrc} eq '');
  }
  $main::debug && i::logit( "DISPATCHER parsed JOB $defid: ". Dumper($cfg_jobs->[$defid]));
}

my $col_array = [ map { "wq.$_"} split /\s+/s, $cfg_tables->{tbl_WorkQueue}->{columns} ]; 
my $out_array = [];
my $rslt_ids = [];
my $selfrom = "FROM tbl_WorkQueue wq WITH (HOLDLOCK ROWLOCK) ";

while ( my ($reqtab, $tinfo) = each %{$reqtables} ) {
     my $cpfx = $tinfo->{tbalias};
     my $tcols = [ split /\s+/s, $tinfo->{columns} ];
     push @{$rslt_ids}, $cpfx;
     push @{$col_array}, ( "${cpfx}.\$IDENTITY as ${cpfx}_id", map { "${cpfx}.$_ as ${cpfx}_$_"} @{$tcols});
     push @{$out_array}, ( "src.${cpfx}_id", "'$reqtab' as ${cpfx}_xrdb_tbl_origin", map { "src.${cpfx}_$_" } @{$tcols} );
     $selfrom .= "LEFT JOIN $reqtab AS $cpfx ON wq.ExternalKey = $cpfx.\$IDENTITY and wq.ExternalTableName = '$reqtab' ";
     if ( exists( $tinfo->{subtable}) ) {
        while ( my($stname, $subt) = each %{$tinfo->{subtable}} ) {
           my ($stcpfx, $jcond, $idcol) = @{$subt}{qw(tbalias joincondition idcol)};
           unless ( $idcol && $jcond && exists($subt->{columns}) ) {
              i::logit("table $reqtab join with $stname skipped due to misconfiguration ");
              next;
	}

           my $stcols = [ split /\s+/s, $subt->{columns} ];
           push @{$rslt_ids}, $stcpfx;
           push @{$col_array}, ( "${stcpfx}.$idcol as ${stcpfx}_id", map { "${stcpfx}.$_ as ${stcpfx}_$_"} @{$stcols});
           push @{$out_array}, ( "src.${stcpfx}_id", , "'$stname' as ${stcpfx}_xrdb_tbl_origin", map { "src.${stcpfx}_$_" } @{$stcols});
           $selfrom .= "LEFT JOIN $stname AS $stcpfx ON $jcond ";
	}
	}
}

my $col_list = join(', ', @{$col_array});
my $out_cols = join(', ', @{$out_array});

my $sql = qq{
MERGE dbo.tbl_Workqueue WITH (HOLDLOCK ROWLOCK) AS tgt
USING (
SELECT TOP 1 specs.defid, specs.startstatus, jwq.* 
FROM ( $selvals ) specs(defid, jobtable, TypeOfWork, status, workclass, startstatus) 
CROSS APPLY (
  SELECT $col_list 
  $selfrom
  WHERE ( (specs.jobtable = wq.ExternalTableName AND specs.TypeOfWork = wq.TypeOfWork  
           AND ((specs.WorkClass IS NULL and (wq.Workclass IS NULL or wq.WorkClass = 0)) 
		        OR specs.workclass = wq.workclass)) 
        AND ( (specs.status = wq.status AND (wq.srvname IS NULL OR wq.SrvName = '')) 
		   OR (specs.status <> wq.status AND (wq.srvname = '$main::SrvName' OR GETDATE() > (case when ISDATE(wq.Srvname) = 1 THEN wq.SrvName else '3000-01-01' end) )) 
		) )
) jwq
ORDER BY priority desc
) AS src
ON tgt.workid = src.workid AND tgt.externaltablename = src.externaltablename AND tgt.externalkey = src.externalkey AND tgt.typeofwork = src.typeofwork
WHEN MATCHED THEN UPDATE SET tgt.status = src.startstatus, tgt.srvname = '$main::SrvName' 
OUTPUT src.defid, INSERTED.*, convert(varchar, GETDATE(), 120) as WQElabStartTime,  $out_cols
;
};

i::logit("End build WQ Query: $sql");

$main::xrdbc = XReport::DBUtil->new(DBNAME=>'XREPORT');

my $retryre = qr/^RETRY(?:;(\d+))?$/i;
my $lasthh = '25';

i::logit("Entering main loop");
while (1) {
   my $dbr = $main::xrdbc->dbExecute_NORETRY($sql) || die "DB ERRORR";

  if ( $dbr->eof() ) {
     $dbr->Close();
	 my $nowhh = strftime( '%H', localtime );
	 if ( $nowhh ne $lasthh ) {
	    my $msgext = ( $lasthh eq '25' ? '' : ' in the last hour' );
        i::logit("no suitable WorkQueue items found$msgext - going to sleep");
		$lasthh = $nowhh;
	 }	
     sleep 10;
     next;
  } 
  $lasthh = '25';
  my $result = buildResult($dbr, $rslt_ids) || next;
  $dbr->Close();

  my $wkitem = delete $result->{'_ITEM@WQ_'};
  my ($WorkId, $ExternalKey, $ExternalTableName, $TypeOfWork) = @{$wkitem}{qw(WorkId ExternalKey ExternalTableName TypeOfWork)};
  my $cur_cfg = $cfg_jobs->[$wkitem->{defid}];
  i::logit(sprintf("WQ ITEM SELECTED DUE TO DEFINITION %d - tbl: %s id: %d tow: %d"
                  ,@{$wkitem}{qw(defid ExternalTableName ExternalKey TypeOfWork)}));

  my $setstring = [ ];
  my @wqsets = ();
  my $statusCodes = $cur_cfg->{statusCodes};
  
  my $tablesOK = ':'.join('::', map { $_->{xrdb_tbl_origin} } values %{$result} ).':';
  my @tablesMissing = ();
  if (  $cur_cfg->{jobtable_cfg} ) {
     my $tbname = $cur_cfg->{jobtable};
     push @tablesMissing, $tablesOK !~ /\:$tbname\:/  ;
     push @tablesMissing,( grep { $tablesOK !~ /\:$_\:/ } keys %{$cur_cfg->{jobtable_cfg}->{subtable}} ) if exists($cur_cfg->{jobtable_cfg}->{subtable});
  }

  # my @tablesRequired = [];
  # if (  $cur_cfg->{jobtable_cfg} ) {
     # push @tablesRequired, $cur_cfg->{jobtable};
     # push @tablesRequired, keys %{$cur_cfg->{jobtable_cfg}->{subtable}} if exists($cur_cfg->{jobtable_cfg}->{subtable});
  # }
  # my @tablesMissing = ( scalar(@{$tablesRequired}) ? grep { $tablesOK !~ /\:$_\:/ } @{$tablesRequired} : ());
  
  my $tableMissingAllowed = ':'.join(':', (exists($cur_cfg->{allowmissingtables}) ? (split(/[,; ]/, $cur_cfg->{allowmissingtables})) : ())).':'; 
  $main::debug && 
  i::logit(''.Dumper({JOB => $cur_cfg, TOK => $tablesOK, TMISS => \@tablesMissing, TALLOW => $tableMissingAllowed, WQ => $wkitem, QRES => $result})); #TREQ => $tablesRequired, 
  
  my ($status_verb, $work_status, $query_aborted);
  my $cur_work_dir;
  if ( scalar(@tablesMissing) && grep { $tableMissingAllowed !~ /\:$_\:/ } @tablesMissing ) {
     i::logit("Partial result from WQ query - missing rows from tables: \"".join(', ', @tablesMissing )."\" - cfg allows missing only $tableMissingAllowed - job $wkitem->{defid} skipped " );
     $status_verb = ( $cur_cfg->{onerror} =~ /$retryre/ ? 'retry' : 'error' );
     $work_status = $statusCodes->{$status_verb};
     $query_aborted = 1;
  }
  else {
    $query_aborted = 0;
    push @{$setstring}, ( "ElabStartTime='".$wkitem->{WQElabStartTime}."'"
	                    , "SrvName='".$main::SrvName."'"
						);

    doSETENV($cur_cfg, $wkitem, $result, $glblsetenv) if $glblsetenv;
    $cur_work_dir = init_worker($cur_cfg, $wkitem, $result);  
    my $jobOK = processJobSteps($cur_cfg, $wkitem, $result, $cur_work_dir, $main::SrvName, $setstring);
    $status_verb = ($jobOK != 0 ? 'end' : ( $cur_cfg->{onerror} =~ /$retryre/ ? 'retry' : 'error' ) );
    $work_status = $statusCodes->{$status_verb};
    i::logit(sprintf("Terminate work for TOW %d(%d) - Job Ended OK: %d - Work_status: %d", $TypeOfWork, $WorkId, $jobOK, $work_status));
    $main::veryverbose && i::logit("B4COMMIT: " . Dumper({WQ => \@wqsets, ET => $setstring}));  

    if ( $cur_cfg->{DISP} !~ /^LEAVE$/i ) {
       my $wrapperOK = completeJob($cur_cfg, $wkitem, $result, $cur_work_dir, $main::SrvName, $setstring, $status_verb);
       # $cur_cfg, $wkitem, $result, $handler, $cur_work_dir, $srvname, $setstring, $status
       $work_status = $statusCodes->{($wrapperOK ? 'end' : ( $cur_cfg->{onerror} =~ /$retryre/ ? 'retry' : 'error' ) ) } if ( $work_status == $statusCodes->{'end'} );
    }
    $main::veryverbose && i::logit("AFTERCOMMIT: " . Dumper({WQ => \@wqsets, ET => $setstring}));  
    push @$setstring, "ElabEndTime = '" . strftime( '%Y-%m-%d %H:%M:%S', localtime ) . "'";
    i::logit(sprintf("Completed work for TOW %d(%d) - ( %s %d ) - Work_status: %d", $TypeOfWork, $WorkId, $ExternalTableName, $ExternalKey, $work_status));
    #die "EC: $jobOK WS: $work_status";
  }
  
  my @wqsets = ("UPDATE tbl_WorkQueue SET ");

  push @wqsets, "Status = $work_status";
  
  if ( $work_status == $statusCodes->{'end'} ) {
            push @wqsets, "TypeOfWork = $cur_cfg->{To_TypeOfWork}"
              if exists( $cur_cfg->{To_TypeOfWork} ) && $cur_cfg->{To_TypeOfWork};
            if ( exists( $cur_cfg->{To_WorkClass} ) ) { 
              push @wqsets, "WorkClass = ".($cur_cfg->{To_WorkClass} || 'NULL');
            }  
            my $SrvName =  ($cur_cfg->{DISP} !~ /^(?:SUSPEND|HOLD)$/i ? 'NULL' : "'SUSPENDED'");
            $SrvName = "'$cur_cfg->{To_SrvName}'" if exists( $cur_cfg->{To_SrvName} ) && $cur_cfg->{To_SrvName};
            push @wqsets,
              ( "SrvName = $SrvName", "Work Item $WorkId ($ExternalTableName $ExternalKey) has been set for further processing" ) unless ( $cur_cfg->{DISP} !~ /^(?:RETRY|DELETE)$/i );
  }
  
  if ( ( $work_status == $statusCodes->{'end'}   && $cur_cfg->{DISP} !~ /^(?:LEAVE|SUSPEND|HOLD)$/i ) 
    || ( $work_status == $statusCodes->{'error'} && $cur_cfg->{onerror} =~ /^DELETE$/i ) ) {
            @wqsets = ( "DELETE from tbl_WorkQueue", "Work Item $WorkId ($ExternalTableName $ExternalKey) deleted from workqueue" );
  }
  elsif ( $work_status == $statusCodes->{'retry'} ) {
            (my $retry_interval) = ($cur_cfg->{onerror} =~ /$retryre/);
            $retry_interval ||= 60;
            my $retrytime = strftime( '%Y-%m-%d %H:%M:%S.%U', localtime( time() + $retry_interval ) );
            push @wqsets, ( "SrvName = convert(varchar, dateadd(ss, $retry_interval, GETDATE()), 120)",
                            "Work Item $WorkId ($ExternalTableName $ExternalKey) has been set for processing $retry_interval seconds from now ($retrytime)" );
  }
  else {
    push @wqsets, ( "SrvName = 'SUSPENDED'", "Work Item $WorkId ($ExternalTableName $ExternalKey) has been holded from processing (SUSPENDED)" );
  }

  #die Dumper({WQ => \@wqsets, ET => $setstring});  
  my $sqlst;
  if ( !$query_aborted && scalar(keys %{$result}) ) {
    #i::logit("SETSTRING:", Dumper($setstring));
    $setstring = [ grep !/^Status =/i, @$setstring ];
    push @$setstring, "Status = $work_status";
    $sqlst = "UPDATE $ExternalTableName set " . join( ', ', @$setstring );
    $sqlst .=  " OUTPUT INSERTED.* ";
    $sqlst .=  " WHERE \$IDENTITY = '$ExternalKey'";
    $main::veryverbose && i::logit("updating $ExternalTableName table: $sqlst");
    my $jrs = $main::xrdbc->dbExecute_NORETRY($sqlst) || die "DB ERROR";
    logUpdateResult($cur_cfg, $jrs, $ExternalTableName,  "UPDATED" );
    $jrs->Close();
  }
 
  #i::logit("WQSTRING:", Dumper(\@wqsets));
  my $logmsg = pop @wqsets;
  $sqlst  = shift @wqsets;
  $sqlst .= join( ', ', @wqsets ) ;
  $sqlst .=  " OUTPUT ".($sqlst =~ /^\s*DELETE/ ? "DELETED.* " : "INSERTED.* ");
  $sqlst .= sprintf(" WHERE WorkId = %d AND ExternalTableName = '%s' AND ExternalKey = '%s' AND TypeOfWork = %d  ", @{$wkitem}{qw(WorkId ExternalTableName ExternalKey TypeOfWork)});

  $main::veryverbose && i::logit("Updating WQ table: $sqlst");
  my $wqrs = $main::xrdbc->dbExecute_NORETRY($sqlst) || die "DB ERROR";
  logUpdateResult($cur_cfg, $wqrs, 'tbl_WorkQueue',  ($sqlst =~ /^\s*DELETE/ ? "DELETED" : "UPDATED") );
  $wqrs->Close();
 
  i::logit($logmsg);
  $main::logger->RemoveFile($main::SrvName.'WQ'.$WorkId);

  unless ( $main::debug ) {
	  if ( ( $work_status == $statusCodes->{'end'}   && $cur_cfg->{DISP} !~ /^(?:LEAVE|SUSPEND|HOLD)$/i )
		|| ( $work_status == $statusCodes->{'error'} && $cur_cfg->{onerror} =~ /^DELETE$/i ) ) {
		   do { removeTree($cur_work_dir); $cur_work_dir = undef }
				   unless ($cur_work_dir && (exists($cur_cfg->{keepworkdir}) && $cur_cfg->{keepworkdir}));
	  }
	  else { $cur_cfg->{keepworkdir} = 1; }
	  $cur_work_dir = undef if ( $cur_cfg->{DISP} eq 'LEAVE' );

	  if (  $cur_work_dir && (exists($cur_cfg->{keepworkdir}) && $cur_cfg->{keepworkdir}) ) {
		 if ( $cur_work_dir !~ /(?:[\W_]$ExternalTableName(?:[\W_]|[\W_].*[\W_])?$ExternalKey|[\W_]$ExternalKey(?:[\W_]|[\W_].*[\W_])?$ExternalTableName)(?:[\W_]|$)/ ) {
			(my $newname = join('_', ($cur_work_dir, $ExternalTableName, $ExternalKey))) =~ s/\//\\/g;
			$cur_work_dir =~ s/\//\\/g;
			removeTree($newname) if ( -e $newname );
			rename $cur_work_dir, $newname;
			i::logit("Work directory $cur_work_dir renamed to $newname");
		 }
		 $cur_work_dir = undef;
	  }
	  removeTree($cur_work_dir) if $cur_work_dir;
  }
  $main::donotiterate && last;
}

#sub i::logit { print ''.strftime( '%Y-%m-%d %H:%M:%S', localtime ), " $main::SrvName ($main::myName) => ", @_, "\n"; }

sub logUpdateResult {
   my ($step, $rs, $tbname, $modtype) = @_;
   if ( $rs->eof() ) {
     i::logit("Update $tbname failed - no rows updated");
   } 
   else {
       my @clist = (split /\s+/s, $cfg_tables->{$tbname}->{columns});
       i::logit(join("\n", ( "UPDATE to $tbname SUCCEDED - $modtype ROW:"
                     , join("\t", @clist)
                     , join("\t", ( $rs->GetFieldsValues(@clist) ) ) 
                     )));
   }
 }

sub buildResult {
  my ($rs, $rslt_ids) = @_;
  my $result = {};
  my $rsflds = $rs->Fields()->Count() - 1;
  $main::debug && i::logit("Processing $rsflds fields");
  foreach my $fnum ( 0..$rsflds ) {
    my ($ft, $fn) = split(/_/, $rs->Fields()->Item($fnum)->Name(), 2);
    if ( !$fn ) {
        $fn = $ft;
        $ft = '_ITEM@WQ_';
    }
    my $vp = $rs->Fields()->Item($fnum)->Value();
    if (  ref($vp) ) {
      my $field_value = [];
      push @$field_value, $vp->Date('yyyy-MM-dd') if $vp->can('Date');
      push @$field_value, $vp->Time('HH:mm:ss') if $vp->can('Time');
      push @$field_value, $vp unless scalar(@$field_value);
      $result->{$ft}->{$fn} = join(' ', @$field_value);
    }
    else {
      $result->{$ft}->{$fn} = $vp;
    }
  }
  for (@{$rslt_ids}) { 
     delete $result->{$_} unless defined($result->{$_}->{id}); 
  }
  
  return $result;
}
 
sub completeJob {
   $main::debug && i::logit("COMPLETEJOB ARGS:", Dumper(\@_));
   my ($cur_cfg, $wkitem, $result, $cur_work_dir, $srvname, $setstring, $status) = @_;
   my ($WorkId, $ExternalKey, $ExternalTableName, $TypeOfWork) = @{$wkitem}{qw(WorkId ExternalKey ExternalTableName TypeOfWork)};
   i::logit("Final processing for ${ExternalKey}(WQID: $WorkId TOW: $TypeOfWork) ($status)"); 

   my $rows = { map { $_->{xrdb_tbl_origin} => $_ } values %{$result} };

   my $handler = $cur_cfg->{jobtable_href} || return 1;

   my @set_result = (); 
   my $wrapname = ($status eq 'end' ? 'commitWQJob' : $status eq 'error' ? 'recoverWQJob' : 'retryWQJob');
   my $wrapper = $handler->can($wrapname);
   if ( $wrapper ) {
      my @wrapper_result; 
      eval "\@wrapper_result = &{\$wrapper}(\$handler, \$cur_cfg, \$wkitem, \$rows);";
      my $rtnerr = $@;
      if ($rtnerr) {
        i::logit("DISPATCHER: Wrapper ".(ref($handler) || $handler)."\:\:$wrapname failed - $rtnerr");
        return undef;
      }
      $main::debug && i::logit("Wrapper ".(ref($handler) || $handler)." method ended - result: ".Dumper(\@wrapper_result));
      return undef unless ( defined($wrapper_result[0]) || $#wrapper_result );
      push @set_result, grep { defined($_) } @wrapper_result;
   }
   else {
     i::logit("no status $status completion routines found for handler: ".(ref($handler) || $handler || 'UNDEF' ));
     return 1;
   }
#   die Dumper($cur_cfg);
   my $ppsqlfn = "$cur_work_dir/\$\$POST.\$\$PROCESS\$\$.$wkitem->{ExternalKey}.sql";
   if ( $status eq 'end' && -e $ppsqlfn && -s $ppsqlfn ) {
           my $ppsqlfh = new FileHandle("<$ppsqlfn");
           my $dbc     = $main::xrdbc->dbExecute("BEGIN TRANSACTION");
           while (<$ppsqlfh>) {
               chomp;
               my $sqlstmnt = $_;
               eval { $main::xrdbc->dbExecute($sqlstmnt) };
               my $sqlerr = $@;
               if ( $sqlerr ) {
                  i::logit("DISPATCHER: error during SQL statement in \"$ppsqlfn\" - $sqlerr");
                  $main::xrdbc->dbExecute("ROLLBACK TRANSACTION");
                  return undef;
               }
           }
           $main::xrdbc->dbExecute("COMMIT TRANSACTION");
   }

   push @{$setstring}, (@set_result) if scalar(@set_result);
   return 1;
}

sub get_status {
	my $currjob = shift;
	my $status = $currjob->status();
	my $jpid = shift @{ [(keys %$status)] };
	my $times = $status->{$jpid}->{time};
	my $sn = $main::jobsinfo->{'_PID'.$jpid}->{_sn};
    my $statmsg = "JobMon $jpid STATUS - process: $sn kernel: $times->{kernel}, User: $times->{user}, elapsed: $times->{elapsed}";
	i::logit($statmsg);
	return $statmsg;
}

use constant DESTINATION => '226.1.1.2:2000'; 

sub JobMon {
	my $currjob = shift;
	my $statmsg = get_status($currjob);
#	$main::mcsock->send($statmsg, 0, DESTINATION) if $main::mcsock;
	return 0;
}

sub seek_error_msg {
    (my $fn = shift ) =~ s/\\/\//g;
  my $rc = 0;

    unless ( -s $fn ) {
       i::logit("FILE \"$fn\" is EMPTY");
       return 0;
  }
    my $basefn = File::Basename::basename($fn);
    my $fh = IO::File->new("<$fn") || die "Cannot open $fn: $! - $^E\n";
    binmode $fh;
    $fh->read( my $buff, -s $fn );
    $fh->close();
    if ( $buff =~ /\s(Out Of Memory\!|BEGIN failed--compilation aborted|syntax +error|(?:UN)?RECOVERABLE\s+ERROR|Status\s*=\s*31)\s/si ) {
       i::logit("Failure detected ($1) into \"$basefn\" " );
       return 1;
    }
    return 0;
  
}

sub logFileContent {
  my ($pfx, $fn) = (shift, shift);
  $fn =~ s/\\/\//g;
  unless (-s $fn) {
    return i::logit("$pfx - EMPTY");
  }
    my $fh = IO::File->new("<$fn") || return i::logit("$pfx - unable to open \"$fn\" - $! - $^E");

  binmode $fh;
  $fh->read(my $buff, -s $fn);
  close $fh;
    return i::logit( join("\n", map { "$pfx - $_" } split /[\r\n]+/, $buff) );
}

sub buildStepCmd {
   my ($job, $step, $wkitem, $result, $cur_work_dir, $srvname) = @_;
	
   my $cmd = $step->{cmd};
   my $XREPORT_HOME = $main::Application->{XREPORT_HOME};
   (my $jobhome) = (grep /^XREPORT_HOME/, keys %{$job->{setenv}});
   $XREPORT_HOME = $job->{setenv}->{$jobhome}->{content} if $jobhome;
   (my $stephome) = (grep /^XREPORT_HOME/, keys %{$step->{setenv}});
   $XREPORT_HOME = $step->{setenv}->{$stephome}->{content} if $stephome;
   my $cmdfpath = '';
#   $cmdfpath .= "$XREPORT_HOME\\"; # if ( $cmd =~ /\.pl\s*$/i );
   $cmdfpath .= $cmd;

#   unless ( -e $cmdfpath && -f $cmdfpath ) {
#         doEventLog($job, $step, $wkitem, $result, "script not accessible - \"$cmdfpath\" - $^E");
#        return undef;
#    }
   my ( $logid, $scriptdir, $scriptsfx) = fileparse($step->{cmd},qr/\.[^.]*/);
   my $scriptname = $logid.$scriptsfx;
   my $myperl = $^X;
   $myperl = $step->{perlexe} if exists($step->{perlexe});
   my @command = ( ($scriptsfx =~ /^.pl$/i ? $myperl :  $scriptsfx =~ /^.(?:cmd|bat)$/i ? $ENV{COMSPEC} : $cmdfpath ), $scriptname);
   unless ( $scriptsfx =~ /^\.(?:pl|exe)$/i && grep { length && -e $_.'\\'.$scriptname } map {s/"//g; s/\\$//; $_ } split(';', $ENV{PATH}) ) {
      i::logit( "DISPATCHER: unable to find $scriptname in PATH - aborting step");
      return undef;
   }
   if ( $command[0] eq  $myperl ) {
      @command = ($myperl); 
#      push @command, ("-I", "\"$XREPORT_HOME\\perllib\"", "\"$cmdfpath\"");
#    INCLUDE MUST BE TAKEN FROM PERL5LIB ENV VARIABLE FIRST
      push @command, ("-S", $scriptname);
   }
   elsif ( $command[0] eq  $ENV{COMSPEC} ){
      push @command, ("/I", "/C", "\"$cmdfpath\"");
   }
   push @command, "\"$cur_work_dir\"", $wkitem->{ExternalKey}, "-N", $srvname;
   push @command, solveVar($wkitem, $result, $step->{parameters}) if ( $step->{parameters} );
   push @command, ('-SrvParameters', solveVar($wkitem, $result, $wkitem->{SrvParameters} )) if ( $wkitem->{SrvParameters} );
   my $debugflag = '';
   if ( ($command[0] eq  $myperl) && $main::veryverbose ) {
      push @command, '-dd';
   }
   elsif ( ($command[0] eq  $myperl) && $main::debug ) {
      push @command, '-d';
   }

=pod    

# perl -I "D:\XRTGTLIB\xreport_home\perllib" "D:\XRTGTLIB\xreport_home\bin\xrJobReports2PDF.pl"  "//localhost/D$/XREPORT/work/XR_TEST_PDF1" 40626 -N XR_TEST_PDF1

# use this code to test dispatcher behavior
   my $exename = $^X;
#   my $command = "-I \"$XREPORT_HOME\\perllib\" \"$XREPORT_HOME\\bin\\xrJobReports2PDF.pl\" " ;
   my $command = "-I \"$XREPORT_HOME\\perllib\" -MData::Dumper -le \"print 'ARGV:', Dumper(\\\@ARGV), 'ENV:', Dumper(\\\%ENV); die;\" \"$cmdfpath\"";
#   my $command = "$logid -I \"$XREPORT_HOME\\perllib\" ..\\testCalledScript.pl \"$cmdfpath\"";
#   $exename = $ENV{COMSPEC};
#   $command = "testExitCodes.cmd /I /C ..\\testExitCodes.cmd $scriptname \"$cmdfpath\"";


   $command .= " \"$cur_work_dir\" $wkitem->{ExternalKey} -N $srvname "
								 .( exists($step->{parameters}) ? ' '.$step->{parameters} : ' ')
								 .( $wkitem->{SrvParameters} ? ' -SrvParameters '.$wkitem->{SrvParameters} : ' ');
   
   return ($exename, $command);

=cut
   
   return (shift(@command), join(' ', @command));
  }
  
 sub processStep {
   my ($cur_cfg, $step, $wkitem, $result, $cur_work_dir, $srvname, $setstring) = @_;
   my ($ExternalTableName, $ExternalKey) = @{$wkitem}{qw(ExternalTableName ExternalKey)};
#   i::logit("DOJOB ARGS:", Dumper($step));
   my $cond = $step->{cond} || '0,EQ';
   i::logit("Invoking script $step->{cmd} as specified in JobStep $step->{order} - COND: $cond");

   my ($exename, $command) = buildStepCmd($cur_cfg, $step, $wkitem, $result, $cur_work_dir, $srvname );
   return undef unless $exename;

   my ( $logid, $scriptdir, $scriptsfx) = fileparse($step->{cmd}, qr/\.[^.]*/);
   my $scriptname = $logid.$scriptsfx;
   $logid = $step->{stepname} if $step->{stepname};  
    
   my $logfnm = $cur_work_dir . '/' . $logid . $wkitem->{ExternalKey}. '.#' . $step->{order};
   doSETENV($cur_cfg, $wkitem, $result, $step->{setenv}) if exists($step->{setenv});
    
   # my $w32job = new Win32::Job();   my $jpid = $w32job->spawn( $exename, $command,
                # { stdin  => 'NUL',                                                   # the NUL device
                  # stdout => $logfnm . '.stdout',
                  # stderr => $logfnm . '.stderr',
			    # }
			   # );
      # $main::jobsinfo->{'_PID'.$jpid } = {_sn => $scriptname};

   # $w32job->watch( \&JobMon, 10 );
   # my $statmsg = get_status($w32job);
      # $main::mcsock->send($statmsg, 0, DESTINATION) if $main::mcsock;
   # my $exit_code = undef;
   # $exit_code = $w32job->status()->{$jpid}->{'exitcode'};
      # delete $main::jobsinfo->{'_PID'.$jpid};

#   (my $redirect = ' 1>'.$logfnm.'.stdout 2>'.$logfnm.'.stderr') =~ s/\//\\/g;

#   my $exit_code = system("$exename $command".$redirect);
    
#   $command .= $redirect;
   i::logit( "DISPATCHER: exename: $exename command=$command ");

   my ($abortre, $okre) = @{$step}{qw(ABORTRE OKRE)};
   my ($step_failed, $step_is_ok) = (0, 0);
   my $linect = 0;
   open STEPCMD, "$exename $command".' 2>&1 |' or die "Couldn't call $exename - $^E";
   while ( <STEPCMD> ) {
     chomp;
     $linect += 1;
     $step_failed = $linect if (defined($abortre) && $_ =~ /$abortre/);
     $step_is_ok = $linect if (defined($okre) && $_ =~ /$okre/);
	 i::logit("WorkId $wkitem->{WorkId} ( $wkitem->{ExternalTableName} $wkitem->{ExternalKey} ) JobStep $step->{order} ($logid) - $_");
      }
   close STEPCMD;
   my $exit_code = $?;
   my $steprc = unpack('s>',pack('n', $exit_code));
   # RC for DIE or syntax: 255, RC for out of memory: 12 
   i::logit("WorkId $wkitem->{WorkId} ( $wkitem->{ExternalTableName} $wkitem->{ExternalKey} ) JobStep $step->{order} script ended with RC $steprc ($exit_code)" );
   if ( $step_failed == 0 && $step_is_ok == 0) {  return ("$exit_code" eq "$step->{okrc}" ? 1 : 0) ; } 
   elsif ( $step_failed == $step_is_ok ) {
    i::logit("Step ended in error due to Matching conflict in line: $step_failed - ABORTRE: $abortre - OKRE: $okre"); 
    return 0; 
   } 
   elsif ( $step_failed > $step_is_ok ) {
    i::logit("error string matched in output - Assuming step abnormally ended"); 
    return 0; 
   }
   elsif ( $step_is_ok > $step_failed  ) { 
    i::logit("Normal end string matched in output - Assuming step ended normally"); 
    return 1; 
   }
   return ("$exit_code" eq "$step->{okrc}" ? 1 : 0) ;
    
}
  
sub removeTree {
  my ($cur_work_dir) = @_;
  ( my $w32dir = $cur_work_dir );# =~ s/\//\\/g;
  $main::debug && i::logit("DISPATCHER: removeTree requested by ".join('::',caller()));
  File::Path::remove_tree($w32dir, { result => \my $list, error => \my $delrc} );
  $main::debug && i::logit("DISPATCHER: deleted: $_") for @$list;
  i::logit("DISPATCHER: $cur_work_dir delete ".( scalar(@$delrc) != 0 ? "error - $^E" : 'successfull'));
}  
 
sub solveVar {
   my ($wkitem, $result, $invar ) = @_;
   (my $val = $invar) =~ s/\$\{QR#wq\.([^\}]+)\}/$wkitem->{$1}/g;
   $val =~ s/\$\{QR#([^\.]+)\.([^\}]+)\}/$result->{$1}->{$2}/g;
   $val =~ s/\$\{SVC#appl\.([^\}]+)\}/$main::Application->{$1}/g;
   $val =~ s/\$\{SVC#ENV\.([^\}]+)\}/$ENV{$1}/g;
   return $val;
}
	
	
sub doSETENV {
  my ($cur_cfg, $wkitem, $result, $setlist) = @_;
  
  while ( my ($var, $val) = each %{$setlist} ) {
      if ( $var eq '_ADDPATH') {
	     (my $currentpath = $ENV{'PATH'}) =~ s/;?\s*$/;/;
		 $val = $val->{content} if(ref($val));
		 $ENV{'PATH'} = $currentpath.$val;
         i::logit("DISPATCHER: doSETENV added item to PATH: \"$val\"");
	     next;
	  }
      i::logit("DISPATCHER: doSETENV setting ENV $var to \"$val->{content}\"");
      $ENV{$var} = solveVar($wkitem, $result, $val->{content});
	}
  }

sub init_worker {
   my ($cur_cfg, $wkitem, $result ) = ( shift, shift, shift);
   my $wkdirsfx = solveVar($wkitem, $result, $cur_cfg->{workdir_suffix} );
   
   $cur_cfg->{jobworkdir} = "$main::XR_WRK/$wkdirsfx";
   if ( grep( /^\-WORKDIR$/, @ARGV ) ) {
        for my $argi ( 0..$#ARGV ) {
            if ( $ARGV[$argi] eq '-WORKDIR' ) {
                $argi += 1;
                $cur_cfg->{jobworkdir} = $ARGV[$argi];
                last;
    }
        }
   }
   my ( $cur_work_dir, $WorkId, $ExternalTableName, $ExternalKey, $status ) =
      ( $cur_cfg->{jobworkdir}, @{$wkitem}{qw(WorkId ExternalTableName ExternalKey Status)} );
   i::logit("DISPATCHER: Checking workdir \"$cur_work_dir\" sfx: $wkdirsfx - ".($cur_work_dir =~ /^[^\s]+[\\\/]\Q$wkdirsfx\E$/ ? 1 : 0).' - '.(-e $cur_work_dir ? 1 : 0).' - '.(-d $cur_work_dir ? 1 : 0));
   if ( $wkdirsfx && $cur_work_dir =~ /^[^\s]+\/\Q$wkdirsfx\E$/ && -e $cur_work_dir && -d $cur_work_dir ) {
        i::logit("DISPATCHER: $cur_work_dir already exists - attempting to delete");
		removeTree($cur_work_dir);
   }
   $main::debug && i::logit("DISPATCHER: Create work directory $cur_work_dir");
   eval { File::Path::mkpath($cur_work_dir); };
   if ($@) {
        i::logit("DISPATCHER: Could not create $cur_work_dir: $@");
        die "DISPATCHER: Could not create $cur_work_dir: $@\n";
   }
   (my $elabdt = $wkitem->{WQElabStartTime}) =~ s/[^\d]//g;
   my $logfile = "$cur_work_dir/\$\$WQ\$\$$wkitem->{WorkId}.D$elabdt.LOG";
   $main::logger->AddFile($main::SrvName.'WQ'.$wkitem->{WorkId}, $logfile);
   
   my $handler = $cur_cfg->{jobtable_handler};
   my $hinit = $handler->can('new') if ( $handler );
   if ( $hinit ) { 
     eval "\$cur_cfg->{jobtable_href} = &{\$hinit}(\$handler, job => \$cur_cfg, workdir => \$cur_work_dir, WQ => \$wkitem, QRESULT => \$result);";
     my $initerr = $@;
     if ( $initerr ) {
        i::logit("Initialization (new) for $handler failed - $initerr");
        $cur_cfg->{jobtable_href} = $handler;
    }
  }
  else {
        $cur_cfg->{jobtable_href} = $handler;
    }
   
   i::logit("DISPATCHER: Worker initialized - WorkDir: $cur_work_dir - href: ".(ref($cur_cfg->{jobtable_href}) || $cur_cfg->{jobtable_href} || 'UNDEF'));
   return $cur_work_dir;
  
}

sub  processJobSteps {
   my ($cur_cfg, $wkitem, $result, $cur_work_dir, $srvname, $setstring) = @_;
   #i::logit("processJobSteps DOJOB ARGS:", Dumper($cur_cfg->{setenv}));
   my $handler = $cur_cfg->{jobtable_cfg}->{href};
   doSETENV($cur_cfg, $wkitem, $result, $cur_cfg->{setenv}) if exists($cur_cfg->{setenv});
   my $jobStat = 1;
   i::logit("Processing job to satisfy rule $wkitem->{defid} matching WorkId $wkitem->{WorkId} ( $wkitem->{ExternalTableName} $wkitem->{ExternalKey} )");
   foreach my $step ( sort { $a->{order} <=> $b->{order} } @{$cur_cfg->{step}} ) {
       if ( $jobStat == 0  && ( $step->{cond} !~ /^(?:EVEN|ONLY)$/i ) ) {
          i::logit("WorkId $wkitem->{WorkId} ( $wkitem->{ExternalTableName} $wkitem->{ExternalKey} ) JobStep $step->{order} skipped because of previous steps failures");
          next;
       }
       if ( $jobStat == 1  && ($step->{cond} =~ /^ONLY$/i) ) {
          i::logit("WorkId $wkitem->{WorkId} ( $wkitem->{ExternalTableName} $wkitem->{ExternalKey} ) JobStep $step->{order} skipped - DISP=ONLY specified ");
          next;
       }
       my $stepStat = processStep($cur_cfg, $step, $wkitem, $result, $cur_work_dir, $srvname, $setstring);
       $jobStat = $stepStat;
       doEventLog( $cur_cfg, $step, $wkitem, $result ) if ($stepStat == 0);      
   } 
   return $jobStat;
}

sub doEventLog {
    my ($job, $step, $wkitem, $result) = (shift, shift, shift, shift);
    my @es   = ( $wkitem->{WorkId}
               , ( $wkitem->{ExternalTableName} || 'N/A')
               , ( $wkitem->{ExternalKey} || 'N/A' )
               , ( $step->{stepname} || $step->{cmd} )
               , $step->{order}
               , $wkitem->{defid}
               );
    my $evData = sprintf("failure processing WQ item %s ( %s %s ) during execution of %s ( step %s of cfg item %s )", @es);
    i::logit("$evData - ".join(' - ', @_));
    my $evID = $step->{'EventID'} || $job->{EventId} || $main::EventId;
    return unless ( $main::EventId && exists($XReport::cfg->{useeventlog}) && $XReport::cfg->{useeventlog} ne "-1");
    
    unless ( $main::EventLog ) {
       $main::EventLog = Win32::EventLog->new( "Application", '.' ) or die "Can't open Application EventLog\n";
    }
    $main::EventLog->Report(
        my $ei = {
            Source    => "XReport",
            Computer  => '.',
            EventType => EVENTLOG_ERROR_TYPE,
            Category  => 0,
            EventID   => $evID,
            Data   => join( "\0", $main::SrvName, @es),
            Strings =>  join( "\0", ("Service $main::SrvName ".$evData, @_))
                 }
    );
}

__END__

	
merge tbl_WorkQueue as twq
using ( select * from (  
SELECT status + 1 as endcode, status + 2 as retrycode, status + 14 as errcode, *  WHERE (STATUS = 16 AND TypeOfWork = 1 and SrvName is NULL)
union 
SELECT status + 1 as endcode, status + 2 as retrycode, status + 14 as errcode, *  WHERE (STATUS = 16 AND TypeOfWork = 1 and SrvName is NULL)
union 
SELECT status + 1 as endcode, status + 2 as retrycode, status + 14 as errcode, *  WHERE (SrvName = '$main::SrvName' AND TypeOfWork = 1 )
)

      SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 11   AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 12   AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 13   AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 24   AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 33   AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 34   AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 44   AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 50   AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 54   AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 64   AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 84   AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 90   AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 999  AND Status = 16) AND SrvName IS NULL )
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 9990 AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 9991 AND Status = 0)  AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 9992 AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 9993 AND Status = 0)  AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 9994 AND Status = 16) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 9995 AND Status = 32) AND SrvName IS NULL ) 
UNION SELECT * FROM tbl_WorkQueue WHERE ((TypeOfWork = 9996 AND Status = 48) AND SrvName IS NULL )
UNION SELECT * FROM tbl_WorkQueue WHERE (SrvName='$main::SrvName' AND (
                    (TypeOfWork = 11)   OR (TypeOfWork = 12)   OR (TypeOfWork = 13)   OR (TypeOfWork = 24)   OR (TypeOfWork = 33) 
                 OR (TypeOfWork = 34)   OR (TypeOfWork = 44)   OR (TypeOfWork = 50)   OR (TypeOfWork = 54)   OR (TypeOfWork = 64) 
                 OR (TypeOfWork = 84)   OR (TypeOfWork = 90)   OR (TypeOfWork = 999)  OR (TypeOfWork = 9990) OR (TypeOfWork = 9991) 
                 OR (TypeOfWork = 9992) OR (TypeOfWork = 9993) OR (TypeOfWork = 9994) OR (TypeOfWork = 9995) OR (TypeOfWork = 9996)
                 ))
UNION SELECT * FROM tbl_WorkQueue WHERE ( (Status & 3) = 3 AND ISDATE(SrvName) = 1 AND GETDATE() > SrvName AND (
                    (TypeOfWork = 11) OR (TypeOfWork = 12) OR (TypeOfWork = 13) OR (TypeOfWork = 24) OR (TypeOfWork = 33) 
                 OR (TypeOfWork = 34) OR (TypeOfWork = 44) OR (TypeOfWork = 50) OR (TypeOfWork = 54) OR (TypeOfWork = 64) 
                 OR (TypeOfWork = 84) OR (TypeOfWork = 90) OR (TypeOfWork = 999) OR (TypeOfWork = 9990) OR (TypeOfWork = 9991) 
                 OR (TypeOfWork = 9992) OR (TypeOfWork = 9993) OR (TypeOfWork = 9994) OR (TypeOfWork = 9995) OR (TypeOfWork = 9996)
                 ) ) 
ORDER BY Priority DESC, WorkId

( VALUES(11, 16, NULL, NULL)
,(11   , 16, NULL, NULL) 
,(12   , 16, NULL, NULL) 
,(13   , 16, NULL, NULL) 
,(24   , 16, NULL, NULL) 
,(33   , 16, NULL, NULL) 
,(34   , 16, NULL, NULL) 
,(44   , 16, NULL, NULL) 
,(50   , 16, NULL, NULL) 
,(54   , 16, NULL, NULL) 
,(64   , 16, NULL, NULL) 
,(84   , 16, NULL, NULL) 
,(90   , 16, NULL, NULL) 
,(999  , 16, NULL, NULL)
,(9990 , 16, NULL, NULL) 
,(9991 , 0, NULL, NULL) 
,(9992 , 16, NULL, NULL) 
,(9993 , 0, NULL, NULL) 
,(9994 , 16, NULL, NULL) 
,(9995 , 32, NULL, NULL) 
,(9996 , 48, NULL, NULL))


--WITH  specs (defid, TypeOfWork, Status, WorkClass, startstatus) AS
--(SELECT * FROM ( VALUES (0,11,16,NULL,17), (1,12,16,NULL,17), (2,13,16,NULL,17), (3,24,16,NULL,17), (4,33,16,NULL,17), (5,34,16,NULL,17), (6,44,16,NULL,17)
--                      , (7,50,16,NULL,17), (8,54,16,NULL,17), (9,64,16,NULL,17), (10,84,16,NULL,17), (11,90,16,NULL,17), (12,999,16,NULL,17), (13,9990,16,NULL,17)
--                      , (14,9991,0,NULL,1), (15,9992,16,NULL,17), (16,9993,0,NULL,1), (17,9994,16,NULL,17), (18,9995,32,NULL,33), (19,9996,48,NULL,49)
--                      ) selection(defid, TypeOfWork, status, workclass, startstatus)
--)
MERGE dbo.tbl_Workqueue WITH (HOLDLOCK ROWLOCK) AS tgt
USING (
SELECT TOP 1 * FROM (
SELECT specs.defid, specs.startstatus, 1 AS selpri, jwq.*
--FROM specs
FROM ( VALUES (0,11,16,NULL,17), (1,12,16,NULL,17), (2,13,16,NULL,17), (3,24,16,NULL,17), (4,33,16,NULL,17), (5,34,16,NULL,17), (6,44,16,NULL,17), (7,50,16,NULL,17), (8,54,16,NULL,17)
            , (9,64,16,NULL,17), (10,84,16,NULL,17), (11,90,16,NULL,17), (12,999,16,NULL,17), (13,9990,16,NULL,17), (14,9991,0,NULL,1), (15,9992,16,NULL,17), (16,9993,0,NULL,1), (17,9994,16,NULL,17)
            , (18,9995,32,NULL,33), (19,9996,48,NULL,49) ) specs(defid, TypeOfWork, status, workclass, startstatus)
CROSS APPLY (
  SELECT wq.ExternalTableName, wq.ExternalKey, wq.TypeOfWork, wq.WorkId, wq.InsertTime, wq.WorkClass, wq.Priority, wq.Status, wq.SrvName, wq.SrvParameters
  , jr.JobReportName as jr_JobReportName, jr.JobReportId as jr_JobReportId, jr.DateTime as jr_DateTime, jr.LocalFileName as jr_LocalFileName, jr.LocalPathId_IN as jr_LocalPathId_IN
  , jr.LocalPathId_OUT as jr_LocalPathId_OUT, jr.OrigJobReportName as jr_OrigJobReportName, jr.RemoteFileName as jr_RemoteFileName, jr.JobName as jr_JobName, jr.JobNumber as jr_JobNumber
  , jr.XferStartTime as jr_XferStartTime, jr.XferRecipient as jr_XferRecipient, jr.XferMode as jr_XferMode, jr.XferDaemon as jr_XferDaemon, jr.isAFP as jr_isAFP, jr.isFullAFP as jr_isFullAFP
  , jr.Status as jr_Status, jr.PendingOp as jr_PendingOp
  , js.JobSpoolId as js_JobSpoolId, js.InsertTime as js_InsertTime, js.JobReportId as js_JobReportId, js.SpoolPathId as js_SpoolPathId, js.InputFileName as js_InputFileName, js.LocalFileName as js_LocalFileName
  , js.ContentType as js_ContentType, js.RemoteHostName as js_RemoteHostName, js.RemoteHostAddr as js_RemoteHostAddr, js.RemoteQueueName as js_RemoteQueueName, js.JobName as js_JobName
  , js.JobNumber as js_JobNumber, js.XferDaemon as js_XferDaemon, js.XferStartTime as js_XferStartTime, js.XferRecipient as js_XferRecipient, js.XferMode as js_XferMode, js.RetryCount as js_RetryCount
  , jn.JobReportName as jn_JobReportName, jn.JobReportDescr as jn_JobReportDescr, jn.ReportFormat as jn_ReportFormat, jn.ElabFormat as jn_ElabFormat, jn.CharsPerLine as jn_CharsPerLine
  , jn.LinesPerPage as jn_LinesPerPage, jn.PageOrient as jn_PageOrient, jn.HasCc as jn_HasCc, jn.FontSize as jn_FontSize, jn.FitToPage as jn_FitToPage, jn.ParseFileName as jn_ParseFileName, jn.TargetLocalPathId_IN as jn_TargetLocalPathId_IN, jn.TargetLocalPathId_OUT as jn_TargetLocalPathId_OUT, jn.TypeOfWork as jn_TypeOfWork, jn.WorkClass as jn_WorkClass, jn.Priority as jn_Priority 
  from tbl_Workqueue wq
  LEFT JOIN tbl_JobReports as jr on wq.ExternalKey = jr.$IDENTITY and wq.ExternalTableName = 'tbl_JobReports'
  LEFT JOIN tbl_JobSpool as js on wq.ExternalKey = js.$IDENTITY and wq.ExternalTableName = 'tbl_JobSpool'
  LEFT JOIN tbl_JobReportNames jn on jr.JobReportName = jn.JobReportName
  WHERE (specs.TypeOfWork = wq.TypeOfWork AND ((specs.WorkClass IS NULL and wq.Workclass IS NULL) OR specs.workclass = wq.workclass) AND wq.srvname IS NULL AND specs.status = wq.status)
  OR    (specs.TypeOfWork = wq.TypeOfWork AND ((specs.WorkClass IS NULL and wq.Workclass IS NULL) OR specs.workclass = wq.workclass) AND wq.srvname = '$main::SrvName' AND specs.status <> wq.status)
) jwq
) DTBL
ORDER BY priority desc
) AS src
ON tgt.workid = src.workid AND tgt.externaltablename = src.externaltablename AND tgt.externalkey = src.externalkey AND tgt.typeofwork = src.typeofwork
WHEN MATCHED THEN UPDATE SET tgt.status = src.startstatus, tgt.srvname = '$main::SrvName'
OUTPUT src.defid, INSERTED.*, src.jr_JobReportName, src.jr_JobReportId, src.jr_DateTime, src.jr_LocalFileName, src.jr_LocalPathId_IN, src.jr_LocalPathId_OUT, src.jr_OrigJobReportName, src.jr_RemoteFileName, src.jr_JobName, src.jr_JobNumber, src.jr_XferStartTime, src.jr_XferRecipient, src.jr_XferMode, src.jr_XferDaemon, src.jr_isAFP, src.jr_isFullAFP, src.jr_Status, src.jr_PendingOp, src.js_JobSpoolId, src.js_InsertTime, src.js_JobReportId, src.js_SpoolPathId, src.js_InputFileName, src.js_LocalFileName, src.js_ContentType, src.js_RemoteHostName, src.js_RemoteHostAddr, src.js_RemoteQueueName, src.js_JobName, src.js_JobNumber, src.js_XferDaemon, src.js_XferStartTime, src.js_XferRecipient, src.js_XferMode, src.js_RetryCount, src.jn_JobReportName, src.jn_JobReportDescr, src.jn_ReportFormat, src.jn_ElabFormat, src.jn_CharsPerLine, src.jn_LinesPerPage, src.jn_PageOrient, src.jn_HasCc, src.jn_FontSize, src.jn_FitToPage, src.jn_ParseFileName, src.jn_TargetLocalPathId_IN, src.jn_TargetLocalPathId_OUT, src.jn_TypeOfWork, src.jn_WorkClass, src.jn_Priority
;

= qq{
WITH  specs (defid, TypeOfWork, Status, WorkClass, startstatus) AS
(SELECT * FROM ( $selvals ) selection(defid, jobtable, TypeOfWork, status, workclass, startstatus)
)
MERGE dbo.tbl_Workqueue WITH (HOLDLOCK ROWLOCK) AS tgt
USING (
SELECT TOP 1 specs.defid, specs.startstatus, 1 AS selpri, $col_list 
FROM tbl_WorkQueue  AS wq WITH (READPAST)
INNER JOIN specs ON (specs.TypeOfWork = wq.TypeOfWork AND ((specs.WorkClass IS NULL and wq.Workclass IS NULL) OR specs.workclass = wq.workclass) AND wq.srvname IS NULL AND specs.status = wq.status)
LEFT JOIN tbl_JobReports as jr on wq.ExternalKey = jr.\$IDENTITY 
LEFT JOIN tbl_JobSpool as js on wq.ExternalKey = js.\$IDENTITY 
LEFT JOIN tbl_JobReportNames jn on jr.JobReportName = jn.JobReportName 
ORDER BY wq.priority desc
union
SELECT TOP 1 specs.defid, specs.startstatus, 0 AS selpri, $col_list
FROM dbo.tbl_WorkQueue AS wq WITH (READPAST)
INNER JOIN specs ON (specs.TypeOfWork = wq.TypeOfWork AND ((specs.WorkClass IS NULL and wq.Workclass IS NULL) OR specs.workclass = wq.workclass) AND wq.srvname = '$main::SrvName' AND specs.status <> wq.status)
LEFT JOIN tbl_JobReports as jr on wq.ExternalKey = jr.\$IDENTITY 
LEFT JOIN tbl_JobSpool as js on wq.ExternalKey = js.\$IDENTITY 
LEFT JOIN tbl_JobReportNames jn on jr.JobReportName = jn.JobReportName 
ORDER BY wq.priority desc
) DTBL
ORDER BY selpri 
) AS src
ON tgt.workid = src.workid AND tgt.externaltablename = src.externaltablename AND tgt.externalkey = src.externalkey AND tgt.typeofwork = src.typeofwork
WHEN MATCHED THEN UPDATE SET tgt.status = src.startstatus, tgt.srvname = '$main::SrvName' 
OUTPUT src.defid, INSERTED.*, $out_cols
;
};

$sql 

== test configure

my $cfg = XMLin(q{
<cfg>
<GlobalWorkQueues>
<table name="tbl_WorkQueue" handler="my::QUtil" tbalias="wq" >
<columns>ExternalTableName ExternalKey TypeOfWork WorkId InsertTime 
  WorkClass Priority Status SrvName SrvParameters</columns>
</table>
<table name="tbl_JobReports" handler="XReport::JobREPORT" tbalias="jr" >
<columns>JobReportName JobReportId DateTime LocalFileName LocalPathId_IN LocalPathId_OUT OrigJobReportName RemoteFileName 
                 JobName JobNumber XferStartTime XferRecipient XferMode XferDaemon isAFP isFullAFP Status PendingOp</columns>
<subtable name="tbl_JobReportNames" tbalias="jn" joincondition="jr.JobReportName=jn.JobReportName" idcol="JobReportName" >
<columns>JobReportName JobReportDescr ReportFormat ElabFormat CharsPerLine LinesPerPage PageOrient HasCc FontSize FitToPage 
                 ParseFileName TargetLocalPathId_IN TargetLocalPathId_OUT TypeOfWork WorkClass Priority</columns>
</subtable>
</table>
<table name="tbl_JobSpool" handler="my::JobSpool" tbalias="js" >
<columns>JobSpoolId InsertTime JobReportId SpoolPathId InputFileName LocalFileName ContentType RemoteHostName RemoteHostAddr
                 RemoteQueueName JobName JobNumber XferDaemon XferStartTime XferRecipient XferMode RetryCount</columns>
</table>
</GlobalWorkQueues>
<jobs>
	<job type="11">
		<step order="1" cmd="bin\null_script.pl" />
		<step order="2" cmd="bin\null_script.pl" />
	</job>
	<job type="12">
		<step order="1" cmd="bin\W3SVCLogAnalyzer.pl" EventID="10020"/>
		<step order="2" cmd="bin\xrJROKWrapper.pl" EventID="10021"/>
	</job>
	<job type="13">
		<step order="1" cmd="bin\xrSOAPstore.pl" EventID="10010"/>
		<step order="2" cmd="bin\xrLoadIndexesTSV.pl" EventID="10011"/>
		<step order="3" cmd="bin\xrJROKWrapper.pl" EventID="10012"/>
	</job>
	<job type="24">
		<step order="1" cmd="bin\xrImportXreportArch.pl" />
		<step order="2" cmd="bin\xrLoadIndexesTSV.pl" />
		<step order="3" cmd="bin\xrJROKWrapper.pl" />
	</job>
	<job type="33">
		<step order="1" cmd="bin\xrExport.pl"   EventID="10030"/>
		<step order="2" cmd="bin\null_script.pl" EventID="10099"/>	
	</job>
	<job type="34">
		<step order="1" cmd="bin\xrLoadZipArchive.pl" EventID="10040"/>
		<step order="2" cmd="bin\xrLoadIndexesTSV.pl" EventID="10041"/>		
		<step order="3" cmd="bin\xrPostProcessor.pl" EventID="10042"/>
		<step order="4" cmd="bin\xrSetStatus.pl" EventID="10043"/>		
		<step order="5" cmd="bin\xrJROKWrapper.pl" EventID="10044"/>
	</job>
	<job type="44">
		<step order="1" cmd="bin\extractPDFxST.pl"   EventID="10050"/>
		<step order="2" cmd="bin\null_script.pl" EventID="10099"/>	
	</job>
	<job type="50">
			<step order="1" cmd="bin\xrLoadArchive.pl" EventID="10060"/>
			<step order="2" cmd="bin\xrLoadIndexesTSV.pl" EventID="10061"/>
			<step order="3" cmd="bin\xrPostProcessor.pl" EventID="10062"/>
			<step order="4" cmd="bin\xrJROKWrapper.pl" EventID="10063"/>
	</job>
	<job type="54">
		<step order="1" cmd="bin\xrLoadBlob.pl" EventID="10070"/>
		<step order="2" cmd="bin\xrLoadIndexesTSV.pl" EventID="10071"/>	
		<step order="3" cmd="bin\xrPostProcessor.pl" EventID="10072"/>
		<step order="4" cmd="bin\xrJROKWrapper.pl" EventID="10073"/>
	</job>
	<job type="64">
		<step order="1" cmd="bin\xrExtractPDF.pl"   EventID="10080"/>
		<step order="2" cmd="bin\null_script.pl" EventID="10099"/>	
	</job>	
	<job type="84">
		<step order="1" cmd="bin\extractHC.pl" EventID="10090"/>
		<step order="2" cmd="bin\null_script.pl" EventID="10099"/>		
	</job>
	<job type="90">
		<step order="1" cmd="bin\MODCA_Converter.pl" EventID="10090"/>	
		<step order="2" cmd="bin\null_script.pl" EventID="10099"/>
	</job>
	<job type="999">
		<step order="1" cmd="bin\null_script.pl" />
		<step order="2" cmd="bin\printfatal.pl" />
		<step order="3" cmd="bin\null_script.pl" cond="ONLY"/>
		<step order="4" cmd="bin\printerr.pl" />
	</job>
	<job type="9990" status="16">
		<step order="1" cmd="sbin\DocPARequest.pl" />
	</job>
	<job type="9991" status="0" DISP="LEAVE" To_TypeOfWork="9992">
		<step order="1" cmd="sbin\DocPAFirmaRequest.pl" />
	</job>
	<job type="9992" status="16">
		<step order="1" cmd="sbin\DocPAArchiveSigned.pl" />
		<step order="2" cmd="sbin\DocPAFinalWrapper.pl" />
	</job>
	<job type="9993" status="0" DISP="LEAVE" To_TypeOfWork="9994">
		<step order="1" cmd="sbin\DocPAFirmaRequest.pl" />
	</job>
	<job type="9994" status="16" DISP="LEAVE" To_TypeOfWork="9995">
                <statusCodes start="17" end="32" />
		<step order="1" cmd="sbin\dooutmem.pl" />
	</job>
	<job type="9995" status="32" DISP="LEAVE" To_TypeOfWork="9996">
                <statusCodes start="33" end="34" />
		<step order="1" cmd="sbin\donotfind.pl" />
		<step order="3" cmd="bin\null_script.pl" cond="ONLY"/>
	</job>
	<job type="9996" status="48">
                <statusCodes start="49" end="50" />
		<step order="1" cmd="sbin\DocPAFinalWrapper.pl" />
	</job>
</jobs>

</cfg>
}, ForceArray=>[qw(table subtable job step)]);

my $wqcols = [qw(ExternalTableName ExternalKey TypeOfWork WorkId InsertTime WorkClass Priority Status SrvName SrvParameters)];
my $jrcols = [qw(JobReportName JobReportId DateTime LocalFileName LocalPathId_IN LocalPathId_OUT OrigJobReportName RemoteFileName 
                 JobName JobNumber XferStartTime XferRecipient XferMode XferDaemon isAFP isFullAFP Status PendingOp)];
my $jscols = [qw(JobSpoolId InsertTime JobReportId SpoolPathId InputFileName LocalFileName ContentType RemoteHostName RemoteHostAddr
                 RemoteQueueName JobName JobNumber XferDaemon XferStartTime XferRecipient XferMode RetryCount)];
my $jncols = [qw(JobReportName JobReportDescr ReportFormat ElabFormat CharsPerLine LinesPerPage PageOrient HasCc FontSize FitToPage 
                 ParseFileName TargetLocalPathId_IN TargetLocalPathId_OUT TypeOfWork WorkClass Priority)];                 
my $col_list = join(', ',
   (map { "wq.$_"} @{$wqcols}), (map { "jr.$_ as jr_$_"} @{$jrcols}), (map { "js.$_ as js_$_"} @{$jscols}), (map { "jn.$_ as jn_$_"} @{$jncols})  
);
my $out_cols = join(', ', 
   (map { "src.jr_$_"} @{$jrcols}), (map { "src.js_$_"} @{$jscols}), (map { "src.jn_$_"} @{$jncols})  
);
=cut

