/*
 ============================================================================
 Name        : xrDaemon.c
 Author      : mpezzi
 Version     :
 Copyright   : Copyright PZWARE s.r.l 2019
 Description :
 ============================================================================
 */

#ifdef __BORLANDC__
  typedef wchar_t wctype_t; /* in tchar.h, but unavailable unless _UNICODE */
#endif

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <process.h>
#include <tchar.h>
#include <string.h>
#include <stddef.h>
#include <libgen.h>
#include <math.h>

#include "service.h"
#include "plmodmap.h"

#if defined(__cplusplus)  && !defined(PERL_OBJECT)
extern "C" {
#endif

  #include "EXTERN.h"
  #include "perl.h"
  #include "Xsub.h"

#if defined(__cplusplus)  && !defined(PERL_OBJECT)
}
#endif

// #include <dfcln.h>

// internal variables
SERVICE_STATUS          ssStatus;       // current status of the service
SERVICE_STATUS_HANDLE   sshStatusHandle;
DWORD                   dwErr = 0;
BOOL                    bDebug = FALSE;
TCHAR                   szErr[256];

// internal function prototypes
VOID WINAPI service_ctrl(DWORD dwCtrlCode);
VOID WINAPI service_main(DWORD dwArgc, LPTSTR *lpszArgv);
VOID CmdDebugService(int argc, char **argv);
BOOL WINAPI ControlHandler ( DWORD dwCtrlType );
LPTSTR GetLastErrorText( LPTSTR lpszBuf, DWORD dwSize );

// EXT_BIN_SYM(perlbin_gz_xrLpdDump_pl);
// EXT_BIN_SYM(src_pluto_pl);
// EXT_BIN_SYM(src_paper_pl);
EXTERN_C void xs_init();

/*
static char plcode[] =
"print 'This is coming from perl', \"\n\""
;

XS(TStringsTIEARRAY) {
    dXSARGS;

    SV *ssv=ST(0); // "G::TStrings"
    void* tmp[4];
    memset(tmp,0,sizeof(tmp));
    tmp[2] = 0;
    ((TStrings*)(tmp[0])) = TyingStrings;
    ((TComponent*)(tmp[1])) = TyingComp;
    ((int)(tmp[2])) = TyingOpt;
    SV *tsv;
    tsv = newSVpv((char*)&tmp[0],16);

    STRLEN len;
    char *ptr = SvPV(ssv,len);
    HV *stash = gv_stashpvn(ptr,len, TRUE);
    SV *rsv = newRV_inc(tsv);
    sv_bless(rsv,stash);

    ST(0) = rsv;

    XSRETURN(1);
}


*/
XS(getCodeRef) {
	  dXSARGS;
	  SV *ssv0=ST(0); // "G::TStrings"
	  char *refe = getPerlModAddress(SvPV(ssv0, PL_na));
	  if ( !refe ) { XSRETURN_NO; }
	  SV* pSV = newSVpv( refe, strlen(refe) );
	  ST( 0 ) = sv_2mortal( pSV );

	  XSRETURN(1);
}

XS(DumpHash2)
{
  dXSARGS;
  SV *ssv0=ST(0); // "G::TStrings"
//  SV *ssv1=ST(1); // "G::TStrings"
  SV *obj_sv=NULL;
  HV *pHV=NULL;

  HE *pHE = NULL;
  I32 lval = 0;
  I32 lElements;

  SV **psValn1_ptr;
  SV *psValn1;

  if (!SvROK(ssv0))
      croak("Not a reference");

  obj_sv = SvRV(ssv0);
  if (SvTYPE(obj_sv) != SVt_PVHV)
	  obj_sv = ssv0;
   if (SvTYPE(obj_sv) != SVt_PVHV)
      croak("Not a reference to a hash");

  pHV = MUTABLE_HV(obj_sv);

  psValn1_ptr = hv_fetchs(pHV, "N1", &lval );
  psValn1 = *psValn1_ptr;
  printf( "Element - N1: %s: \n", SvPV(psValn1, PL_na) );

  lElements = hv_iterinit( pHV );
  printf( "Dumping %d elements\n", lElements );

  printf( "\tKey\tValue\n\t--\t-----\n" );
  while( NULL != ( pHE = hv_iternext( pHV ) ) )
  {
    //long lKeyLength;
    // long lValLen = 10;
    SV *pszKey = hv_iterkeysv( pHE );
    // printf( "Dumping key %s:\n", SvPV(pszKey, PL_na ));
    SV *pSVValue = hv_iterval( pHV, pHE );
    printf( "\t'%s'\t'%s'\n", SvPV(pszKey, PL_na ), SvPV( pSVValue, PL_na ) );
  }

  printf( "Completed\n" );
//  puTARblock member = point("bin/main.pl");
//  if ( member == (puTARblock)NULL ) { XSRETURN_NO; }
//  SV* pSV = newSVpv( member->hdr.name, member->hdr.usize );
//  ST( 0 ) = sv_2mortal( pSV );

  XSRETURN_YES;
}
/*
char *getSymbolPtr(char *symname) {
	    void *hdl;
	    const char *ptr;
	    int i;

	    hdl = dlopen(NULL, 0);
	    ptr = dlsym(hdl, symname);
	    printf("%s = %p\n", symname, ptr);
	    return ptr;
}
*/


/*
void DumpHash2(char *pHV[] )
{
  HE *pHE = NULL;
  long lElements = hv_iterinit( (HV *)&pHV->[0] );
  printf( "Dumping %d elements:\n", lElements );
  printf( "\tKey\tValue\n\t--\t-----\n" );

  while( NULL != ( pHE = hv_iternext( pHV ) ) )
  {
    long lKeyLength;
    char *pszKey = hv_iterkey( pHE, &lKeyLength );
    SV *pSVValue = hv_iterval( pHV, pHE );
    printf( "\t'%s'\t'%s'\n", pszKey, SvPV_nolen( pSVValue ) );m
  }
}
*/
#undef printf

static char *maincode =
		"BEGIN {"
#ifdef DEBUG
		  "print \"This is coming from -e BEGIN code - ^X: $^X P0:\". $0 . 'ARGV: ' . join('::', @ARGV) .\"\\n\";"
#endif
		  "unshift @INC, sub {"
		       "my $modnm = $_[1] =~ /^maincode\.pm$/ ? \"bin/$^X.pl\" : 'perllib/'.$_[1];"
	           "print \"INC PARMS: \", join('::', @_), \" seeking $modnm\\n\";"
		       "my $code = PZ::getCodeRef($modnm) || return;"
		       "open my $fh, '<', \\$code or return;"
		       "$fh->seek(0, 0) or die \"Failed to seek $modnm: $! - $^E\";"
		       "return $fh;"
		  "};"
		"};"
		  "$| = 1;"
		"require maincode;"
//		  "my $maincode = PZ::getCodeRef('bin/main.pl') || die \"no maincode found\";"
//		  "$maincode =~ s/\\n__END__.*\\Z//mso;"
//		  "my $pkgcode = \"package perlcode; sub handler { \";"
#ifdef DEBUG
//		  "$pkgcode .= \"printf(\\\"entering %s from %s invoker: %s ARGV: %s\\\\n\\\", __PACKAGE__, join('::', caller()), \\$0, join('::', \\@ARGV));\";"
#endif
//		  "$pkgcode .= $maincode;"
//		  "$pkgcode .= '}';"
//		  "print \"maincode:\\n\", $pkgcode, \"\\n\";"
//		  "eval $pkgcode;"
//		  "die $@ if $@;"
//		  "my $perlcode = perlcode->can('handler') || die \"no handler built found\";"
;
/*
static char *mcode = R"(
BEGIN {
  print "This is coming from -e BEGIN code\n";
  unshift @INC, sub { 
       print "INC PARMS: ", join('::', @_), "\n";
       my $modnm = $_[1];
       my $code = PZ::getCodeRef('perllib/'.$modnm) || return;
       open my $fh, '<', \$code or return;
       $fh->seek(0, 0) or die "Failed to seek $modnm: $! - $^E";
       return $fh;
  };
  my $maincode = PZ::getCodeRef('perllib/main.pl') || die "no maincode found";
  eval qq{package perlcode; sub handler { $maincode; }};
  die $@ if $@;

}
)";
*/

static PerlInterpreter *my_perl;  /***    The Perl interpreter    ***/
int CallPerl(int argc, char **argv, char **env)
{
//	printf("code len: %I64d\n", (intptr_t)&_binary_perlbin_gz_xrLpdDump_pl_size);
//	char *symptr = getSymbolPtr('_binary_perlbin_gz_xrLpdDump_pl_size');
	   char *exename = strdup(argv[0]);
	   char *myself = strtok(basename(exename), ".");
	   char *embedding[20] = { myself, "-MIO::String -MFcntl", "-e", maincode, "--"}; // , "-N", "XXXX" };
	   for ( register int iArgNum = 1; iArgNum <= argc; iArgNum++ ) {
		   embedding[iArgNum + 4 ] = argv[iArgNum]; // strdup(argv[iArgNum]);
	   }
	   embedding[argc + 5] = (char *)NULL;
//	   char *embedding[] = { "", "", "-e", maincode };
//	   char *args[] = { argv , FALSE, NULL };
//	   char *args[] = { "-N" , "XXXXXXX", NULL };
       int exitstatus = 0;

	   PERL_SYS_INIT3(&argc,&argv,&env);
	   if((my_perl = perl_alloc()) == NULL) {
	        fprintf(stderr, "no memory!");
	        exit(1);
	   }
       perl_construct(my_perl);
	   PL_exit_flags |= PERL_EXIT_DESTRUCT_END;
       exitstatus = perl_parse(my_perl, xs_init, argc + 4, embedding, (char **)NULL);
       if (!exitstatus ) {
          newXS("E::Utils::DumpHash2", DumpHash2, "BCB+Perl internal");
          newXS("PZ::getCodeRef", getCodeRef, "MPZ+Perl internal");
          perl_run(my_perl);
//          eval_pv("$main::logrRtn = sub { print @_, \"\\n\"; }", TRUE);
//          perl_call_pv("perlcode::handler", G_EVAL|G_DISCARD|G_NOARGS);

                   /* check $@ */
//          if(SvTRUE(GvSV(PL_errgv))) fprintf(stderr, "eval error: %s\n", SvPV(GvSV(PL_errgv), PL_na));
//         eval_pv(getPerlModAddress("bin/main.pl"), TRUE);
       }
       perl_destruct(my_perl);
       perl_free(my_perl);
	   PERL_SYS_TERM();

	   return exitstatus;
}

int  main(int argc, char **argv, char **env)
{
	int perl_rc = 0;;
	initDirectory();
	perl_rc = CallPerl(argc, argv, env);
	exit(perl_rc);
    SERVICE_TABLE_ENTRY dispatchTable[] =
    {
        { TEXT(SZSERVICENAME), (LPSERVICE_MAIN_FUNCTION)service_main },
        { NULL, NULL }
    };

    if ( (argc > 1) &&
         ((*argv[1] == '-') || (*argv[1] == '/')) )
    {
        if ( _stricmp( "debug", argv[1]+1 ) == 0 )
        {
            bDebug = TRUE;
            CmdDebugService(argc, argv);
        }
        else
        {
            goto dispatch;
        }
        exit(0);
    }

    // if it doesn't match any of the above parameters
    // the service control manager may be starting the service
    // so we must call StartServiceCtrlDispatcher
    dispatch:
        // this is just to be friendly
        printf( "%s -debug <params>   to run as a console app for debugging\n", SZAPPNAME );
        printf( "\nStartServiceCtrlDispatcher being called.\n" );
        printf( "This may take several seconds.  Please wait.\n" );

        if (!StartServiceCtrlDispatcher(dispatchTable))
            AddToMessageLog(TEXT("StartServiceCtrlDispatcher failed."));

        return 1;
}



//
//  FUNCTION: service_main
//
//  PURPOSE: To perform actual initialization of the service
//
//  PARAMETERS:
//    dwArgc   - number of command line arguments
//    lpszArgv - array of command line arguments
//
//  RETURN VALUE:
//    none
//
//  COMMENTS:
//    This routine performs the service initialization and then calls
//    the user defined ServiceStart() routine to perform majority
//    of the work.
//

VOID ServiceStart(DWORD dwArgc, LPTSTR *lpszArgv) {

}

VOID ServiceStop() {

}


void WINAPI service_main(DWORD dwArgc, LPTSTR *lpszArgv)
{

    // register our service control handler:
    //
    sshStatusHandle = RegisterServiceCtrlHandler( TEXT(SZSERVICENAME), service_ctrl);

    if (!sshStatusHandle)
        goto cleanup;

    // SERVICE_STATUS members that don't change in example
    //
    ssStatus.dwServiceType = SERVICE_WIN32_OWN_PROCESS;
    ssStatus.dwServiceSpecificExitCode = 0;


    // report the status to the service control manager.
    //
    if (!ReportStatusToSCMgr(
        SERVICE_START_PENDING, // service state
        NO_ERROR,              // exit code
        3000))                 // wait hint
        goto cleanup;


    ServiceStart( dwArgc, lpszArgv );

cleanup:

    // try to report the stopped status to the service control manager.
    //
    if (sshStatusHandle)
        (VOID)ReportStatusToSCMgr(
                            SERVICE_STOPPED,
                            dwErr,
                            0);

    return;
}



//
//  FUNCTION: service_ctrl
//
//  PURPOSE: This function is called by the SCM whenever
//           ControlService() is called on this service.
//
//  PARAMETERS:
//    dwCtrlCode - type of control requested
//
//  RETURN VALUE:
//    none
//
//  COMMENTS:
//
VOID WINAPI service_ctrl(DWORD dwCtrlCode)
{
    // Handle the requested control code.
    //
    switch(dwCtrlCode)
    {
        // Stop the service.
        //
        // SERVICE_STOP_PENDING should be reported before
        // setting the Stop Event - hServerStopEvent - in
        // ServiceStop().  This avoids a race condition
        // which may result in a 1053 - The Service did not respond...
        // error.
        case SERVICE_CONTROL_STOP:
            ReportStatusToSCMgr(SERVICE_STOP_PENDING, NO_ERROR, 0);

            ServiceStop();
            return;

        // Update the service status.
        //
        case SERVICE_CONTROL_INTERROGATE:
            break;

        // invalid control code
        //
        default:
            break;

    }

    ReportStatusToSCMgr(ssStatus.dwCurrentState, NO_ERROR, 0);
}



//
//  FUNCTION: ReportStatusToSCMgr()
//
//  PURPOSE: Sets the current status of the service and
//           reports it to the Service Control Manager
//
//  PARAMETERS:
//    dwCurrentState - the state of the service
//    dwWin32ExitCode - error code to report
//    dwWaitHint - worst case estimate to next checkpoint
//
//  RETURN VALUE:
//    TRUE  - success
//    FALSE - failure
//
//  COMMENTS:
//
BOOL ReportStatusToSCMgr(DWORD dwCurrentState,
                         DWORD dwWin32ExitCode,
                         DWORD dwWaitHint)
{
    static DWORD dwCheckPoint = 1;
    BOOL fResult = TRUE;


    if ( !bDebug ) // when debugging we don't report to the SCM
    {
        if (dwCurrentState == SERVICE_START_PENDING)
            ssStatus.dwControlsAccepted = 0;
        else
            ssStatus.dwControlsAccepted = SERVICE_ACCEPT_STOP;

        ssStatus.dwCurrentState = dwCurrentState;
        ssStatus.dwWin32ExitCode = dwWin32ExitCode;
        ssStatus.dwWaitHint = dwWaitHint;

        if ( ( dwCurrentState == SERVICE_RUNNING ) ||
             ( dwCurrentState == SERVICE_STOPPED ) )
            ssStatus.dwCheckPoint = 0;
        else
            ssStatus.dwCheckPoint = dwCheckPoint++;


        // Report the status of the service to the service control manager.
        //
        if (!(fResult = SetServiceStatus( sshStatusHandle, &ssStatus))) {
            AddToMessageLog(TEXT("SetServiceStatus"));
        }
    }
    return fResult;
}



//
//  FUNCTION: AddToMessageLog(LPTSTR lpszMsg)
//
//  PURPOSE: Allows any thread to log an error message
//
//  PARAMETERS:
//    lpszMsg - text for message
//
//  RETURN VALUE:
//    none
//
//  COMMENTS:
//
VOID AddToMessageLog(LPTSTR lpszMsg)
{
    TCHAR   szMsg[256];
    HANDLE  hEventSource;
    LPTSTR  lpszStrings[2];


    if ( !bDebug )
    {
        dwErr = GetLastError();

        // Use event logging to log the error.
        //
        hEventSource = RegisterEventSource(NULL, TEXT(SZSERVICENAME));

        _stprintf(szMsg, TEXT("%s error: %d"), TEXT(SZSERVICENAME), (int) dwErr);
        lpszStrings[0] = szMsg;
        lpszStrings[1] = lpszMsg;

        if (hEventSource != NULL) {
            ReportEvent(hEventSource, // handle of event source
                EVENTLOG_ERROR_TYPE,  // event type
                0,                    // event category
                0,                    // event ID
                NULL,                 // current user's SID
                2,                    // strings in lpszStrings
                0,                    // no bytes of raw data
                (const CHAR **)lpszStrings,          // array of error strings
                NULL);                // no raw data

            (VOID) DeregisterEventSource(hEventSource);
        }
    }
}




///////////////////////////////////////////////////////////////////
//
//  The following code is for running the service as a console app
//


//
//  FUNCTION: CmdDebugService(int argc, char ** argv)
//
//  PURPOSE: Runs the service as a console application
//
//  PARAMETERS:
//    argc - number of command line arguments
//    argv - array of command line arguments
//
//  RETURN VALUE:
//    none
//
//  COMMENTS:
//
void CmdDebugService(int argc, char ** argv)
{
    DWORD dwArgc;
    LPTSTR *lpszArgv;

#ifdef UNICODE
    lpszArgv = CommandLineToArgvW(GetCommandLineW(), &(dwArgc) );
#else
    dwArgc   = (DWORD) argc;
    lpszArgv = argv;
#endif

    _tprintf(TEXT("Debugging %s.\n"), TEXT(SZSERVICEDISPLAYNAME));

    SetConsoleCtrlHandler( ControlHandler, TRUE );

    ServiceStart( dwArgc, lpszArgv );
}


//
//  FUNCTION: ControlHandler ( DWORD dwCtrlType )
//
//  PURPOSE: Handled console control events
//
//  PARAMETERS:
//    dwCtrlType - type of control event
//
//  RETURN VALUE:
//    True - handled
//    False - unhandled
//
//  COMMENTS:
//
BOOL WINAPI ControlHandler ( DWORD dwCtrlType )
{
    switch( dwCtrlType )
    {
        case CTRL_BREAK_EVENT:  // use Ctrl+C or Ctrl+Break to simulate
        case CTRL_C_EVENT:      // SERVICE_CONTROL_STOP in debug mode
            _tprintf(TEXT("Stopping %s.\n"), TEXT(SZSERVICEDISPLAYNAME));
            ServiceStop();
            return TRUE;
            break;

    }
    return FALSE;
}

//
//  FUNCTION: GetLastErrorText
//
//  PURPOSE: copies error message text to string
//
//  PARAMETERS:
//    lpszBuf - destination buffer
//    dwSize - size of buffer
//
//  RETURN VALUE:
//    destination buffer
//
//  COMMENTS:
//
LPTSTR GetLastErrorText( LPTSTR lpszBuf, DWORD dwSize )
{
    DWORD dwRet;
    LPTSTR lpszTemp = NULL;

    dwRet = FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM |FORMAT_MESSAGE_ARGUMENT_ARRAY,
                           NULL,
                           GetLastError(),
                           LANG_NEUTRAL,
                           (LPTSTR)&lpszTemp,
                           0,
                           NULL );

    // supplied buffer is not long enough
    if ( !dwRet || ( (long)dwSize < (long)dwRet+14 ) )
        lpszBuf[0] = TEXT('\0');
    else
    {
        lpszTemp[lstrlen(lpszTemp)-2] = TEXT('\0');  //remove cr and newline character
        _stprintf( lpszBuf, TEXT("%s (0x%x)"), lpszTemp, (unsigned int)GetLastError() );
    }

    if ( lpszTemp )
        LocalFree((HLOCAL) lpszTemp );

    return lpszBuf;
}
