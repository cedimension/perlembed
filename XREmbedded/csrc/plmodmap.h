/*
 * perlmodmap.h
 *
 *  Created on: 27 lug 2019
 *      Author: mpezzi
 */

#ifndef PLMODMAP_H_
#define PLMODMAP_H_

#include "ustar.h"

#define PLMOD_ADDR(plname) &_binary_ ## plname ## _start[0]
#define PLMOD_END(plname) &_binary_ ## plname ## _end[0]
#define PLMOD_SIZE(plname) (intptr_t)&_binary_ ## plname ## _size

// #define REF_ENTRY(plname) #plname, PLMOD_ADDR(plname), PLMOD_SIZE(plname)
#define EXT_BIN_SYM(plname) extern char _binary_ ## plname ## _start[]; extern char _binary_ ## plname ## _end[]; extern char _binary_ ## plname ## _size

typedef struct _dir_entry {
	uint32_t UlMEMttr;
} dir_entry, *p_dir_entry;

typedef struct _perllib {
	uint16_t UdirSize;
	dir_entry directory[1024];
	uTARblock TARfile[0];
} perllib, *p_perllib;

p_perllib initDirectory();
puTARblock dirEntry();
void *point();
void *getPerlModAddress(char *modid);

#endif /* PLMODMAP_H_ */
