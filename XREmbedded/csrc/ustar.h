/*
 * ustar.h
 *
 *  Created on: 04 ago 2019
 *      Author: mpezzi
 */

#ifndef USTAR_H_
#define USTAR_H_

/*
 * Standard Archive Format - Standard TAR - USTAR
 */
#define  RECORDSIZE  512
#define  NAMSIZ      100
#define  TNUMLEN      32
#define  TGNMLEN      32

#define    CHKBLANKS    "        "        /* 8 blanks, no null */

/* The magic field is filled with this if uname and gname are valid. */
#define    USTARMAGIC    "ustar"        /* 7 chars and a null */
#define USTARSIZE sizeof(USTARMAGIC)-1

typedef enum __attribute__((packed)) _eTAR_filetype {
	  eTARft_regular_null = '\x00'
	, eTARft_regular_zero = '0'
	, eTARft_internal_link = '1'
	, eTARft_symbolic_link = '2'
	, eTARft_char_dev = '3'
	, eTARft_block_dev = '4'
	, eTARft_directory = '5'
	, eTARft_FIFO_file = '6'
	, eTARft_reserved = '7'
} eTAR_filetype;


typedef struct _USTAR_h {
	char name[NAMSIZ];
	char mode[8];
	char uid[8];
	char gid[8];
	char size[12];
	char mtime[12];
	char chksum[8];
	eTAR_filetype typeflag;
	char linkname[NAMSIZ];
	char magic[8];
	char version[2];
	char uname[TNUMLEN];
	char gname[TGNMLEN];
	char devmajor[8];
	char devminor[8];
	char prefix[155];
	/* ustar format ends here: follows user defined fields */
	char padding[2]; /* alignment to multiple of four address */
	uint32_t usize;
} USTAR_h, *pUSTAR_h;

typedef union _uTARblock {
	char data[RECORDSIZE];
    USTAR_h hdr;
} uTARblock, *puTARblock;

#define TARHDR(addr) memcmp(((puTARblock)addr)->hdr.magic, USTARMAGIC, USTARSIZE)

#endif /* USTAR_H_ */
