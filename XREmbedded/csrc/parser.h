/*
 * parser.h
 *
 *  Created on: 23 giu 2018
 *      Author: mpezzi
 */

#ifndef PARSER_H_
#define PARSER_H_

#include <stdint.h>
#include <sys/stat.h>
#include <unistd.h>

typedef unsigned char UCHAR;    /* uch */
typedef unsigned short USHORT;  /* us  */
typedef unsigned long ULONG;    /* ul  */

/* Combine l & h to form a 16 bit quantity. */
#define MAKEUSHORT(l, h) (((USHORT)(l)) | ((USHORT)(h)) << 8)
#define MAKESHORT(l, h)  ((SHORT)MAKEUSHORT(l, h))

#define B_SIZE 128*1024

enum whence {
	FROM_BEGIN,
	FROM_CURRENT_POS,
	FROM_END
};

typedef struct _dcb {
	FILE *phf;
	struct stat fstat;
	void *(*read)();
	uint32_t datalen;    /* data bytes in buffer */
	uint64_t buffpos;    /* buffer begin position in file */
	uint64_t filepos;    /* current file position */
	uint32_t nextpos;    /* current pos in buffer */
	char *databuff;
	char filename[255];
} dcb_t, *pdcb;

typedef struct globals {
	dcb_t *input;
} globals_t, *pglobals;
globals_t GLBLS;

// #define INDCB(f) (struct globals *(GLBLS->input))->f
// #define INDCB(f)({(register dcb_t *x) = GLBLS->input; return ((dcb_t *)x)->f})
#define pINDCB ((&GLBLS)->input)
#define READINPUT(p) (*(p->read))()

void *readStream();
void *openInput(char *);
void *getData(int32_t);
void *movePointer(uint64_t, enum whence);
_Bool inputEOF();

/*
 * every _memmap entry contains  a pointer to a 256KB area and a long integer indicating the amount of bytes usable in the area.
 */
struct _memmap {
	void *ptr;
	long freesize;
};
typedef struct _memmap memmap_t;
typedef struct _memmap *pmemmap;
//typedef struct _memmap *_state[], _currentenv[];

#endif /* PARSER_H_ */
