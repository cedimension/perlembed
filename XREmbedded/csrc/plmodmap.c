/*
 * plmodmap.c
 *
 *  Created on: 27 lug 2019
 *      Author: mpezzi
 */

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <process.h>
#include <tchar.h>
#include <string.h>
#include <math.h>

#include "ustar.h"
#include "plmodmap.h"
#include "zlib.h"

EXT_BIN_SYM(perl_lib);
// EXT_BIN_SYM(src_perlcode_pl);

static p_perllib TARarea;

#define ASCII_TO_NUMBER(num) ((num)-48) //Converts an ascii digit to the corresponding number (assuming it is an ASCII digit)
/**
 * Decode a TAR octal number.
 * Ignores everything after the first NUL or space character.
 * @param data A pointer to a size-byte-long octal-encoded
 * @param size The size of the field pointer to by the data pointer
 * @return
 */
static uint64_t decodeTarOctal(const char *data, size_t size) {
    unsigned char* currentPtr = (unsigned char*) data + size;
    uint64_t sum = 0;
    uint64_t currentMultiplier = 1;
    //Skip everything after the last NUL/space character
    //In some TAR archives the size field has non-trailing NULs/spaces, so this is neccessary
    unsigned char* checkPtr = currentPtr; //This is used to check where the last NUL/space char is
    for (; checkPtr >= (unsigned char*) data; checkPtr--) {
        if ((*checkPtr) == 0 || (*checkPtr) == ' ') {
            currentPtr = checkPtr - 1;
        }
    }
    for (; currentPtr >= (unsigned char*) data; currentPtr--) {
        sum += ASCII_TO_NUMBER(*currentPtr) * currentMultiplier;
        currentMultiplier *= 8;
    }
    return sum;
}

/* Parse an octal number, ignoring leading and trailing nonsense. */
/*
static int parseoct(const char *p, size_t n)
{
	int i = 0;

	while ((*p < '0' || *p > '7') && n > 0) {
		++p;
		--n;
	}
	while (*p >= '0' && *p <= '7' && n > 0) {
		i *= 8;
		i += *p - '0';
		++p;
		--n;
	}
	return (i);
}
*/

/* Verify the tar checksum. */
/*
static int verify_checksum(const char *p)
{
	int n, u = 0;
	for (n = 0; n < 512; ++n) {
		if (n < 148 || n > 155)
			u += ((unsigned char *)p)[n]; // Standard tar checksum adds unsigned bytes.
		else
			u += 0x20;
	}
	return (u == parseoct(p + 148, 8));
}
*/

int memGunzip(const Bytef *src, int srcLen, const Bytef *dst, int dstLen){
    z_stream strm;
    strm.zalloc=NULL;
    strm.zfree=NULL;
    strm.opaque=NULL;

    strm.avail_in = srcLen;
    strm.avail_out = dstLen;
    strm.next_in = (Bytef *)src;
    strm.next_out = (Bytef *)dst;

    int retBytes;

    int err = inflateInit2(&strm, MAX_WBITS+16);
    retBytes = -1;
    if (err == Z_OK){
        err = inflate(&strm, Z_FINISH);
        if (err == Z_STREAM_END){
            retBytes = strm.total_out;
        }
        else{
            inflateEnd(&strm);
            return err;
        }
    }
    else{
        inflateEnd(&strm);
        return err;
    }
    if ( retBytes != -1 ) {}
    inflateEnd(&strm);
    return err;
}

#define TARFN(entry) 	(TARarea->TARfile + ((p_dir_entry)entry)->UlMEMttr)->hdr.name
#define PERLMOD(entry) ((puTARblock)(&TARarea->TARfile + entry->UlMEMttr) + 1)

int sort_cmp (const void *file1, const void *file2 ) {
	int sz1 = strlen(TARFN(file1));
	int sz2 = strlen(TARFN(file2));
	if (sz1 < sz2 ) { return -1; }
	if (sz1 > sz2 ) { return 1; }
	return strcmp(TARFN(file1), TARFN(file2));
}

int seek_cmp (const void *modid, const void *file2 ) {
	char *modname = (char *)TARFN(file2);
	int sz1 = strlen((char *)modid);
	int sz2 = strlen(modname);
	if (sz1 < sz2 ) { return -1; }
	if (sz1 > sz2 ) { return 1; }
	int result = memcmp((char *)modid, modname, sz2);
	return result;
}

puTARblock dirEntry( char *memname ) {
    p_dir_entry direntry = bsearch(memname, TARarea->directory, TARarea->UdirSize, sizeof(dir_entry), seek_cmp);
    if ( direntry == (p_dir_entry)NULL ) { return (puTARblock)NULL; }
	return (TARarea->TARfile + direntry->UlMEMttr);
}

void *point( puTARblock TARHDR ) {
	if ( TARHDR->hdr.usize == 0 ) { return NULL; }
	char *pladdr = (++TARHDR)->data;
// #ifdef DEBUG
//    printf("POINT: content for %s:\n%s\n-----------------------", TARHDR->hdr.name, pladdr);
// #endif
	return pladdr;
}

void *getPerlModAddress ( char *pkey ) {
	puTARblock direntry = dirEntry(pkey);
    if ( direntry == (puTARblock)NULL ) { return (void *)NULL; }
#ifdef DEBUG
	printf("getPerlModAddress: found entry for %s - %p\n", direntry->hdr.name, direntry);
#endif
    return point( direntry);
}


p_perllib initDirectory() {
	Bytef *ZTARfile = (Bytef *)PLMOD_ADDR(perl_lib);
	uint32_t dataLen = PLMOD_SIZE(perl_lib) + 0;
	uint32_t osize = *((uint32_t *)((char *)PLMOD_END(perl_lib) - 4));
	TARarea = malloc(sizeof(perllib) + osize);

	int zrc = memGunzip(ZTARfile, dataLen, ((Bytef *)(TARarea->TARfile)), osize);
	if (zrc != Z_OK && zrc != Z_STREAM_END) exit(-1024);

	puTARblock endAddr = (puTARblock)((char *)&TARarea->TARfile + osize);

	TARarea->UdirSize = 0;
	p_dir_entry p_dir = &TARarea->directory[0];
	for (puTARblock TARblock = TARarea->TARfile ; TARblock < endAddr; TARblock++ ) {
		//printf("checking now address %p\n", TARblock);
		if ( !TARHDR(TARblock) && (TARblock->hdr.typeflag == eTARft_regular_null || TARblock->hdr.typeflag == eTARft_regular_zero) ) {;
			p_dir->UlMEMttr = (uint32_t)((((unsigned char *)TARblock) - ((unsigned char *)&TARarea->TARfile)) / RECORDSIZE);
			TARblock->hdr.usize = decodeTarOctal(TARblock->hdr.size, sizeof(TARblock->hdr.size));
			//p_dir->Ulmodsize = decodeTarOctal(TARblock->hdr.size, sizeof(TARblock->hdr.size));
#ifdef DEBUG
			printf("START: %p entry: % 4d fsize: % 6d address: %p DATATTR: % 8d filename: %s content: %20.20s\n"
					     , &TARarea->TARfile, TARarea->UdirSize, TARblock->hdr.usize, TARblock, p_dir->UlMEMttr
						 , TARFN(p_dir), (unsigned char *)&(TARblock + 1)->data[0]);
#endif
			uint32_t DataBlocks = (TARblock->hdr.usize / RECORDSIZE);
			TARarea->UdirSize++;
			p_dir++;
			TARblock += DataBlocks;
		}
	}
    qsort( TARarea->directory, TARarea->UdirSize, sizeof(dir_entry), sort_cmp );
#ifdef DEBUG
    for (register int i = 0; i < TARarea->UdirSize; ++i)
            printf("%d %s\n", TARarea->directory[i].UlMEMttr, TARFN(&TARarea->directory[i]));
	return TARarea;
#endif
}
