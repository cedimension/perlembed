/*
 * CTDACTF.h
 *
 *  Created on: 25 lug 2018
 *      Author: mpezzi
 */

#ifndef CTDACTF_H_
#define CTDACTF_H_

#include <xsIOLib.h>

#pragma pack(push,1)
/*
typedef struct __attribute__((packed)) _HWORD {
    unsigned char hibyte[1];
    unsigned char lobyte[1];
} HWORD, *pHWORD;

typedef struct __attribute__((packed)) _TSTAMP {
    unsigned char bit31;
    unsigned char bit28;
    unsigned char bit24;
    unsigned char bit20;
    unsigned char bit16;
    unsigned char bit12;
    unsigned char bit08;
    unsigned char bit04;
} TSTAMP, *pTSTAMP;

#define getShort(hword) ((hword.hibyte[0] * 256)+hword.lobyte[0])

*/

typedef struct __attribute__((packed)) _RSFLAG {
    unsigned char bit4_8:5;
    unsigned char Done:1;
    unsigned char InProgress:1;
    unsigned char Scheduled:1;
} RSFLAG, *pRSFLAGe;

typedef struct __attribute__((packed)) _FLGByte {
    unsigned char LastRecord:1;
    unsigned char RBAHasPagenum:1;
    unsigned char ParmEncodingSpecified:1;
    unsigned char UnloadedForExport:1;
    unsigned char bit4:1;
    unsigned char RetentionPreserved:1;
    unsigned char bit1_2:2;
} FLGByte, *pFLGByte;

typedef enum __attribute__((packed)) _eACTFType {
      eACTFRec_Unknown         = '\x00'
    , eACTFRec_ALTIndexKey     = '\xC1'        /*    ALT INDEX KEY RECORD           */
    , eACTFRec_CenteraClipID   = '\xC2'        /*    CENTERA CLIP ID RECORD         */
    , eACTFRec_Continuation    = '\xC3'        /*    CONTINUATION TYPE OF RECORD    */
    , eACTFRec_CTDSDest        = '\xC4'        /*    CTDS DESTINATION RECORD        */
    , eACTFRec_FEPRint         = '\xC6'        /*    'FE'PRINT RECORD TYPE          */
    , eACTFRec_MigrPartition   = '\xC7'        /*    MIG PARTITION RECORD           */
    , eACTFRec_SYN             = '\xC8'        /*    SYNCHRONIZATION RECORD         */
    , eACTFRec_INDEX           = '\xC9'        /*    INDEX TYPE ENTRY               */
    , eACTFRec_MigrateControl  = '\xD4'        /*    A MIGRATE CONTROL RECORD       */
    , eACTFRec_Notepad         = '\xD5'        /*    A NOTE PAD RECORD              */
    , eACTFRec_OBJCT           = '\xD6'        /*    OBJECT TYPE ENTRY              */
    , eACTFRec_NPContinuation  = '\xD7'        /*    A NOTE PAD CONTINUATION REC    */
    , eACTFRec_OBJContinuation = '\xD8'        /*    OBJECT CONTINUATION TYPE       */
    , eACTFRec_PrimaryUser     = '\xD9'        /*    PRIMARY USER RECORD FOR REPORT */
    , eACTFRec_Sysdata         = '\xE2'        /*     SYSDATA                       */
    , eACTFRec_RulContinuation = '\xE3'        /*    A RULER CONTINUATION RECORD    */
    , eACTFRec_DefaultUser     = '\xE4'        /*    DEFAULT USER RECORD            */
    , eACTFRec_RULER           = '\xE7'        /*    A RULER TYPE OF RECORD         */
    , eACTFRec_RestoreControl  = '\xE8'        /*    A RESTORE CONTROL RECORD       */
    , eACTFRec_BackupControl   = '\xE9'        /*    A BACKUP CONTROL RECORD        */
} eACTFType;

typedef enum __attribute__((packed)) _eNOTEPType {
      eNPType_Columns     = '\xC3'          /*   NOTE PAD TYPE 'COLOMNS'     */
    , eNPType_General     = '\xC7'          /*   NOTE PAD TYPE 'GENERAL'     */
    , eNPType_TAG         = '\xE3'          /*   NOTE PAD TYPE 'TAG'         */
    , eNPType_WorkFlowHST = '\xE6'          /*   NOTE PAD TYPE 'WORKFLOW HST'*/
} eNOTEPType;

typedef struct __attribute__((packed)) _NPDAT {
	   unsigned char line[133];
} NPDAT, pNPDAT;


typedef struct __attribute__((packed)) _URECDsect {
} UREC, pUREC;


typedef struct __attribute__((packed)) _NPDsect {
   eNOTEPType NPTYPE;
   char NPFUT[4];
   HWORD NPLNnum;
   unsigned char NPUSER[8];
   unsigned char NPDATE[6];
   unsigned char NPTIME[6];
   unsigned char NPKEY[24];
   unsigned char NPRL[8];
   unsigned char NPSTRING[60];
   NPDAT NPlines[0];
} NP, *pNP;

typedef enum __attribute__((packed)) _eNOTEPID {
      eNPID_Continuation     = '\xC3'          /*   Indicate Continuation     */
    , eNPID_Free             = '\xC6'          /*   Indicate Free Block       */
    , eNPID_Note             = '\xD5'          /*   Note Block                */
    , eNPID_EOD              = '\xE9'          /* Indicate end of data        */
} eNOTEPID;

typedef union __attribute__((packed)) _uNPRECS {
	NP RECN;
	char RECZ[0];
	char RECF[0];
	char RECC[0];
} uNPRECS, *puNPRECS;

typedef struct __attribute__((packed)) _NPDATA {
	HWORD NPLEN;
	eNOTEPID NPID;
	uNPRECS NPREC;
} NPData, *pNPData;

typedef struct __attribute__((packed)) _NPBlock {
    eNOTEPType NPType;
    char ReportName[20];
	float ReportRecNum;
	char century[2];
	char odate[6];
	char primeURecCount[8];
	char rulerName[8];
	HWORD noteRecLen;
	TSTAMP decollationTM;
	char JESNUM[5];
	RSFLAG restore;           /* VSARSFLAG ?*/
	char restoreDate[6];
	char category[8];
	char migDate[8];
	char res3[9];
	char RNextension[30];
	NPData NDATA[0];
} NPBlock, *pNPBlock;

typedef union _DATA {
	NPBlock notePad;
} DATA, *pData;

typedef struct __attribute__((packed)) _ACTFbHDR_H {
	HWORD len;
	HWORD num;
	char RBA[4];
	char recidentifier[2];
	char res1[4];
	VARCHAR keyUsrName;
} ACTFbHDR_H, *pACTFbHDR_H;

typedef struct __attribute__((packed)) _ACTFbHDR {
	char keyJobName[8];
	TSTAMP keyCount;
	VARTSTAMP lastUpdate;
} ACTFbHDR, *pACTFbHDR;

typedef struct __attribute__((packed)) _ACTFbHDR_p1 {
	eACTFType type;
	char producerVersion[2];
	HWORD contRecNum;
	FLGByte flag;
	char jobName[8];
	char fill03[1];
    VARCHAR ReportName;
} ACTFbHDR_p1, *pACTFbHDR_p1;

typedef struct __attribute__((packed)) _ACTFbHDR_p2 {
    char JESNUM[5];
	char century[2];
	char odate[6];
	VARCHAR category;
	char fill05[20];
	VARCHAR shortnote;
} ACTFbHDR_p2, *pACTFbHDR_p2;

typedef struct __attribute__((packed)) _ACTFblock {
	HWORD len;
	HWORD num;
	char RBA[4];
	char keyUsrName[8];
	char keyJobName[8];
	TSTAMP keyCount;
	TSTAMP lastUpdate;
	eACTFType type;
	char producerVersion[2];
	char primeRecCount[8];
	HWORD contRecNum;
	FLGByte flag;
	char jobName[8];
	DATA block;
} ACTFblock, *pACTFblock;

typedef struct __attribute__((packed)) _ACTFrec {
	char res1[1];
	HWORD recnum;
	char res2[1];
	HWORD datalen;
	char res3[18];
	ACTFblock blocks[0];
}  ACTFrec, *pACTFrec;

#pragma pack(pop)

#endif /* CTDACTF_H_ */
